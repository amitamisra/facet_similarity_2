#!/usr/bin/python3
'''
Created on Jun 19, 2015

@author: amita
'''
import csv,codecs, nltk, enchant
import configparser
import copy 



def ReadCongigFile():
    config = configparser.ConfigParser()
    config.read('Compare_Pre_Postfilter_Config.ini')
    input_file = config.get('GC', 'input-file')
    output_file= config.get('GC', 'output-file')
    arguments=(input_file,output_file)
    return  (arguments)

def wordcount(inputcsv):
    NewRows=[]
    d = enchant.Dict("en_US")
    with open(inputcsv,encoding='utf-8') as csvfile:
        reader = csv.DictReader(csvfile)
        for row in reader:
            newrow=copy.deepcopy(row)
            sentence=row["sentence"]
            tokens=nltk.tokenize.word_tokenize(sentence)
            wordcount=len(tokens)
            newrow["wordcount"]=wordcount
            NewRows.append(newrow)
    return  NewRows  

def write_csv(outputcsv, rowdicts, fieldnames):
            restval=""
            extrasaction="ignore"
            dialect="excel"
            outputfile = codecs.open(outputcsv,'w',encoding='utf-8')
            csv_writer = csv.DictWriter(outputfile, fieldnames, restval, extrasaction, dialect, quoting=csv.QUOTE_NONNUMERIC)
            csv_writer.writeheader()
            csv_writer.writerows(rowdicts) 
            outputfile.close()  

def Execute(inputfile,outputfile):
    NewRows=wordcount(inputfile)
    fieldnames=NewRows[0].keys()
    write_csv(outputfile, NewRows, fieldnames)
    
def runGC():
    args= ReadCongigFile()
    inputfile= args[0]
    outputfile=args[1]
    Execute(inputfile,outputfile)


if __name__ == '__main__':
    runGC()