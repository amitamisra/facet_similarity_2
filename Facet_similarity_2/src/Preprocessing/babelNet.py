'''
Created on Jun 23, 2015

@author: amita
'''

import urllib
import json
import gzip
from urllib.parse import urlencode
from io import BytesIO
import configparser

#retrieve all hypernyms,antonyms, holonym 
def GetEdges(Synid,Key):
    hypernymlist=[]
    holonymlist=[]
    antonymlist=[]
    service_url = 'https://babelnet.io/v1/getEdges'
    params = {
        'id' : Synid,
        'key'  : Key
    }
    
    url = service_url + '?' + urlencode(params)
    request = urllib.request.Request(url)
    request.add_header('Accept-encoding', 'gzip')
    response = urllib.request.urlopen(request)

    if response.info().get('Content-Encoding') == 'gzip':
        buf = BytesIO( response.read())
        f = gzip.GzipFile(fileobj=buf)
        data = json.loads(f.read().decode("utf-8"))
    
        # retrieving Edges data
        for result in data:
            target = result.get('target')
            language = result.get('language')
            if language =="EN" :
            
                # retrieving BabelPointer data
                pointer = result['pointer']
                relation = pointer.get('fName')
                if 'hypernym' in relation.lower():
                    hypernymlist.append(target.encode('utf-8'))
                if 'antonym' in relation.lower():
                    antonymlist.append(target.encode('utf-8'))
                if  'holonym' in relation.lower():
                    holonymlist.append(target.encode('utf-8'))
                    
        return(hypernymlist,holonymlist,antonymlist)                
