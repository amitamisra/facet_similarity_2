
#!/usr/bin/python3
'''
Created on Oct 6, 2015

@author: amita
'''
import csv,codecs, nltk, enchant
import configparser
import copy 
from utilities import file_handling



def readCongigFile():
    config = configparser.ConfigParser()
    config.read('filter_predictionsConfig.ini')
    input_file = config.get('GC', 'input-file')
    output_file= config.get('GC', 'output-file')
    prediction_score=config.get('GC', 'prediction-score')
    prediction_value=float(config.get('GC', 'prediction-value'))
    arguments=(input_file,output_file,prediction_score,prediction_value)
    return  (arguments)


def predictionfilter(rows, predictionField,predictionvalue):
    NewRows=[]
    for row in rows:
        if row[predictionField] =="NA" :
            continue
        else:
            if float(row[predictionField]) > predictionvalue :
                NewRows.append(row)
                
            
    return NewRows    
        
    


def execute(inputfile,outputfile,predictionfield,predictionvalue):
    rows=file_handling.readCsv(inputfile)
    newRows=predictionfilter(rows, predictionfield,predictionvalue)
    fieldnames=newRows[0].keys()
    file_handling.writeCsv(outputfile, newRows, fieldnames)
    
def runGC():
    args= readCongigFile()
    inputfile= args[0]
    outputfile=args[1]
    predictionfield=args[2]
    predictionvalue=args[3]
    execute(inputfile,outputfile,predictionfield,predictionvalue)


if __name__ == '__main__':
    runGC()
