'''
Created on Jul 8, 2015

@author: amita
'''
from nltk import word_tokenize, FreqDist
import codecs, csv,  configparser, os
import copy
from utilities import file_handling 

def runGC():
    args= readCongigFile()
    input_csv= args[0]
    output_csv= args[1]
    stopwordfile=args[2]
    field_name=args[3]
    execute(input_csv,output_csv,stopwordfile, field_name)

def readCongigFile():
    config = configparser.ConfigParser()
    config_file = os.path.join(os.path.dirname(__file__), 'frequent_words_Config.ini')
    config.read(config_file, encoding='utf-8')
    input_csv = config.get("GC", 'input_file')
    output_csv = config.get("GC", 'output_file')
    stopword_file=config.get("GC", 'stopword_file')
    field_name= config.get("GC", 'field_name')
    arguments=(input_csv,output_csv,stopword_file,field_name)
    return  (arguments) 

def get_text(all_rows,field):
    all_text=[]
    combined_text =" "
    for row in all_rows:
        text=row[field]
        combined_text=combined_text + " " + text
        all_text.append(text)
    return (all_text, combined_text)

def frequency(combined_text,stopwordfile):
    combined_text=combined_text.lower()
    tokens= word_tokenize(combined_text)
    stopwordlist=file_handling.readTextFile(stopwordfile)
    stopwordlist=[word.strip() for word in stopwordlist ]
    tokens=[ newtoken for newtoken in tokens if newtoken not in stopwordlist]
    fdist=FreqDist(tokens) 
    print (fdist.most_common(10))
   
  
def execute(input_csv,output_csv,stopwordfile,field):
    all_rows=file_handling.readCsv(input_csv)
    text=get_text(all_rows,field)
    all_text_list= text[0]
    combined_text=text[1]
    frequency(combined_text,stopwordfile)
    

if __name__ == '__main__':
    runGC()