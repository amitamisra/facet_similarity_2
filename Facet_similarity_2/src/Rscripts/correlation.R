browser()
options( stringsAsFactors=FALSE) 
library(lpSolve,lib.loc="/Users/amita/software/Rpackage") 
library(irr,lib.loc="/Users/amita/software/Rpackage") 
setwd("/Users/amita/Facet_similarity_part2/Data/MechanicalTurk/BootstrappingFeaturesMT2") #sets R's working directory to near where my files are

InputFile<-"mt2_corr_input.csv"
total=2000
OutputFile<-"mt2_Corr.csv"


mytable<-read.csv(InputFile,header=TRUE,stringsAsFactors=FALSE)
table.sub2 <- subset(mytable, select = c(Id_A142ZRU284W9O:similarity))
sink("mt2_worker_count.txt")


dim(mytable)
class(mytable)
ncolm<-ncol(table.sub2)
ncolm

for(i in names(table.sub2)){
  targetColumn <- table.sub2[,i]
  len<-length(targetColumn[is.na(targetColumn)])
  cat(i)
  cat("\t")
  cat(total-len)
  cat("\n\n")
  
}
sink()
names(table.sub2)
class(table.sub2)
corr<-cor(table.sub2,use="pairwise.complete.obs")
write.csv(corr,OutputFile)

