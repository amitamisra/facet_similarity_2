library("ggplot2",lib.loc="/Users/amita/software/Rpackage")
library("reshape2",lib.loc="/Users/amita/software/Rpackage")

df <- read.csv("/Users/amita/facet_similarity_part2/data/deathPenalty/mechanicalTurk/greater_than_55/argumentQuality/Results/mt1_split_dp_gt55_argQualScale.csv")

make_yes_percent_avgs_by_bin <- function(df) {
  return(melt(lapply(split(df, cut(df$Prediction, seq(from = 0.54, to = 1.0, by = 0.02))),
                     function(df) {
                       percent.yes <- df$scale3_Count / 3
                       means <- mean(percent.yes)
                       means[is.na(means)] <- 0
                       return(means)
                     }))$value)
}

make_prediction_avgs_by_bin <- function(df) {
  return(melt(lapply(split(df, cut(df$Prediction, seq(from = 0.54, to = 1.0, by = 0.02))),
                     function(df) {
                       means <- mean(df$Prediction)
                       means[is.na(means)] <- 0
                       return(means)
                     }))$value) 
}

labels <- seq(from = 0.56, to = 1.0, by = 0.02)
yes.percent.avgs <- make_yes_percent_avgs_by_bin(df)
pred.avgs <- make_prediction_avgs_by_bin(df)
data.frame(labels, pred.avgs)
ggplot(data.frame(labels, yes.percent.avgs), aes(x=labels, y=avgs)) + 
  geom_point(aes(colour="Average Yes Argument")) + 
  geom_smooth(method=lm) +
  geom_point(data=data.frame(labels, pred.avgs), aes(x=labels, y=pred.avgs, colour="Prediction Scores")) +
  xlab("Bin range") +
  ylab("Argument Score")

