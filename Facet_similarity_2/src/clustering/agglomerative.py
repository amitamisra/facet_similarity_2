'''
Created on Jul 1, 2015
Agglomerative clustering  using scikit-learn.
Remove stop words and stem.
Uses TFIDF vectorizer for clustering
To change behaviour, change processtext function

@author: amita
'''
from nltk.stem.snowball import SnowballStemmer
from sklearn.feature_extraction.text  import TfidfVectorizer
import os
from nltk.tokenize import word_tokenize as word_tokenize
from operator import itemgetter
import itertools
from sklearn.cluster import AgglomerativeClustering 
import codecs, configparser, csv, copy

def readCsv(inputcsv):   
        inputfile = codecs.open(inputcsv,'r',encoding='utf-8') 
        result = list(csv.DictReader(inputfile))
        return result 

def writeCsv(outputcsv, rowdicts):
            fieldnames=rowdicts[0].keys()
            restval=""
            extrasaction="ignore"
            dialect="excel"
            outputfile = codecs.open(outputcsv ,'w',encoding='utf-8' )
            csv_writer = csv.DictWriter(outputfile, fieldnames, restval, extrasaction, dialect, quoting=csv.QUOTE_NONNUMERIC)
            csv_writer.writeheader()
            csv_writer.writerows(rowdicts) 
            outputfile.close()
        
    
def readTextFile(Filename): 
        f = open(Filename, "r", encoding='utf-8')
        TextLines=f.readlines()
        f.close()
        return TextLines
    
def writeTextFile(Filename,Lines): 
        f = open(Filename, "w", encoding='utf-8')
        f.writelines(Lines)
        f.close()   
        
         
# process text for clustering, stemming, remove stop words
def processText(AllRows,stopwordfile,clusterfield,process_clusterfield, stemmer):
    stopwordlist=readTextFile(stopwordfile)
    stopwordlist=[token.lower() for token in stopwordlist ]
    stopwordlist=[word.strip() for word in stopwordlist ]
    NewRows=[]
    for row in AllRows:
        newrow=copy.deepcopy(row)
        text=row[clusterfield]
        text=text.lower()
        tokens=word_tokenize(text)
        stemtoken=[stemmer.stem(token) for token in tokens if token not in stopwordlist]
        stemtext=" ".join(stemtoken)
        newrow[process_clusterfield]=stemtext
        NewRows.append(newrow)
    return  NewRows 

def runGC(stemmer):
    args= readCongigFile()
    inputcsv= args[0]
    outputcsv=args[1]
    outputtext=args[2]
    stopwordfile=args[3]
    clusterfield=args[4]
    process_clusterfield=args[5]
    clusternum=args[6]
    execute(inputcsv,outputcsv,outputtext,stopwordfile,clusterfield,process_clusterfield,clusternum,stemmer)

def readCongigFile():
    
    config = configparser.ConfigParser()
    config_file = os.path.join(os.path.dirname(__file__), 'agglomerative_Config.ini')
    config.read(config_file, encoding='utf-8')
    input_file = config.get("GC", 'input-file')
    output_file= config.get("GC", 'output-file')
    output_text=config.get("GC", 'output-text')
    stopword_file=config.get("GC", 'stopword-file')
    cluster_field= config.get("GC", 'cluster-field')
    process_clusterfield=config.get("GC", 'process-clusterfield')
    cluster_num= config.get("GC", 'cluster-num')
    arguments=(input_file,output_file,output_text,stopword_file,cluster_field,process_clusterfield, cluster_num)
    return  (arguments)

def execute(inputcsv,outputcsv,outputtext,stopwordfile,clusterfield,process_clusterfield,numcluster,stemmer):
    AllRows=readCsv(inputcsv)
    lines=[]
    StemmRows=processText(AllRows,stopwordfile,clusterfield,process_clusterfield,stemmer)
    clustertext=[row[process_clusterfield] for row in StemmRows]
    Res_Labels=createAgglomerativecluster(clustertext,numcluster)
    clusterOutputLabels(Res_Labels,StemmRows,clusterfield,lines,outputtext,outputcsv)

def createAgglomerativecluster(AllStrings,numcluster):
    Agg_cluster=AgglomerativeClustering(int(numcluster))
    termdoc=createtermdocTFIDFVect(AllStrings)
    Res_Labels=Agg_cluster.fit_predict(termdoc.toarray())
    return Res_Labels  



def createtermdocTFIDFVect(AllStrings):
    vectorizer_termTuple= termDoc_TFIDFCountVector(AllStrings)
    termdoc=vectorizer_termTuple[1]
    return termdoc
    
def termDoc_TFIDFCountVector(AllStrings):
    vectorizer = TfidfVectorizer()
    term_doc=vectorizer.fit_transform(AllStrings)
    return (vectorizer,term_doc)



def clusterOutputLabels(Res_Labels,StemmRows,clusterfield,lines,outputtext,outputcsv):
        
        doccount=0
        cluster_list=list() 
        for label in Res_Labels:
            row_cluster=copy.deepcopy(StemmRows[doccount])
            row_cluster["label_cluster"]=str(label)
            doccount=doccount+1
            cluster_list.append(row_cluster)
        writeCsv(outputcsv,cluster_list)    
        sortedcluster=sorted(cluster_list, key = itemgetter('label_cluster'))
        for key, items in itertools.groupby(sortedcluster, itemgetter('label_cluster')):
            Individualcluster_list =list(items)
            lines.append("\n\nNew Cluster")
            lines.append(key)
            count=0
            for cluster in Individualcluster_list:
                
                for keyp,value in cluster.items():
                    if keyp =="sentence":
                        count=count+1
                        lines.append("\n\n\n")
                        lines.append(str(count) + ": " + value)
        writeTextFile(outputtext,lines)                 
       

    
if __name__ == '__main__':
    stemmername=SnowballStemmer("english")
    runGC(stemmername)
    