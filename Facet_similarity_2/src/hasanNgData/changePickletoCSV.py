
#!/usr/bin/env python2
#coding: utf8 
'''
Created on Dec 10, 2015
create pairs from gay marriage from Hasan and Ng from Keshav, 
This strictly requires python2 since the data was pickled using python2
@author: amita
'''
from utilities import file_handling
import configparser,sys,copy
from unidecode import unidecode

def ascii_only_unicodeerror(text):  
    try:
        text=unidecode(text)
        return text
    except Exception as e: 
        print (str(e))
        sys.exit(-1)

def runGM(section):
    args= readConfigFile(section)
    input_file= args[0]
    output_file=args[1]
    execute(input_file,output_file)


def readConfigFile(section):
    config = configparser.ConfigParser()
    config.read('changePickletoCSV_config.ini')
    input_file = config.get(section, 'input-file')
    output_file= config.get(section, 'output-file')
    arguments=(input_file,output_file)
    return  (arguments)

def createtopicsentences(reason_sentences):
    allList=[]
    for topic,reasons in reason_sentences.items():
        for reason,reasonList in reasons.items():
                for reasonSent in reasonList:
                    topic_dict={}
                    topic_dict["topic"]=topic
                    topic_dict["reason"]=reason
                    print (reasonSent)
                    topic_dict["sentence"]=ascii_only_unicodeerror(reasonSent)
                    allList.append(topic_dict)
    return  allList
                

def execute(input_file,output_file):
    reason_sentences = readPickle(input_file)
    allrows= createtopicsentences(reason_sentences)
    colnames=allrows[0].keys()
    file_handling.writeCsv(output_file, allrows, colnames)
    

def readPickle(inputPickle):
    data =file_handling.readsPickle(inputPickle)
    return data
    


def create_allsentences(all_sentences):
    allList=[]
    for topic,topicList in all_sentences.items():
        for topickey,topicdata in topicList.items():
                for item_dict in topicdata:
                    topic_dict={}
                    topic_dict["topic"]=topic
                    topic_dict["topickey"]=topickey
                    for key,value in item_dict.items():
                        if key=="annotations":
                            annotation_dict=copy.deepcopy(value)
                        else:
                            #if key=="text":
                                #value=ascii_only_unicodeerror(value)
                            topic_dict[key]=value
                    # topic_dict["sentence"]=ascii_only_unicodeerror(reasonSent)
                    for reason,texts in annotation_dict.items():
                                topic_dict["reason"]=reason
                                for sentence in texts:
                                    if sentence:
                                        topic_dict["sentence"]=sentence
                                        allList.append(topic_dict)
                                    else:
                                        topic_dict["sentence"]=""
                                        allList.append(topic_dict)
                    
    return  allList
    

def executeallData(input_file,output_file):
    all_sentences = readPickle(input_file)
    allrows= create_allsentences(all_sentences)
    colnames=allrows[0].keys()
    file_handling.writeCsv(output_file, allrows, colnames)
    
    

def runGMallData(section):
    args= readConfigFile(section)
    input_file= args[0]
    output_file=args[1]
    executeallData(input_file,output_file)
    

if __name__ == '__main__':
    section="GM_Kehasv"
    #runGM(section)
    section="GM_Keshav_allData"
    runGMallData(section)