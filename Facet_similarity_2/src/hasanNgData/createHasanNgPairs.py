#!/usr/bin/env python3
#coding: utf8 
'''
Created on Dec 10, 2015
create pairs from gay marriage from Hasan and Ng from Lyn 
@author: amita
'''
import  configparser
from utilities import file_handling
from itertools import combinations
import operator
import itertools
from collections import defaultdict
import random

def sortrows(rows,sortfield):
    sorted_mtrows=sorted(rows, key=lambda x: int(operator.itemgetter(sortfield)(x)))   
    return  sorted_mtrows


def runGM(section):
    
    args= readCongigFile(section)
    input_file= args[0]
    output_fileunique=args[1]
    output_fileuniqueNodup=args[2]
    output_file_combs=args[3]
    sortfield=args[4]
    topic=args[5]
    columnlabel=args[6]
    output_file_combs_limited=args[7]
    max_sent_inclusions=args[8]
    execute(input_file,output_fileunique,output_fileuniqueNodup,output_file_combs,output_file_combs_limited,sortfield,topic,columnlabel,max_sent_inclusions)


def readCongigFile(section):
    config = configparser.ConfigParser()
    config.read('createHasanNgPairs_Config.ini')
    input_file = config.get(section, 'input-file')
    output_fileunique=config.get(section, 'output_fileunique')
    output_fileuniqueNodup=config.get(section, 'output_fileuniqueNodup')
    output_file_combs = config.get(section, 'output_file_combs')
    sortfield=config.get(section, 'sortfield')
    topic=config.get(section, 'topic')
    columnlabel=config.get(section, 'columnlabel')
    output_file_combs_limited=config.get(section, 'output_file_combs_limited')
    max_sent_inclusions=config.get(section, 'max_sent_inclusions')
    arguments=(input_file,output_fileunique,output_fileuniqueNodup,output_file_combs,sortfield,topic,columnlabel,output_file_combs_limited,max_sent_inclusions)
    return  (arguments)


        
def adduniquesentenceId(rows): 
    count=0
    for row in rows:
        row["uniqueRowNo"]=count
        row["source"]="HasanNg"
        count=count+1
    return rows


def removeduplicates(rows,sortfield):
    sentencelist=[]
    newrows=[]
    for row in rows:
        sentence=row[sortfield]
        if sentence not in sentencelist:
            sentencelist.append(row[sortfield])
            newrows.append(row)
            
    return newrows
    

def filterpairs(paircombs_2labels,explabel,labelcol,FinalListPairs):
    ListPairs=[]
    splitcombsgetpairs(paircombs_2labels,explabel,ListPairs)
    for pair in ListPairs:
        labels1=pair[ labelcol +"_1"]
        listlabels1=labels1.split(",")
        listlabels1 = [ x.strip() for x in  listlabels1]
        labels2=pair[ labelcol +"_2"]
        listlabels2=labels2.split(",")
        listlabels2 = [ x.strip() for x in  listlabels2]
        intersectionList=set(listlabels1).intersection(listlabels2)
        if len(intersectionList)> 0:
            FinalListPairs.append(pair)
    
    
        
def createpairrow_2label(rows_2label,labelcol,FinalListPairs):
    explabel=4
    paircombs_2labels= list(combinations(rows_2label,2))
    filterpairs(paircombs_2labels,explabel,labelcol,FinalListPairs)
    
    
def splitcombsgetpairs(combs,explabel,FinalListPairs):
    length=len(combs)
    for i in range(0,length):
        newdict={}
        rowtup=combs[i]
        tup=rowtup[0]
        for key,value in tup.items():
            newdict[key+"_1"]=value
        tup=rowtup[1]
        for key,value in tup.items():
            newdict[key+"_2"]=value 
        newdict["explabel"] = explabel      
        FinalListPairs.append(newdict)  



def makepairs(rows,columnlabel,tupleprocon,FinalListPairs): 
    pro=tupleprocon[1].strip()
    con=tupleprocon[0].strip()
    rowspro=[]
    rowscon=[]
    for row in rows:
        row[columnlabel]=row[columnlabel].strip()
        if row[columnlabel]==pro:
            rowspro.append(row)
        if row[columnlabel]==con:  
            rowscon.append(row) 
        
    combspro=list(combinations(rowspro, 2))
    combscon=list(combinations(rowscon, 2))
    products=list(itertools.product(rowspro,rowscon))
    
    splitcombsgetpairs(combspro,5,FinalListPairs)
    splitcombsgetpairs(combscon,5,FinalListPairs)
    splitcombsgetpairs(products,3,FinalListPairs)
    
    
    
def  make2labelList(rows,columnlabel):
    rows_2label=[]
    for row in rows:
        if len(row[columnlabel].split(",") ) ==2:
            rows_2label.append(row) 
    return rows_2label     
    

def makePairsproonly(rows,columnlabel,pro,FinalListPairs):
    rowspro=[]
    for row in rows:
        row[columnlabel]=row[columnlabel].strip()
        if row[columnlabel]==pro.strip():
            rowspro.append(row)
    combspro=list(combinations(rowspro, 2))
    splitcombsgetpairs(combspro,5,FinalListPairs)
    

def makecombsall(uniquerowsNodup,reasonList,columnlabel,FinalListPairs):
    for procon in reasonList:
        con=procon[0].strip()
        pro=procon[1].strip()
        tupleprocon=(con,pro)
        makepairs(uniquerowsNodup,columnlabel,tupleprocon,FinalListPairs)
        
    for proonly in reasonListpro:
        makePairsproonly(uniquerowsNodup,columnlabel,proonly,FinalListPairs)
        
        
    
    rows_2label=make2labelList(uniquerowsNodup,columnlabel)    
    createpairrow_2label(rows_2label,columnlabel,FinalListPairs)    
        
   
def filtercombination(FinalListPairs,max_sent_inclusions,sortfield):
    FilterPairs=[]
    unique=defaultdict(int)
    for row in FinalListPairs:
        unique1=row[sortfield+"_1"]
        unique2=row[sortfield+"_2"]
        if unique[unique1] == int(max_sent_inclusions) or  unique[unique2] == int(max_sent_inclusions):
            continue
        else:  
                unique[unique1]=unique[unique1]+1
                unique[unique2]=unique[unique2]+1
                FilterPairs.append(row)
    return  FilterPairs         
   
def execute(input_csv,output_fileunique,output_fileuniqueNodup,output_file_combs,output_file_combs_limited,sortfield,topic,columnlabel,max_sent_inclusions):
        random.seed(0)
        FinalListPairs=[]
        rows=file_handling.readCsv(input_csv) 
        uniquerows=adduniquesentenceId(rows)
        uniquerowsNodup=removeduplicates(uniquerows,sortfield)
        colnames= uniquerowsNodup[0].keys()
        file_handling.writeCsv(output_fileunique, uniquerows, colnames)
        file_handling.writeCsv(output_fileuniqueNodup,uniquerowsNodup, colnames)
        makecombsall(uniquerowsNodup,reasonListprocon,columnlabel,FinalListPairs)
        random.shuffle(FinalListPairs)
        combcols=sorted(list(FinalListPairs[0].keys()))
        file_handling.writeCsv(output_file_combs,FinalListPairs ,combcols)
        FilterPairs =filtercombination(FinalListPairs,max_sent_inclusions,sortfield)
        file_handling.writeCsv(output_file_combs_limited,FilterPairs ,combcols)
        

if __name__ == '__main__':
    section="MakeCombinationsLyn"
    reasonListprocon=[("c-procreation","p-procreation"),("c-normal-abnormal","p-normalabnormal"),("c-civilrights","p-civilrights"),("c-definitionmarriage","p-definitionmarriage"),\
                 ("c-moralimmoral","p-moralimmoral"),("c-familychildren","p-childfamily"),("c-harmnotharmsociety","p-harmnotharmsociety"), ("c-slipperyslope","p-slipperyslope")]
    reasonListpro=["p-churchstate","p-disease"]
    
    runGM(section)
    
    
    
    
    
    
    
    
#===========================================================================
    # c-normal-abnormal, c-disease, 
    # p-normalabnormal, p-definitionmarriage
    # p-moralimmoral, p-harmnoharm
    # p-moralimmoral, p-normalabnormal
    # p-moralimmoral, p-disease
    # p-normalabnormal, p-disease
    # p-churchstate, p-moralimmoral
    # p-churchstate, p-civilrights
    # p-churchstate, p-moralimmoral
    # c-moralimmoral, c-definitionmarriage
    # c-moralimmoral,  c-slipperyslope
    # c-moralimmoral, c-churchstate
    # c-moralimmoral, c-slipperyslope
    #===========================================================================    