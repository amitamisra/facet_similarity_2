'''
Created on Oct 20, 2015

@author: amita
'''
import operator
import configparser
from utilities import file_handling
import itertools

def run(section):
    args= readCongigFile(section)
    input_csv= args[0]
    output_csv=args[1]
    goodWorkerList=args[2]
    execute(input_csv,output_csv,goodWorkerList)


def readCongigFile(section):
    config = configparser.ConfigParser()
    config.read('annotatorReliabilityReid_Config.ini')
    input_file = config.get(section, 'input-file')
    output_file= config.get(section, 'output-file')
    goodWorkerList=config.get(section, 'goodWorkerList').split(",")
    arguments=(input_file,output_file,goodWorkerList)
    return  (arguments)


def execute(input_csv,output_csv,goodWorkerList):
    rowdicts=file_handling.readCsv(input_csv)
    newRows=changeInputFormat(rowdicts,goodWorkerList)
    colnames=sorted(newRows[0].keys(),reverse=True)
    file_handling.writeCsv(output_csv, newRows, colnames)
    

# assumes workerUD begins with prefix Id and each hit identified by HITId
def changeInputFormat(rowdicts,goodWorkerList):
    allRows=[]
    rowdicts.sort(key=operator.itemgetter("HITId"))
    for dictkey, items in itertools.groupby(rowdicts, operator.itemgetter("HITId")):
        Hitids=list(items)
        count=0
        for row in Hitids:
                count=count+1
                for key,value in row.items():
                    if str(key).startswith("Id"):
                            newdict={}
                            newdict["Item Id"]=row["HITId"]+ " +" + str(count)
                            if row[key].lower()=="yes":
                                newdict["Annotation"]="1"
                            else:
                                if row[key].lower()=="no":
                                    newdict["Annotation"]=-1
                                    
                                else:
                                    newdict["Annotation"]="NA"
                            newdict["Annotator Id"]=key
                            allRows.append(newdict)
    return allRows            
if __name__ == '__main__':
    section="ArgumentQualityMT1_GC"
    run(section)
    