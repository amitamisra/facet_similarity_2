#!/usr/bin/python3
'''
Created on Jul 5, 2015
Take pairs file, create a sorted file based on umbc similarity, create a file of top 500 umbc.
expand top 500 umbc file and create  mechanical turk input with a fixed number of pairs in a hit.
for mt1 we used 10 pairs in a hit, put 50 hits on turk
@author: amita
'''


from operator import itemgetter
import codecs, configparser, csv
from copy import deepcopy


def runGC():
    args= readCongigFile()
    input_csv= args[0]
    output_file_sorted=args[1]
    argpairs_umbctop500=args[2]
    argpairs_mt1=args[3]
    num_pairs_row=args[4]
    No_ofhits=args[5]
    sort_field_name=args[6]
    unique_field_name=args[7]
    execute(input_csv,output_file_sorted,argpairs_umbctop500,argpairs_mt1,num_pairs_row,No_ofhits,sort_field_name,unique_field_name)


def readCongigFile():
    config = configparser.ConfigParser()
    config.read('argsim_mt1_Config.ini')
    input_file = config.get('GC', 'input-file')
    output_file_sorted= config.get('GC', 'output-file-sorted')
    argpairs_umbctop500=config.get('GC', 'argpairs-umbctop500')
    argpairs_mt1=config.get('GC', 'argpairs-mt1')
    num_pairs_row= int(config.get('GC', 'num-pairs-row'))
    no_ofhits=int(config.get('GC', 'no-ofhits'))
    umbcfieldname= config.get('GC', 'umbcfieldname')
    unique_field_name=config.get('GC', 'unique-field-name')
    arguments=(input_file,output_file_sorted,argpairs_umbctop500,argpairs_mt1,num_pairs_row,no_ofhits,umbcfieldname,unique_field_name)
    return  (arguments)





# csv had an issue with period in header field, replace by _
def removeperiodheader(row_dicts):
    new_rows=[]
    for row in row_dicts:
        newrow={}
        for colname, value in row.items():
            if "." in colname:
                newcol=colname.replace(".", "_")
                newrow[newcol]=value
            else:
                newrow[colname]=row[colname]       
        new_rows.append(newrow)        
    return new_rows





# defines overall flow of the script, see config file for description of arguments
def execute(input_csv,output_file_sorted,argpairs_umbctop500,argpairs_mt1,num_pairs_row,No_ofhits,sort_field_name,unique_field_name):
    all_rows=readCsv(input_csv)
    sorted_rows=sorted(all_rows, key=lambda x: float(itemgetter(sort_field_name)(x)), reverse=True)
    unique_rows=addUniqueRowCol(sorted_rows)
    new_rows=removeperiodheader(unique_rows)
    fieldnames=list(new_rows[0].keys())
    writeCsv(output_file_sorted, new_rows,fieldnames)
    writeCsv(argpairs_umbctop500,new_rows[0:500],fieldnames)
    createHitInput(argpairs_umbctop500,argpairs_mt1,num_pairs_row,No_ofhits,sort_field_name,unique_field_name)
    

#change long csv to extended  pairs_row for MT input
#num_pairs_row: num pf pairs in a HIT
#argpairs_umbctop500:long input csv with all the records
#argpairs_mt1: output csv expanded, combines records
# create fieldnames for combined file
# all fields except umbcfieldname,unique_field_name were duplicates as present in both the pairs
def createHitInput(argpairs_umbctop500,argpairs_mt1,num_pairs_hit,No_ofhits, umbcfieldname,unique_field_name):
    MTrow={}
    fieldnames=[]
    uniquefields=[]
    MTRows=[]
    intc=1
    extc=0
    row_dicts=readCsv(argpairs_umbctop500)
    fields=row_dicts[0].keys()
    for field in fields:
        if field==umbcfieldname or field==unique_field_name:
            uniquefields.append(field)
        else:
            fieldnames.append(field[:-1])
    set_fieldnames=set(fieldnames)        
    for row in row_dicts:
        if extc < int(No_ofhits):  
            if intc<=int(num_pairs_hit):
                for field in set_fieldnames:                
                    MTrow[field +str("a_")+str(intc)]=row[field+str(1)]
                    MTrow[field +str("b_")+str(intc)]=row[field+str(2)] 
                for field in uniquefields:
                    MTrow[field +str(intc)]=row[field]
                    
                intc=intc+1       
                                     
            if intc-1==num_pairs_hit:
                MTRows.append(MTrow)   
                extc=extc+1
                intc=1
                MTrow=dict()
                
        else:
            break
        
    new_fieldnames=list(MTRows[0].keys())  
    writeCsv(argpairs_mt1,MTRows, new_fieldnames)       



    
#read a csv
#return a list of dictionaries
def readCsv(inputcsv):   
        inputfile = codecs.open(inputcsv,'r',encoding='utf-8') 
        result = list(csv.DictReader(inputfile))
        return result 
   
   
   
#rowdicts:list of dicts
#fieldnames:header fields   
def writeCsv(outputcsv, rowdicts, fieldnames):
            restval=""
            extrasaction="ignore"
            dialect="excel"
            outputfile = codecs.open(outputcsv,'w',encoding='utf-8')
            csv_writer = csv.DictWriter(outputfile, fieldnames, restval, extrasaction, dialect, quoting=csv.QUOTE_NONNUMERIC)
            csv_writer.writeheader()
            csv_writer.writerows(rowdicts) 
            outputfile.close()  




# AllRows:list of dictionaries, read from csv
def addUniqueRowCol(AllRows):
    count=1
    AllNewRows=[]
    for row in AllRows:
        Newrow=deepcopy(row)
        Newrow["uniqueRowNo"]=count # gives each row a unique number for Mechanical turk
        count=count+1
        AllNewRows.append(Newrow)
    return AllNewRows 
     

if __name__ == '__main__':
    runGC()