'''
Created on Oct 2, 2015

@author: amita

Take the output from split_mt2_results file and select 3 random pairs from each HIT to create a gold standard.
@author: amita
'''
from collections import defaultdict
import configparser
from utilities.file_handling import writeCsv
from utilities.file_handling import readCsv
from utilities.file_handling import write_csv_header_unkown
import itertools,operator
from random import shuffle
                    
def execute(input_csv,output_csv,no_ofPairsfromHIT): 
    rowdicts= readCsv(input_csv)
    goldlist=[]
    for key, items in itertools.groupby(rowdicts, operator.itemgetter("HITId")):
        Hitids=list(items)
        shuffle(Hitids)
        goldlist.extend(Hitids[0:no_ofPairsfromHIT])
    write_csv_header_unkown(output_csv,goldlist)

def runGC():
    args= readCongigFile()
    input_csv= args[0]
    output_csv=args[1]
    no_ofPairsfromHIT=args[2]
    execute(input_csv,output_csv, no_ofPairsfromHIT)
    


def readCongigFile():
    config = configparser.ConfigParser()
    config.read('createGoldStandardforMT2_Config.ini')
    input_csv = config.get('GC', 'input_csv')
    output_csv=config.get('GC', 'goldStandard_csv')
    no_ofPairsfromHIT=int(config.get('GC', 'num_pairsfromHIT_csv'))
    arguments=(input_csv,output_csv,no_ofPairsfromHIT)
    return  (arguments)  

if __name__ == '__main__':
    runGC()
    
    
