'''
Created on Oct 2, 2015

@author: amita
'''
from utilities.file_handling import writeCsv
from utilities.file_handling import readCsv
from mechanicalturk import mt2_bootstrapped
import configparser


def runGC():
    args= readCongigFile()
    input_csv= args[0]
    output_csv=args[1]
    num_pairs_row=args[2]
    No_ofhits=args[3]
    unique_field_name=args[4]
    execute(input_csv,output_csv,num_pairs_row,No_ofhits,unique_field_name)


def readCongigFile():
    config = configparser.ConfigParser()
    config.read('createMT2withGoldStandard_Config.ini')
    input_file = config.get('GC', 'input-file')
    output_file= config.get('GC', 'output-file')
    num_pairs_row= int(config.get('GC', 'num-pairs-row'))
    no_ofhits=int(config.get('GC', 'no-ofhits'))
    unique_field_name=config.get('GC', 'unique-field-name')
    arguments=(input_file,output_file,num_pairs_row,no_ofhits,unique_field_name)
    return  (arguments)


def execute(input_csv,output_csv,num_pairs_row,No_ofhits,unique_field_name):
    rows=readCsv(input_csv)
    new_rows=mt2_bootstrapped.removeperiodheader(rows, ".")
    new_rows=mt2_bootstrapped.removeperiodheader(new_rows, "*")
    createHitGold(new_rows, output_csv, num_pairs_row,No_ofhits,unique_field_name)  
    

def createHitGold(row_dicts, output_csv, num_pairs_hit,No_ofhits,unique_field_name):
    MTrow={}
    MTRows=[]
    intc=1
    extc=0
    fields=row_dicts[0].keys()
    
    for row in row_dicts:
        if extc < int(No_ofhits):  
            if intc<=int(num_pairs_hit):
                
                for field in fields:
                    MTrow[field +str(intc)]=row[field]
                    
                intc=intc+1       
                                     
            if intc-1==num_pairs_hit:
                MTRows.append(MTrow)   
                extc=extc+1
                intc=1
                MTrow=dict()
                
        else:
            break
        
    new_fieldnames=list(MTRows[0].keys())  
    writeCsv(output_csv,MTRows, new_fieldnames)       



    
if __name__ == '__main__':
    runGC()