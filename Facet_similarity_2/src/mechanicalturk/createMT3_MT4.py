'''
Created on Oct 4, 2015
create MT3 and MT4 for distribution. contains 3000-4000, and 5000-6000 pairs from sorted predicted file.
create gold standard for MT3 and MT4 by randomly selecting 200 instances for  each. 
@author: amita
'''
from utilities import file_handling
from mechanicalturk import  createMT2withGoldstandard
from mechanicalturk import mt2_bootstrapped
import configparser
import random

def runGC(gc):
    args= readCongigFile(gc)
    input_csv= args[0]
    output_MT_csv=args[1]
    gold_MT_csv=args[2]
    MTinput_gold_csv=args[3]
    num_pairs_row=args[4]
    No_ofhits=args[5]
    unique_field_name=args[6]
    range_begin=int(args[7])
    range_end=int(args[8])
    num_to_select=int(args[9])
    execute(input_csv,output_MT_csv,gold_MT_csv,MTinput_gold_csv,num_pairs_row,No_ofhits,unique_field_name,range_begin,range_end,num_to_select)


def readCongigFile(gc):
    config = configparser.ConfigParser()
    config.read('createMT3_MT4_GoldConfig.ini')
    input_csv = config.get(gc, 'input-csv')
    output_MT_csv= config.get(gc, 'output_MT3_csv')
    gold_MT_csv= config.get(gc, 'gold_MT3_csv')
    MTinput_gold_csv= config.get(gc, 'MT3input_gold_csv')
    num_pairs_row= int(config.get(gc, 'num-pairs-row'))
    no_ofhits=int(config.get(gc, 'no-ofhits'))
    unique_field_name=config.get(gc, 'unique-field-name')
    range_begin= config.get(gc, 'range_begin')
    range_end= config.get(gc, 'range_end')
    num_to_select=int(config.get(gc, 'num_to_select'))
    arguments=(input_csv,output_MT_csv,gold_MT_csv,MTinput_gold_csv,num_pairs_row,no_ofhits,unique_field_name,range_begin,range_end,num_to_select)
    return  (arguments)

def createMT(input_csv,range_begin,range_end):
    rowdicts=file_handling.readCsv(input_csv)
    selected_rows=rowdicts[range_begin:range_end]
    return selected_rows
    
    

def select_random_items(selected_rows,num_to_select):
    list_of_random_items = random.sample(selected_rows, num_to_select)
    return list_of_random_items
    

def execute(input_csv,output_MT_csv,gold_MT_csv,MTinput_gold_csv,num_pairs_row,No_ofhits,unique_field_name,range_begin,range_end,num_to_select):
    selected_rows=createMT(input_csv,range_begin,range_end)
    new_rows=mt2_bootstrapped.removeperiodheader(selected_rows, ".")
    new_rows=mt2_bootstrapped.removeperiodheader(new_rows, "*")    
    colnames=new_rows[0].keys()
    file_handling.writeCsv(output_MT_csv, new_rows, colnames)
    random_rows=select_random_items(new_rows,num_to_select)
    file_handling.writeCsv(gold_MT_csv, random_rows, colnames)
    createMT2withGoldstandard.createHitGold(random_rows, MTinput_gold_csv, num_pairs_row,No_ofhits,unique_field_name)   
    

if __name__ == '__main__':
    runGC('GC3')
    runGC('GC4')