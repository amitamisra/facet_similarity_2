# Filter by prediction score and keep high ones. #

import  argparse
from utilities import  file_handling as fh
import merge as mg

parser = argparse.ArgumentParser(description='Filter by prediction score and keep high ones.')
parser.add_argument('infile', metavar='infile', nargs=1,\
                    help='the input file (json or csv) to filter')
parser.add_argument('min', metavar='float', type=float, nargs=1,\
                    help='a float for the minimum predictoin score')
parser.add_argument('outfile', metavar='outfile', nargs=1,\
                    help='the output csv file')
args = parser.parse_args()
infile = args.infile[0]
outfile = args.outfile[0]
#rows = fh.read_csv(infile)
rows = mg.read_to_dicts(infile)
minvalue = args.min[0]

#prediction='regression_label'
prediction='target'

# Collect only the pairs with a score higher than min
high_rows = []
for i in range (0, len(rows)):
    if float(rows[i][prediction]) >= minvalue:
        high_rows.append(rows[i])

# Randomize the order
#random.shuffle(high_rows)

fieldnames = rows[0].keys()
fh.writeCsv(outfile, high_rows, fieldnames)
