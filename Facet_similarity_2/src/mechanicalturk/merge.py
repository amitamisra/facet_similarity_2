# Merge the feature file and the sentence file.#

import json, argparse
from utilities import file_handling as fh 
import operator

def write_output(name, dicts, fieldnames):
    types = name.split('.')[-1]
    if types == 'json':
        fh.writeJson(dicts, name)
    elif types == 'csv':
        fh.writeCsv(name, dicts, fieldnames)

def read_to_dicts(name):
    types = name.split('.')[-1]
    if types == 'json':
        with open(name) as file:
            rowdicts = json.load(file)
    elif types == 'csv':
        rowdicts = fh.readCsv(name)
    return rowdicts

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Merge the feature file and the sentence file.')
    parser.add_argument('in1', metavar='features', nargs=1,\
                       help='the feature file (json or csv) to merge')
    parser.add_argument('in2', metavar='sentences', nargs=1,\
                       help='the sentence file (json or csv) to merge')
    parser.add_argument('outfile', metavar='outfile', nargs=1,\
                       help='the output file (json or csv)')
    
    args = parser.parse_args()

    in1 = args.in1[0]
    in2 = args.in2[0]
    out = args.outfile[0]
    feats = read_to_dicts(in1)
    sents = read_to_dicts(in2)

    if len(feats) == len(sents):
        for i in range(len(sents)):
            for key, val in sents[i].items():
                feats[i][key] = val
    else:
        print('error: input files not matching')
        exit()

   # change the suffix'_1' '_2' to '_a' '_b'
    colnames = set()
    for rowdict in feats:
        for key in rowdict.keys():
            val = rowdict[key]
            if key[-2:] == '_1':
                newkey = key[:-2] + '_a'
                del rowdict[key]
                rowdict[newkey] = val
                colnames.add(newkey)
            elif key[-2:] == '_2':
                newkey = key[:-2] + '_b'
                del rowdict[key]
                rowdict[newkey] = val
                colnames.add(newkey)
            else:
                colnames.add(key)

    write_output(out, feats, colnames)
    