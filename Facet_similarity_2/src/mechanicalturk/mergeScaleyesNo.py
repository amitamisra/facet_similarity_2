'''
Created on Dec 18, 2015
merge results from gay marriage yes no and gay marriage on scale.
Change 720 on scale to yes /no
if scale=3, give a yes
else if scale =1 or 2 give it a no
@author: amita
'''
import os
from utilities import file_handling

def execute(inputscaleGM,inputyescount_gt_1GM,mergedout):
    mergedrows=[]
    yescount_gt_1rows=file_handling.readCsv(inputyescount_gt_1GM)
    scalerows=file_handling.readCsv(inputscaleGM)
    for scalerow in scalerows:
        
        scalerow["Yes_Count"]=scalerow["scale3_Count"]
        scalerow["No_Count"]=scalerow["scale2_1_Count"]
        
    mergedrows.extend(yescount_gt_1rows)
    mergedrows.extend(scalerows)
    colnames=mergedrows[0].keys()
    file_handling.writeCsv(mergedout,mergedrows,colnames)


def runmergeGMScaleYesNO():
    inputdir="/Users/amita/facet_similarity_part2/data/gayMarriage/mechanicalTurk/greater_than_55/argumentQuality/"
    inputscaleGM=os.path.join(inputdir + "results_mt2/", "mt2_split_gm_gt55_argQualScale.csv" )
    inputyescount_gt_1GM=os.path.join(inputdir+"results_mt1/", "yes_Count_gt1_split_mt1_GM_results.csv")
    mergedout=os.path.join(inputdir+ "mergemt1_mt2/","mt1_mt2_yes_Count_gt1_mt1_mt2_argQualScale.csv")
    execute(inputscaleGM,inputyescount_gt_1GM,mergedout)
    
    
    

if __name__ == '__main__':
    runmergeGMScaleYesNO()
    