#!/usr/bin/env python3
#coding: utf8 
'''
Created on Dec 4, 2015
@author: amita
Created on Oct 26, 2015
Take pairs file created using all pairs with arguments having a yes score > 1 for argument quality task for gay marriage
the file is sorted based on umbc, create a file of top 2000 records and use uniquerow field to get other fields from
yes_Count_gt1_split_mt1_GM_results.csv
expand top 2000 file and create  mechanical turk input with a fixed number of pairs in a hit.
for mt2 we used 10 pairs in a hit, put 200 hits on turk


create pairs from HAsan and Ng annotated from Lyn
@author: amita
'''
from operator import itemgetter
import configparser
from utilities import file_handling
import random
from copy import deepcopy
from mechanicalturk import mt1_argumentqualitypairsGC
#PMI_Max, PMI_Harmonic,discussionId,postId
def runGM(section):
    args= readCongigFile(section)
    input_file1_comb= args[0]
    input_file_cols=args[1]
    output_csv=args[2]
    mt1_top2000=args[3]
    mt1_top2000Formatted=args[4]
    num_pairs_hit=args[5]
    No_ofhits=args[6]
    uniquefield=args[7]
    extrafields=args[8]
    addfields=args[9]
    execute(input_file1_comb,input_file_cols,output_csv,  mt1_top2000,mt1_top2000Formatted,num_pairs_hit,No_ofhits, uniquefield,extrafields, addfields)
    
def removeextracols(colnames,extrafields):
    newcols=[]
    for x in colnames:
        notinclude=False
        for prefix  in  extrafields:
            if str(x).lower().startswith(prefix) :
                notinclude=True
                break
        if notinclude == False:    
            newcols.append(x)
                
                
    return newcols

def mergemissingcols(input_file1_comb,input_file_cols,uniquefield, addfields): 
    all_rows1=file_handling.readCsv(input_file1_comb) 
    all_rows2=file_handling.readCsv(input_file_cols) 
    allnewrows=[]
    for row in all_rows1:
        newdict={}
        unique1=int(row[uniquefield+"_1"])
        unique2=int(row[uniquefield+"_2"])
        row1=[rowUnique for rowUnique in all_rows2 if int(rowUnique[uniquefield])==int(unique1)][0]
        row2=[rowUnique for rowUnique in all_rows2 if int(rowUnique[uniquefield])==int(unique2)][0]
        newdict=deepcopy(row)
        for field in addfields:
            newdict[field+"_1"]=row1[field]
            newdict[field+"_2"]=row2[field]
        allnewrows.append(newdict)
    
    return(allnewrows)          

def execute(input_file1_comb,input_file_cols,output_csv,  mt1_top2000,mt1_top2000Formatted,num_pairs_hit,No_ofhits, uniquefield,extrafields, addfields):
    
    allnewrows=mergemissingcols(input_file1_comb,input_file_cols,uniquefield, addfields)
    top2000rows=allnewrows[0:2000]  
    colnames=top2000rows[0].keys()
    newcols=removeextracols(colnames,extrafields)
    file_handling.writeCsv(output_csv, allnewrows, newcols) 
    file_handling.writeCsv(mt1_top2000, top2000rows, newcols) 
    mt1_argumentqualitypairsGC.createHitInput(mt1_top2000,mt1_top2000Formatted,num_pairs_hit,No_ofhits)
             
def readCongigFile(section):
    config = configparser.ConfigParser()
    config.read('mt1_argumentqualitypairs_Config.ini')
    input_file1_comb = config.get(section, 'input-file1-comb')
    input_file_cols = config.get(section, 'input-file-cols')
    output_file1= config.get(section, 'output-file1')
    mt1_top2000= config.get(section, 'mt1_top2000')
    mt1_top2000Formatted= config.get(section, 'mt1_top2000Formatted')
    num_pairs_hit= int(config.get(section, 'num-pairs-hit'))
    no_ofhits=int(config.get(section, 'no-ofhits'))
    uniquefield=config.get(section,"uniquefield")
    removefields=config.get(section,"removefields").split(",")
    addfields=config.get(section,"addfields").split(",")
    arguments=(input_file1_comb,input_file_cols,output_file1,mt1_top2000,mt1_top2000Formatted,num_pairs_hit,no_ofhits,uniquefield,removefields,addfields)
    return  (arguments)

def select100(allrows,explabel_field,noof_5,noof_3,uniquefield):
    count5=0
    count3=0
    newrows=[]
    for row in allrows:
        label=int(row[explabel_field])
        if label==5 and count5 < int(noof_5):
            count5=count5+1
            newrows.append(row)
        else:
            if label==3 and count3 < int(noof_3):
                count3=count3+1
                newrows.append(row)
        #row[uniquefield+"_1"]=row[uniquefield+"_1"]+"_hasanNg"
        #row[uniquefield+"_2"]=row[uniquefield+"_2"]+"_hasanNg" 
            
                
    return newrows        
     
def removerows(ignorerows, addrandompairs,uniquefield):
    newrows=[]
    ignorerowsunique=[(int(rowignore[uniquefield +"_1"]),int(rowignore[uniquefield +"_2"])) for rowignore in ignorerows]
    for row in addrandompairs:
        unique1=int(row[uniquefield +"_1"])
        unique2=int(row[uniquefield +"_2"])
        listunique=[(unique1,unique2),(unique2,unique1) ]
        if any( c in ignorerowsunique for c in listunique):
            continue
        else:
            row["source_1"]="IAC"
            row["source_2"]="IAC"
            #row[uniquefield+"_1"]=row[uniquefield+"_1"]+"_iac"
            #row[uniquefield+"_2"]=row[uniquefield+"_2"]+"_iac" 
            newrows.append(row)
    return newrows        

def createmt200(ignorerows, addrandompairs,rows100,randompairscount,uniquefield):
    newrows=[]
    newrows.extend(rows100)
    rowstoinclude=removerows(ignorerows, addrandompairs,uniquefield)
    random.shuffle(rowstoinclude)
    newrows.extend(rowstoinclude[0:randompairscount])
    return newrows
    
def executeHasanNg(input_file_comb,ignoreFile,addrandompairsFile,mt1_100,mt1_200,mt1_200Formatted,num_pairs_hit,no_ofhits,uniquefield,explabel_field,noof_5,noof_3,randompairscount):
    allrows=file_handling.readCsv(input_file_comb)
    rows100=select100(allrows,explabel_field,noof_5,noof_3,uniquefield)
    colnames=sorted(list(rows100[0].keys()))
    file_handling.writeCsv(mt1_100, rows100, colnames)
    ignorerows=file_handling.readCsv(ignoreFile)
    addrandompairs=file_handling.readCsv(addrandompairsFile)
    rows200=createmt200(ignorerows, addrandompairs,rows100,randompairscount,uniquefield)
    random.shuffle(rows200)
    file_handling.write_csv_header_unkown(mt1_200, rows200, colnames)
    mt1_argumentqualitypairsGC.createHitInput(mt1_200,mt1_200Formatted, num_pairs_hit, no_ofhits)
    


def runHasanNgLyn(section):
    args= readCongigFileHasanNgPairs(section)
    input_file_comb= args[0]
    ignoreFile= args[1]
    addrandompairsFile= args[2]
    mt1_100=args[3]
    mt1_200=args[4]
    mt1_200Formatted=args[5]
    num_pairs_hit=args[6]
    no_ofhits=args[7]
    uniquefield=args[8]
    explabel_field=args[9]
    noof_5=args[10]
    noof_3=args[11]
    randompairscount=args[12]
    executeHasanNg(input_file_comb,ignoreFile,addrandompairsFile,mt1_100,mt1_200,mt1_200Formatted,num_pairs_hit,no_ofhits,uniquefield,explabel_field,noof_5,noof_3,randompairscount)
    
     
    
def readCongigFileHasanNgPairs(section): 
    randompairscount=100   
    config = configparser.ConfigParser()
    config.read('mt1_argumentqualitypairs_Config.ini')
    input_file_comb = config.get(section, 'input-file-comb')
    ignoreFile=config.get(section,"ignoreFile")
    addrandompairsFile=config.get(section,"addrandompairsFile")
    mt1_100= config.get(section, 'mt1_100')
    mt1_200=config.get(section, "mt1_200")
    mt1_Formatted200= config.get(section, 'mt1_Formatted200')
    num_pairs_hit= int(config.get(section, 'num-pairs-hit'))
    no_ofhits=int(config.get(section, 'no-ofhits'))
    uniquefield=config.get(section,"uniquefield")
    explabel_field=config.get(section,"explabel_field")
    noof_5=config.get(section,"noof5")
    noof_3=config.get(section,"noof3")
    
    arguments=(input_file_comb,ignoreFile,addrandompairsFile,mt1_100,mt1_200, mt1_Formatted200,num_pairs_hit,no_ofhits,uniquefield,explabel_field,noof_5,noof_3,randompairscount)
    return  (arguments)


if __name__ == '__main__':
    random.seed(0)
    section="argumentQualpairsGM"
    #runGM(section)
    
    section="HasanNgPairs_Lyn"
    runHasanNgLyn(section)
    