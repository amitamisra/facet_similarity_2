#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
Created on Nov 3, 2015
merge true labels for AFS with features based on unique RowNo fields
@author: amita
'''

import configparser
from utilities import file_handling
import copy,sys,os
from collections import OrderedDict
uniquefield="uniqueRowNo"

FeatureList=["liwc","rouge","umbcsimilarity","ngram","umbc_combined"]



def readCongigFile(section):
    config = configparser.ConfigParser()
    config.read('mt1_argumentQualPairsmergeTrueLabelFeatures_Config.ini')
    input_csv1 = config.get(section, 'input_csv1')
    input_csv2 = config.get(section, 'input_csv2')
    output_csv= config.get(section, 'output_csv')
    label= config.get(section, 'regression_label')
    arguments=(input_csv1,input_csv2,output_csv,label)
    return arguments



def runGC(section):
    args= readCongigFile(section)
    input_csv1= args[0]
    input_csv2= args[1]
    output_csv=args[2]
    label=args[3]
    execute(input_csv1,input_csv2,output_csv,label)
 

    
def mergeCSV(input_csv1,input_csv2,label):
    allRows=[]   
    input_rows_features1=file_handling.readCsv(input_csv1)
    input_rows_label2=file_handling.readCsv(input_csv2)
    for count in range(0,len(input_rows_features1)):
        newRow= copy.deepcopy(input_rows_features1[count])
        labelRow=input_rows_label2[count]
        newRow[label]= labelRow[label]
        if float(newRow["UmbcSimilarity"][0:5] )!= float(labelRow["similarity"][0:5]):
            if count==714 or count== 1896 or count == 328 or count == 523:
                print()
            else:    
                print("error in merging csv")
                print (count)
                print(newRow["UmbcSimilarity"][0:5])
                print(labelRow["similarity"][0:5])
                sys.exit(-1)
        newRow.update(labelRow)             
        allRows.append(newRow)    
    return allRows



def execute(input_csv1,input_csv2,output_csv,label):
    allRows= mergeCSV(input_csv1,input_csv2,label)     
    colnames=allRows[0].keys()   
    file_handling.writeCsv(output_csv, allRows, colnames)     
     


def readCongigFileGM(section):
    config = configparser.ConfigParser()
    config.read('mt1_argumentQualPairsmergeTrueLabelFeatures_Config.ini')
    input_csv1 = config.get(section, 'input_csv1')
    input_csv2 = config.get(section, 'input_csv2')
    feature_csv= config.get(section, 'feature_csv')
    all_csv=config.get(section, 'all_csv')
    label= config.get(section, 'regression_label')
    arguments=(input_csv1,input_csv2,feature_csv,all_csv,label)
    return arguments

def runGM(section):
    args= readCongigFileGM(section)
    input_csv1= args[0]
    input_csv2= args[1]
    feature_csv=args[2]
    all_csv=args[3]
    label=args[4]
    executeGM(input_csv1,input_csv2,feature_csv,all_csv,label)

def createFeaturesdict(row,featureRow,FeatureList):
    
    for key,value  in row.items():
        if key.strip().lower().startswith(tuple(FeatureList)):
            featureRow[key]= value
    
     
def mergeCSV_GM(input_csv1,input_csv2,label): 
    allRows=[]   
    featureRows=[]
    rows_features=file_handling.readCsv(input_csv1)
    rows_label=file_handling.readCsv(input_csv2)
    
    for count in range(0,len(rows_label)):
        row_label=rows_label[count]
        unique1=int(row_label[uniquefield+"_1"])
        unique2=int(row_label[uniquefield+"_2"])
        row1=[rowUnique for rowUnique in rows_features if int(rowUnique[uniquefield+"_1"])==int(unique1) and  int(rowUnique[uniquefield+"_2"])==int(unique2)]
        
        if float(row1[0]["UmbcSimilarity"][0:5] )!= float(row_label["similarity"][0:5]) or len(row1) !=1:
            print("error in merging csv")
            print (count)
            if count == 63 or count == 421:
                print()
            else:
                sys.exit(-1)
                
        featureRow=OrderedDict()
        newRow=copy.deepcopy(row1[0])
        createFeaturesdict(row1[0],featureRow,FeatureList) 
        if label in newRow.keys():
            newRow.pop(label)   
        if label in featureRow.keys():
            featureRow.pop(label)           
        newRow[label]= row_label[label]  
        featureRow[label]= row_label[label]          
        allRows.append(newRow)  
        featureRows.append(featureRow)  
    return featureRows,allRows    
def executeGM(input_csv1,input_csv2,feature_csv,all_csv,label): 
    
    featureRows,allRows= mergeCSV_GM(input_csv1,input_csv2,label)     
    colnames=allRows[0].keys() 
    colnamesFeatures=featureRows[0].keys()  
    file_handling.writeCsv(feature_csv, featureRows, colnamesFeatures)    
    file_handling.writeCsv(all_csv, allRows, colnames)  

    
      
    
if __name__ == '__main__':
    #createFeatureList() #done Once to create feature File
    section="MT1_MergefeaturesGC"
    #runGC(section) done for gun control
    
    section="MT1_MergefeaturesGM"
    #runGM(section) #done for gay marriage
    
    section="MT1_MergefeaturesDP"
    #runGM(section) #done for gay marriage
    
    section ="MT1_mergefeaturesdataGC"
    #runGC(section)
    
    section="MT1_mergefeatures_dataGCTrain"
    #runGC(section)
    
    section= "MT1_mergefeatures_dataGCTest"
    runGC(section)
    
    
    
 
 
 
 
 
 
 #----------------------------------- #exceuted once to createdeatureList for AFS
#------------------------------------------------------ def createFeatureList():
    # rowfeatures=file_handling.readCsv("/Users/amita/facet_similarity_part2/data/guncontrol/greater_than_55/argumentQuality/argumentPairs/mt1TrueLabel_gc_gt55_argQual_yes_gt3_comb_sim_max-sent-incl-10_sort_Top2000_features_norm.csv")
    # FileName="/Users/amita/facet_similarity_part2/data/gayMarriage/greater_than_55/argumentSimilarity/featureList.txt"
    #-------------------------------------------- features=rowfeatures[0].keys()
    #------------------------------------------------------------------ Lines=[]
    #-------------------------------------------------- for feature in features:
        #-------------------------------------------- Lines.append(feature+"\n")
    #------------------------------ file_handling.writeTextFile(FileName, Lines)   