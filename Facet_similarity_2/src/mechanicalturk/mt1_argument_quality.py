#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
Created on Oct 15, 2015
create mechanical turk input for argument quality

@author: amita
'''

from operator import itemgetter
import configparser
import random 
from utilities import file_handling


def runGC(section):
    args= readCongigFile(section)
    input_csv= args[0]
    output_file_sorted=args[1]
    args_mt1_bin=args[2]
    args_mt1=args[3]
    num_pairs_row=args[4]
    no_ofhits=args[5]
    sort_field_name=args[6]
    unique_field_name=args[7]
    random_seed=args[8]
    execute(input_csv,output_file_sorted,args_mt1_bin,args_mt1,num_pairs_row,no_ofhits,sort_field_name,unique_field_name,random_seed)


def readCongigFile(section):
    config = configparser.ConfigParser()
    config.read('mt1_argument_quality_Config.ini')
    input_file = config.get(section, 'input-file')
    output_file_sorted= config.get(section, 'output-file-sorted')
    args_mt1_bin=config.get(section, 'args_mt1_bin')
    args_mt1=config.get(section, 'args_mt1')
    num_pairs_row= int(config.get(section, 'num-pairs-row'))
    no_ofhits=int(config.get(section, 'no-ofhits'))
    sort_field_name=config.get(section, 'sort_field_name')
    unique_field_name=config.get(section, 'unique-field-name')
    random_seed = int(config.get(section, 'randomseed'))
    arguments=(input_file,output_file_sorted,args_mt1_bin,args_mt1,num_pairs_row,no_ofhits,sort_field_name,unique_field_name,random_seed)
    return  (arguments)
       
            
                
def getbinRows(unique_rows,binNo) :
    allRows=[]
    for row in unique_rows:
        binstr=str(row["bin"]).split(sep=";")
        if  str(binstr[0][-1])==str(binNo):
            newrow=row
            allRows.append(newrow)    
        
    return allRows    

# csv had an issue with special character in header field, replace by _
def removeperiodheader(row_dicts, char):
    new_rows=[]
    for row in row_dicts:
        newrow={}
        for colname, value in row.items():
            if char in colname:
                newcol=colname.replace(char, "_")
                newrow[newcol]=value
            else:
                newrow[colname]=row[colname]       
        new_rows.append(newrow)        
    return new_rows

def selectRandomRowsBin(unique_rows ):
    allBins=[]
    for binNo in reversed(range(0,5)):
        binrows=getbinRows(unique_rows,binNo)
        random.shuffle(binrows)
        if binNo==4:
            
            allBins.extend(binrows[0:len(binrows)])
        else:
            if binNo==3:
                allBins.extend(binrows[0:500])
            else:
                if binNo==2:
                    totalNow=len(allBins)
                    remainder=int(totalNow % 10)
                    needed=10 - remainder
                    additionalNeeded= 500+needed
                    allBins.extend(binrows[0:additionalNeeded])
                else:
                    allBins.extend(binrows[0:500])
         
    return allBins    
    
    



# defines overall flow of the script, see config file for description of arguments
def execute(input_csv,output_file_sorted,args_mt1_bin,args_mt1,num_pairs_row,No_ofhits,sort_field_name,unique_field_name,random_seed):
    random.seed(random_seed)
    all_rows=file_handling.readCsv(input_csv)
    all_rows=removeperiodheader(all_rows, ".")
    sorted_rows=sorted(all_rows, key=lambda x: float(itemgetter(sort_field_name)(x)), reverse=True)
    unique_rows=file_handling.addUniqueRowCol(sorted_rows)
    fieldnames=list(unique_rows[0].keys())
    file_handling.writeCsv(output_file_sorted,unique_rows,fieldnames)
    allBins = selectRandomRowsBin(unique_rows)
    fieldnames_bins=list(allBins[0].keys())
    file_handling.writeCsv(args_mt1_bin,allBins,fieldnames_bins )
    createHitInput(args_mt1_bin,args_mt1,num_pairs_row,No_ofhits,unique_field_name)
    

#change long csv to extended  argumentsfor MT input
#num_pairs_row: num pf pairs in a HIT
#args_mt1_bin:long input csv with all the records
#args_mt1: output csv expanded, combines records
# create fieldnames for combined file
def createHitInput(args_mt1_bin,args_mt1,num_pairs_hit,No_ofhits,unique_field_name):
    MTrow={}
    MTRows=[]
    intc=1
    extc=0
    rowcount=0
    row_dicts=file_handling.readCsv(args_mt1_bin)
    fieldnames=row_dicts[0].keys()
    for row in row_dicts:
        #=======================================================================
        # rowcount=rowcount +1   was for testing
        # if rowcount < 1050: 
        #     continue
        #=======================================================================
        if extc < int(No_ofhits):  
            if intc<=int(num_pairs_hit):
                for field in fieldnames:                
                    MTrow[field +"_"+str(intc)]=row[field]
                intc=intc+1       
                                     
            if intc-1==num_pairs_hit:
                MTRows.append(MTrow)   
                extc=extc+1
                intc=1
                MTrow=dict()
                
        else:
            break
        
    new_fieldnames=list(MTRows[0].keys())  
    file_handling.writeCsv(args_mt1,MTRows, new_fieldnames)       

def runGMScale(section):
    config = configparser.ConfigParser()
    config.read('mt1_argument_quality_Config.ini')
    input_file = config.get(section, 'no_Count_gt1_split_mt1_GM_results')
    output_file= config.get(section, 'mt2_FormattedGMqrgumentQualityScale')
    num_instances_hit=10
    no_ofhits=72
    unique_field_name=config.get(section, 'unique_field_name')    
    createHitInput(input_file,output_file,num_instances_hit,no_ofhits,unique_field_name)
    
    

if __name__ == '__main__':
    #section="MT1" # done for gun control
    #section="MT1DP"
    #runGC(section)
    #section="MT1GayMarriage"
    #runGC(section)
    
    section ="QualityGayMarriageScale"
    runGMScale(section)