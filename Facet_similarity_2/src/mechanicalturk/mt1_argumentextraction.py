'''
Created on Oct 22, 2015

@author: amita
'''

import configparser
from utilities import file_handling


def runGC(section):
    args= readCongigFile(section)
    input_csv= args[0]
    output_file=args[1]
    num_pairs_row=args[2]
    no_ofhits=args[3]
    execute(input_csv,output_file,num_pairs_row,no_ofhits)


def readCongigFile(section):
    config = configparser.ConfigParser()
    config.read('mt1_argument_extraction_Config.ini')
    input_file = config.get(section, 'input-file')
    output_file= config.get(section, 'output-file')
    num_pairs_row= int(config.get(section, 'num-pairs-row'))
    no_ofhits=int(config.get(section, 'no-ofhits'))
    arguments=(input_file,output_file,num_pairs_row,no_ofhits)
    return  (arguments)

def createHitInput(input_csv,mtformatted_csv,num_pairs_hit,No_ofhits):
    MTrow={}
    MTRows=[]
    intc=1
    extc=0
    row_dicts=file_handling.readCsv(input_csv)
    fieldnames=row_dicts[0].keys()
    for row in row_dicts:
        if extc < int(No_ofhits):  
            if intc<=int(num_pairs_hit):
                for field in fieldnames:                
                    MTrow[field +"_"+str(intc)]=row[field]
                intc=intc+1       
                                     
            if intc-1==num_pairs_hit:
                MTRows.append(MTrow)   
                extc=extc+1
                intc=1
                MTrow=dict()
                
        else:
            break
        
    new_fieldnames=list(MTRows[0].keys())  
    file_handling.writeCsv(mtformatted_csv,MTRows, new_fieldnames)       

def execute(input_csv,mtformatted_csv,num_pairs_hit,No_ofhits):
    createHitInput(input_csv,mtformatted_csv,num_pairs_hit,No_ofhits)
  
if __name__ == '__main__':
    section="MT1Argument Extraction"
    runGC(section)
