#!/usr/bin/env python3
#coding: utf8 
'''
Created on Oct 26, 2015
Take pairs file created using all pairs with arguments having a yes score > 4 for argument quality task, create a sorted file based on umbc, create a file of top 2000 records.
expand top 2000 file and create  mechanical turk input with a fixed number of pairs in a hit.
for mt2 we used 10 pairs in a hit, put 200 hits on turk
@author: amita
@author: amita
'''
from operator import itemgetter
import configparser
from utilities import file_handling
import random



def runGC(section):
    args= readCongigFile(section)
    input_csv= args[0]
    output_file_sorted=args[1]
    argpairs_topn=args[2]
    mtformatted_argpairs=args[3]
    num_pairs_hit=args[4]
    No_ofhits=args[5]
    sort_field_name=args[6]
    ignorefield=args[7]
    execute(input_csv,output_file_sorted,argpairs_topn,mtformatted_argpairs,num_pairs_hit,No_ofhits,sort_field_name,ignorefield)


def readCongigFile(section):
    config = configparser.ConfigParser()
    config.read('mt1_argumentqualitypairs_Config.ini')
    input_file = config.get(section, 'input-file')
    output_file_sorted= config.get(section, 'output-file-sorted')
    argpairs_top2000=config.get(section, 'argpairs-top2000')
    mtformatted_argpairs=config.get(section, 'argpairs-mtformatted')
    num_pairs_hit= int(config.get(section, 'num-pairs-hit'))
    no_ofhits=int(config.get(section, 'no-ofhits'))
    sort_field=config.get(section, 'sort-field')
    ignorefield=config.get(section, 'ignore-field')
    arguments=(input_file,output_file_sorted,argpairs_top2000,mtformatted_argpairs,num_pairs_hit,no_ofhits,sort_field,ignorefield)
    return  (arguments)


# ignores workerIds and Yes and No Count fields, specific to argument quality task
def getListfields(fieldnames):
    newfields=[]
    for field in fieldnames:
        if str(field).lower().startswith("id"):
            continue
        if str(field).lower().startswith("yes"):
            continue
        if str(field).lower().startswith("no"):
            continue
        if str(field).lower().startswith("hit"):
            continue
        newfields.append(field)
    
    return   newfields  

def createHitInput(input_csv,mtformatted_csv,num_pairs_hit,No_ofhits):
    MTrow={}
    MTRows=[]
    intc=1
    extc=0
    row_dicts=file_handling.readCsv(input_csv)
    fieldnames=row_dicts[0].keys()
    for row in row_dicts:
        if extc < int(No_ofhits):  
            if intc<=int(num_pairs_hit):
                for field in fieldnames:                
                    MTrow[field +"_"+str(intc)]=row[field]
                intc=intc+1       
                                     
            if intc-1==int(num_pairs_hit):
                MTRows.append(MTrow)   
                extc=extc+1
                intc=1
                MTrow=dict()
                
        else:
            break
        
    fieldnames=list(MTRows[0].keys())  
    
        
    file_handling.writeCsv(mtformatted_csv,MTRows, fieldnames)       


def execute(input_csv,output_file_sorted,argpairs_topn,argpairs_mtformatted,num_pairs_hit,No_ofhits,sort_field_name,ignorefield):
    all_rows=file_handling.readCsv(input_csv)
    sorted_rows=sorted(all_rows, key=lambda x: float(itemgetter(sort_field_name)(x)), reverse=True)
    fieldnames=sorted_rows[0].keys()
    if ignorefield=="True":
        new_fieldnames= getListfields(fieldnames)
    totalPairs=int(num_pairs_hit * No_ofhits)    
    file_handling.writeCsv(output_file_sorted,sorted_rows,new_fieldnames)
    topsortedrows=sorted_rows[:totalPairs]
    # use fixed seed 
    random.seed(1000)
    random.shuffle(topsortedrows)
    file_handling.writeCsv(argpairs_topn,topsortedrows,new_fieldnames)
    createHitInput(argpairs_topn,argpairs_mtformatted,num_pairs_hit,No_ofhits)


def readCongigFileDP(section):
    config = configparser.ConfigParser()
    config.read('mt1_argumentqualitypairs_Config.ini')
    argpairs_top2000=config.get(section, 'argpairs')
    argpairs_topn_Cols=config.get(section, 'argpairs-top2000')
    mtformatted_argpairs=config.get(section, 'argpairs-mtformatted')
    num_pairs_hit= int(config.get(section, 'num-pairs-hit'))
    no_ofhits=int(config.get(section, 'no-ofhits'))
    ignorefield=config.get(section, 'ignore-field')
    sort_field_name=config.get(section, 'sort_field_name')
    arguments=(argpairs_top2000,argpairs_topn_Cols,mtformatted_argpairs,num_pairs_hit,no_ofhits,ignorefield,sort_field_name)
    return  (arguments)
    

def executeDP(argpairs,argpairs_topn_sorted,argpairs_mtformatted,num_pairs_hit,No_ofhits,ignorefield,sort_field_name):
    all_rows=file_handling.readCsv(argpairs)
    sorted_rows=sorted(all_rows, key=lambda x: float(itemgetter(sort_field_name)(x)), reverse=True)
    totalPairs=int(num_pairs_hit * No_ofhits)    
    topsortedrows=sorted_rows[:totalPairs]
    fieldnames=list(all_rows[0].keys())
    if ignorefield=="True":
        new_fieldnames= getListfields(fieldnames)
    file_handling.writeCsv(argpairs_topn_sorted, topsortedrows,new_fieldnames)    
    createHitInput(argpairs_topn_sorted,argpairs_mtformatted,num_pairs_hit,No_ofhits)

def runDP(section):
    args= readCongigFileDP(section)
    argpairs=args[0]
    argpairs_topn_sorted=args[1]
    mtformatted_argpairs=args[2]
    num_pairs_hit=args[3]
    No_ofhits=args[4]
    ignorefield=args[5]
    sort_field_name=args[6]
    executeDP(argpairs,argpairs_topn_sorted,mtformatted_argpairs,num_pairs_hit,No_ofhits,ignorefield,sort_field_name)
        

if __name__ == '__main__':
    section="MT1argumentQualityPairs"
    #runGC(section)
    
    section="MT1argumentQualityPairsDP"
    runDP(section)