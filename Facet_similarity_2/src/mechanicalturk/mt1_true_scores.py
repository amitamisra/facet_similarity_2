#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
Created on Jul 7, 2015
calculate true scores for each pair.
Input: A list of workers that correlate well, 
       inputfile:mt1_splitresults_byhit.csv, created through split_mt1_results_pair.py 
       outputfile:file with true scores for similarity
@author: amita
'''

import  configparser
from utilities import file_handling
from random import shuffle

def runGC():
    args= readCongigFile()
    input_csv= args[0]
    output_csv=args[1]
    output_csv_random=args[2]
    good_worker_list=args[3]
    execute(input_csv,output_csv,output_csv_random,good_worker_list)


def readCongigFile():
    config = configparser.ConfigParser()
    config.read('mt1_true_scores_Config.ini')
    input_csv = config.get('GC', 'input_csv')
    output_csv= config.get('GC', 'output_csv')
    output_csv_random=config.get('GC', 'output_csv_random')
    good_worker_list=config.get('GC', 'good_worker_list').split(",")
    arguments=(input_csv,output_csv,output_csv_random,good_worker_list)
    return  (arguments)

# take average of workers in good worker list
def execute(input_csv,output_csv,output_csv_random,good_worker_list): 
    rowdicts= file_handling.readCsv(input_csv)
    New_rows=[]
    for row in rowdicts:
        score=float(0)
        new_dict={}
        annotation_count=0
        for key, value in row.items():
            if str(key).startswith("Id_"):
                if key in good_worker_list:
                    if value:
                        new_dict[key]=float(value)
                        score=float(value) + score
                        annotation_count=annotation_count+1
            else:
                new_dict[key]=value
                
        true_label= float( score)/annotation_count
        new_dict["regression_label"]=true_label
        new_dict["num_annotation"]=annotation_count
        New_rows.append(new_dict)  
    shuffle(New_rows)          
    file_handling.write_csv_header_unkown(output_csv, New_rows)
    
          
if __name__ == '__main__':
    runGC()
