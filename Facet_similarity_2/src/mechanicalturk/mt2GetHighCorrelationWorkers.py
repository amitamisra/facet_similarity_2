'''
Created on Sep 29, 2015

Initial Good workers were manually selected
To this add all workers who have R> Rcuttoff with any good worker => highCorrWorker_mt2.csv
calculate pairwise correlation for workers in highCorrWorker_mt2.csv
create a correlation file with only good workers=>goodCorr_mt2.csv
create average R for each good worker=>avgOfgoodCorr_mt2.
get avg R of all the workers
@author: amita
'''
from collections import defaultdict
import configparser
from utilities.file_handling import writeCsv
from utilities.file_handling import readCsv
from utilities.file_handling import write_csv_header_unkown

def readCongigFile():
    config = configparser.ConfigParser()
    config.read('mt2GethighCorrelationWorkerConfig.ini')
    input_file = config.get('GC', 'input-file')
    output_file= config.get('GC', 'output-file')
    gold_workers=config.get('GC', 'gold-worker').split(",")
    Rcutoff=float(config.get('GC', 'Rcutoff'))
    good_corr_file=config.get('GC', 'good-corr-file')
    avg_corr_file_goodWorker=config.get('GC','avg-corr-file-goodWorker')
    avg_corr_file_AllWorker=config.get('GC','avg-corr-file-AllWorker')
    avg_morethancutoffGood=config.get('GC','avg-morethancutoffGood')
    avg_morethancutoffAll=config.get('GC','avg-morethancutoffAll')
    arguments=(input_file,output_file,gold_workers,good_corr_file,avg_corr_file_goodWorker,avg_corr_file_AllWorker,Rcutoff,avg_morethancutoffGood,avg_morethancutoffAll)
    return  (arguments)

def runGC():
    args= readCongigFile()
    input_csv= args[0]
    output_csv=args[1]
    gold_workers=args[2]
    good_corr_file=args[3]
    avg_corr_file_goodWorker=args[4]
    avg_corr_file_Allworker=args[5]
    RCutoff=args[6]
    avg_morethancutoffGood=args[7]
    avg_morethancutoffAll=args[8]
    execute(input_csv,output_csv,good_corr_file,avg_corr_file_goodWorker,avg_corr_file_Allworker,avg_morethancutoffGood,avg_morethancutoffAll,gold_workers[0],gold_workers[1],gold_workers[2],RCutoff)


def checkcorr(row,worker,Rcutoff,goodworkerdictlist,goodworkerlist):
    if not row[worker]=="NA" :
        if float(row[worker]) >  float(Rcutoff):
            newdict={}
            newdict["goodworker"]=row["WorkerID"]
            goodworkerdictlist.append(newdict)
            goodworkerlist.append(row["WorkerID"])
            return True
    return False

      


#caluculate average correlation for each worker             
def calculateAvgCorrelation(good_corr_file):
    readcorr= readCsv(good_corr_file)
    countdict=defaultdict(int)
    valuedict=defaultdict(int)
    avgdict=defaultdict(int)
    for row in  readcorr:
        workerId=row["WorkerID"]
        for key,value in row.items():
            if str(key).startswith("WorkerID"):
                continue
            else:
                if str(value)=="NA":
                    continue
                else:
                    valuedict[workerId]= float(value) + valuedict[workerId]
                    countdict[workerId]=countdict[workerId]+1
                    avgdict[workerId]=float(valuedict[workerId]/countdict[workerId])
    return avgdict        
                                     



    
def execute(inputCsv,outputCsv,good_corr_file,avg_corr_file_goodWorker,avg_corr_file_Allworker,avg_morethancutoffGood,avg_morethancutoffAll,workerId1, workerId2,workerId3, Rcutoff):
    rows= readCsv(inputCsv)
    goodworkerlist=[]
    goodworkerdictlist=[]
    avglist_good=[]
    count=0;
    for row in rows:
        print(count)
        count=count+1
        if checkcorr(row,workerId1,Rcutoff,goodworkerdictlist,goodworkerlist):
            continue
        if checkcorr(row,workerId2,Rcutoff,goodworkerdictlist,goodworkerlist):
            continue
        if checkcorr(row,workerId3,Rcutoff,goodworkerdictlist,goodworkerlist):
            continue
               
        
    
    fields=goodworkerdictlist[0].keys()
    writeCsv(outputCsv,goodworkerdictlist,fields)
    goodworker_corr=getcorrgoodworker(goodworkerlist,rows)
    write_csv_header_unkown(good_corr_file,goodworker_corr)
    avgdict_good=calculateAvgCorrelation(good_corr_file)
    avglist_good.append(avgdict_good)
    write_csv_header_unkown(avg_corr_file_goodWorker,avglist_good)
    
    avgdictAll=calculateAvgCorrelation(inputCsv)
    avglistAll=[]
    avglistAll.append(avgdictAll)
    write_csv_header_unkown(avg_corr_file_Allworker,avglistAll)
    #getworkershigh_Avgcorr(avgdict_good,avgdictAll,avg_morethancutoffGood,avg_morethancutoffAll,Rcutoff)


# create a correlation file with only good worker list 
def getcorrgoodworker(goodworkerlist,rows):
    goodcorr=[]
    for row in rows:
        if row["WorkerID"] in goodworkerlist:
            newdict={}
            for key,value in row.items():
                if key  in goodworkerlist:
                    newdict[key]=row[key]
                    newdict["WorkerID"]=row["WorkerID"] 
                else:
                    print()   
            goodcorr.append(newdict)
            
    return   goodcorr      



if __name__ == '__main__':
    runGC()
    
    
    
# def getworkershigh_Avgcorr(avgdict_good,avgdictAll,avg_morethancutoffGood,avg_morethancutoffAll,Rcutoff):
    #-------------------------------------------- avg_morethancutoff_goodlist=[]
    #--------------------------------------------- avg_morethancutoff_Alllist=[]
    #----------------------------------- for key, value in avgdict_good.items():
        #-------------------------------------- if float(value)> float(Rcutoff):
            #-------------------------------------------------------- newdict={}
            #------------------------------------------- newdict["WorkerID"]=key
            #----------------------- newdict["AvgPairWiseCorr_goodWorker"]=value
            #----------------------- avg_morethancutoff_goodlist.append(newdict)
    #------------------------------------- for key, value in avgdictAll.items():
        #-------------------------------------- if float(value)> float(Rcutoff):
            #-------------------------------------------------------- newdict={}
            #------------------------------------------- newdict["WorkerID"]=key
            #------------------------ newdict["AvgPairWiseCorr_AllWorker"]=value
            #------------------------ avg_morethancutoff_Alllist.append(newdict)
#------------------------------------------------------------------------------ 
    #---------------------------- fieldsAll=avg_morethancutoff_Alllist[0].keys()
    #-------------------------- fieldsGood=avg_morethancutoff_goodlist[0].keys()
    #--- writeCsv(avg_morethancutoffGood,avg_morethancutoff_goodlist,fieldsGood)
    #------ writeCsv(avg_morethancutoffAll,avg_morethancutoff_Alllist,fieldsAll)