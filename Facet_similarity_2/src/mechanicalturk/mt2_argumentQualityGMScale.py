#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
NOT USED

@author: amita
'''

from operator import itemgetter
import configparser
import random 
from utilities import file_handling

def runGC(section):
    args= readCongigFile(section)
    input_csv= args[0]
    input_mt2Scale=args[1]
    formatted_mt2Scale=args[2]
    num_instances_row=args[3]
    no_ofhits=args[4]
    unique_field_name=args[5]
    random_seed=args[6]
    execute(input_csv,input_mt2Scale,formatted_mt2Scale,num_instances_row,no_ofhits,unique_field_name,random_seed)


def readCongigFile(section):
    config = configparser.ConfigParser()
    config.read('mt1_argument_quality_Config.ini')
    input_csv = config.get(section, 'input-file')
    input_mt2Scale= config.get(section, 'output-file-sorted')
    formatted_mt2Scale=config.get(section, 'args_mt1_bin')
    num_instances_row= int(config.get(section, 'num-pairs-row'))
    no_ofhits=int(config.get(section, 'no-ofhits'))
    unique_field_name=config.get(section, 'unique-field-name')
    random_seed = int(config.get(section, 'randomseed'))
    arguments=(input_csv,input_mt2Scale,formatted_mt2Scale,num_instances_row,no_ofhits,unique_field_name,random_seed)
    return  (arguments)

def execute(input_csv,input_mt2Scale,formatted_mt2Scale,num_instances_row,no_ofhits,unique_field_name,random_seed,class2count,class3count):
    random.seed(random_seed)
    all_rows=file_handling.readCsv(input_csv)
    class1=[row for row in all_rows if int(row["No_Count"]) >1]
    class2=[row for row in all_rows if int(row["No_Count"]) == 1]
    class3=[row for row in all_rows if int(row["No_Count"]) == 0]
    randomclass2=random.shuffle(class2)
    randomclass3=random.shuffle(class3)
    alllist=class1+randomclass2[0:class2count] + randomclass3[0:class3count]
    #file_handling.writeCsv(outputcsv, alllist, colnames)
    
    


if __name__ == '__main__':
    pass