'''
Created on Jul 7, 2015
calculate true scores for each pair.
Input: A list of workers that correlate well, 
       inputfile:mt1_splitresults_byhit.csv, created through split_mt1_results_pair.py 
       outputfile:file with true scores for similarity
@author: amita
'''

import  configparser
from utilities import file_handling
from random import shuffle

def runGC():
    args= readCongigFile()
    input_csv= args[0]
    output_csv=args[1]
    good_worker_file=args[2]
    execute(input_csv,output_csv,good_worker_file)


def readCongigFile():
    config = configparser.ConfigParser()
    config.read('mt1_true_scores_Config.ini')
    input_csv = config.get('GCMT2', 'input_csv')
    output_csv= config.get('GCMT2', 'output_csv')
    good_worker_file=config.get('GCMT2', 'good_worker_file')
    arguments=(input_csv,output_csv,good_worker_file)
    return  (arguments)

def execute(input_csv,output_csv,good_worker_file): 
    rowdicts= file_handling.readCsv(input_csv)
    good_worker_rows=file_handling.readCsv(good_worker_file)
    good_worker_list=[row["goodworker"] for row in good_worker_rows]
    New_rows=[]
    badrows=0
    for row in rowdicts:
        score=float(0)
        new_dict={}
        annotation_count=0
        for key, value in row.items():
            if str(key).startswith("Id_"):
                if key in good_worker_list:
                    if value:
                        new_dict[key]=float(value)
                        score=float(value) + score
                        annotation_count=annotation_count+1
            else:
                new_dict[key]=value
        if annotation_count==0:
            print(row["uniqueRowNo"])
            badrows=badrows+1 
            continue
            
                
        true_label= float( score)/annotation_count
        new_dict["regression_label"]=true_label
        new_dict["num_annotation"]=annotation_count
        New_rows.append(new_dict)  
    file_handling.write_csv_header_unkown(output_csv, New_rows)
    print("bad rows"+ str(badrows) )  
          
if __name__ == '__main__':
    runGC()
