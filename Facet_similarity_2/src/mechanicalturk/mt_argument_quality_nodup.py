'''
Created on Feb 17, 2016
create  a sample to check argument quality of unfiltered data, but remove duplicates
@author: amita
'''
import configparser
import random 
from utilities import file_handling
from collections import defaultdict
from mechanicalturk import mt1_argumentqualitypairsGC
    
def run(section):
    args= readCongigFile(section)
    input_csv= args[0]
    mt_bin=args[1]
    mt_bin_formatted=args[2]
    num_instances_row=args[3]
    no_ofhits=args[4]
    unique_field_name=args[5]
    no_of_items_bin=args[6]
    random_seed=args[7]
    execute(input_csv,mt_bin,mt_bin_formatted,num_instances_row,no_ofhits,unique_field_name,no_of_items_bin,random_seed)


def readCongigFile(section):
    config = configparser.ConfigParser()
    config.read('mt1_argument_quality_nodup_Config.ini')
    input_csv = config.get(section, 'input_csv')
    mt_bin= config.get(section, 'mt_bin')
    mt_bin_formatted=config.get(section, 'mt_bin_formatted')
    num_instances_row= int(config.get(section, 'num_instances_row'))
    no_ofhits=int(config.get(section, 'no_ofhits'))
    unique_field_name=config.get(section, 'unique_field_name')
    random_seed = int(config.get(section, 'randomseed'))
    no_of_items_bin=int(config.get(section, 'no_of_items_bin'))
    arguments=(input_csv,mt_bin,mt_bin_formatted,num_instances_row,no_ofhits,unique_field_name,no_of_items_bin,random_seed)
    return  (arguments)
       

def createrandombinlist( bindict,no_of_items_bin):
    allbinlist =[]
    for key,binlist in bindict.items():
        random.shuffle(binlist)
        allbinlist.extend(binlist[0:no_of_items_bin])
    return allbinlist    
    
            
def execute(input_csv,mt_bin,mt_bin_formatted,num_instances_row,no_ofhits,unique_field_name,no_of_items_bin,random_seed):
    random.seed(random_seed)
    rows=file_handling.readCsv(input_csv)
    bindict=getbinRows(rows)
    allbinlist=createrandombinlist(bindict,no_of_items_bin)
    unique_rows=file_handling.addUniqueRowCol(allbinlist)
    colnames=sorted(unique_rows[0].keys())
    file_handling.writeCsv(mt_bin, unique_rows, colnames)
    mt1_argumentqualitypairsGC.createHitInput(mt_bin, mt_bin_formatted, num_instances_row, no_ofhits)
    
                 
def getbinRows(rows) :
    bindict=defaultdict(list)
    for row in rows:
        binstr=str(row["bin"]).split(sep=";")
        bin_no=binstr[0][-1]
        bindict[bin_no].append(row)
    return bindict 
if __name__ == '__main__':
    section="gc"
    run(section)