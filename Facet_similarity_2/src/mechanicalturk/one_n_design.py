# Transforms pair design into 1 vs. N design. #
# The scores of _0 should be ignored.

import  random, pprint, argparse
from utilities import  file_handling as fh
from copy import deepcopy

# Remove a character from column name, so that MT can read the output csv
def remove_char_from_header(rowdicts, char):
    for row in rowdicts:
        for key, val in row.items():
            if char in key:
                newkey = key.replace(char, '_')
                del row[key]
                row[newkey] = val

# Add the sentence to be original and its associated data.
def collectOrig(group, row, sent_label):
    group['sentence_0'] = row['sentence_'+sent_label]
    partner_label = 'b' if sent_label == 'a' else 'a'
    for label in labels:
        if label[-2:] == '_'+sent_label:
            group[label[:-2]+'_'+str(0)] = row[label]
        elif label[-2:] == '_'+partner_label:
            pass
        else:
            group[label+'_'+str(0)] = row[label]

# Collect the candidate sentences and remove if excess. 
def collectCand(group, row, sent_label):
    num = 0
    sent_orig = row['datasetId_'+sent_label]  + row['discussionId_'+sent_label] +\
                row['sentenceId_'+sent_label] + row['postId_'+sent_label]
    for j,e in enumerate(rows):
        label_cand=label_orig=''
        # Add as candidate if one in the pair is the original,
        #    and the other is unused.
        sent_a = e['datasetId_a'] + e['discussionId_a'] +\
                 e['sentenceId_a'] + e['postId_a']
        sent_b = e['datasetId_b'] + e['discussionId_b'] +\
                 e['sentenceId_b'] + e['postId_b']
        if sent_a == sent_orig and sent_b not in sents_used:
            label_orig = 'a'
            label_cand = 'b'
            sents_used.add(sent_b)
        elif sent_b == sent_orig and sent_a not in sents_used:
            label_orig = 'b'
            label_cand = 'a'
            sents_used.add(sent_a)
        else:
            continue
        
        # Add candidate to group, attaching serial number.
        num += 1
        group['sentence_' + str(num)] = e['sentence_'+label_cand]
        # Add candidate's associated data.
        for label in labels:
            if 'sentence_' in label:
                continue
            if label[-2:] == '_'+label_cand:
                group[label[:-2] + '_' + str(num)] = e[label]
            elif label[-2:] == '_'+label_orig:
                pass
            else:
                group[label + '_' + str(num)] = e[label]

    # If collected too many similar sentences, remove excess randomly.
    if num > maxt:
        reduce(num, group)
    # If collected too few similar sentences,  
    if num < maxt:
        for i in range(num+1):
            sent=''
            sent = group['datasetId_'+str(i)]  + group['discussionId_'+str(i)] +\
                   group['sentenceId_'+str(i)] + group['postId_'+str(i)]
            sents_used.discard(sent)
        return False
    
    # Each group should have the same keys.
    u_keys = set()
    for key in group.keys():
        keyname = key.split('_')
        u_key = '_'.join(keyname[:-1])
        u_keys.add(u_key)
    for u_key in u_keys:
        for i in range(maxt+1):
            key_i  = u_key+'_'+str(i)
            if key_i not in group.keys():
                group[key_i] = ''
    return True

# Reduces the candidates in a group to maxt,
# recycling the randomly discarded ones
def reduce(num, group):
    excess = random.sample(range(1,num), num-maxt)
    group_cp = deepcopy(group)
    for key,val in group_cp.items():
        if int(key.split('_')[-1]) in excess:
            # Take the deleted sentences out of the used set,
            #    so they are available again.
            sents_used.discard(group[key])
            # Delete the item from group.
            del group[key]

    # The indices of candidates should be in order from 1 to maxt 
    wanted = list(range(1, maxt+1))
    in_grp = list(range(1, num+1))
    remain = sorted(set(in_grp) - set(excess))
    for i in range(maxt):
        for key in group.keys():
            keyname = key.split('_')
            if int(keyname[-1]) == remain[i]:
                keyname[-1] = str(wanted[i])
                group['_'.join(keyname)] = group.pop(key)

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Transform pair into 1 to N design.')
    parser.add_argument('infile', metavar='infile', nargs=1,\
                        help='the input csv file to process')
    parser.add_argument('N', metavar='N', type=int, nargs=1,\
                        help='an integer for the number of candidates')
    parser.add_argument('outfile', metavar='outfile', nargs=1,\
                        help='the output csv file')
    args = parser.parse_args()
    
    maxt = args.N[0]
    infile = args.infile[0]
    outfile = args.outfile[0]
    rows = fh.readCsv(infile)
    labels = list(rows[0].keys())
    sents_used = set()
    u_sents = set()
    one_n = []

    # Collect all the unique sentences by their combination of ID's
    for row in rows:
        sent_a = row['datasetId_a']  + row['discussionId_a'] +\
                row['sentenceId_a'] + row['postId_a']
        sent_b = row['datasetId_b']  + row['discussionId_b'] +\
                row['sentenceId_b'] + row['postId_b']
        u_sents.add(sent_a)
        u_sents.add(sent_b)

    # Loop to ensure that we get the as many groups as possible
    print(len(u_sents))
    for i in range(int(len(u_sents)/(maxt+1))):
        for row in rows:
            sent_a = row['datasetId_a']  + row['discussionId_a'] +\
                     row['sentenceId_a'] + row['postId_a']
            sent_b = row['datasetId_b']  + row['discussionId_b'] +\
                     row['sentenceId_b'] + row['postId_b']
            # If one is used and the other is not, use the unused one as original;
            #    if both are unused, choose randomly;
            #    if both are used, continue.
            if sent_a in sents_used and sent_b not in sents_used:
                sent_label = 'b'
                sent_chosen = sent_b
            elif sent_b in sents_used and sent_a not in sents_used:
                sent_label = 'a'
                sent_chosen = sent_a
            elif sent_a not in sents_used and sent_b not in sents_used:
                sent_label = random.choice(['a', 'b'])
                sent_chosen = sent_a if sent_label == 'a' else sent_b
            else: 
                continue
            
            # The selected original cannot be used again.
            sents_used.add (sent_chosen)
            group = dict()
            # Collect the original and candidates of each group
            collectOrig(group, row, sent_label)
            if collectCand(group, row, sent_label):
                one_n.append(group)

    remove_char_from_header(one_n, '*')
    remove_char_from_header(one_n, '.')
    
    pp = pprint.PrettyPrinter(indent=4)
    pp.pprint(one_n)
    print('Groups: ', len(one_n))
    print('Total unique sentences: ', len(u_sents))
    print('Leftover sentences: ', len(u_sents) - (maxt+1) * len(one_n))
    
    if (len(one_n)>0):
        fieldnames = one_n[0].keys()
        fh.writeCsv(outfile, one_n, fieldnames)
