#!/usr/bin/env python3
# -*- coding: utf-8 -*-
'''
Created on Dec 7, 2015

@author: amita
'''
import operator
import itertools
from collections import defaultdict
import  configparser
import sys,os
from utilities import file_handling
from copy import deepcopy

def updaterow(row, workerId):
    HITId=row["HITId"]
    uniqueRowNo=row["uniqueRowNo"]
    response=getmisingresponse(workerId,HITId,uniqueRowNo,missingresponses)
    row["Id_"+ workerId]=response
    updatescales(row,workerId)
    
def updatescales(row,workerId) :
    if int(row["Id_"+ workerId]) == 3 :
                            row['scale3_Count']=int(row['scale3_Count']) +1
    else:        
        if int(row["Id_"+ workerId]) < 3:  
                            row['scale2_1_Count']=int(row['scale2_1_Count'])+1    

def mergeMissingHITS(all_rows_extra,missingFileDPArQualScale):
    for row in all_rows_extra:
        if "Id_A1SWV4X4PD25S1"  not in row.keys():
            updaterow(row,"A1SWV4X4PD25S1")
        if "Id_A2DLH5XGBNYXWS" not in row.keys():
            updaterow(row,"A2DLH5XGBNYXWS")  
             
def getmisingresponse(WorkerId,HITId,uniqueRowNo,missingFile):
    row_dicts=file_handling.readCsv(missingFile)
    row=[rowmatch for rowmatch in row_dicts if rowmatch["WorkerId"]==WorkerId and rowmatch["HITId"]==HITId and rowmatch["uniqueRowNo"]==str(uniqueRowNo)]
    #assert len(row) == 1 
    if len(row)!=1:
        print("error in " + "WorkerId "+ WorkerId  + "uniqueRow  "+ str(uniqueRowNo)+ "in getmisingresponse")
        sys.exit(-1)
    else:
        return row[0]["response"]    
    

def getbinNumber(row) :
        binstr=str(row["bin"]).split(sep=";")
        return str(binstr[0][-1])


#remove extra rows from bin2. These were added to make instances as multiple of 10 for mechanical turk for death penalty
def selectRandomRowsBin(all_rows ):
    new_rows=[]
    bin2_rows=[]
    scale3_gt_1allrows=[]
    scale2_1_gt_1allrows=[]
    for row in all_rows:
        binno= getbinNumber(row)
        if binno in ["0","1","3","4","5"]:  
            new_rows.append(row)
        else:
            bin2_rows.append(row)    
    new_rows=new_rows+ bin2_rows[0:500]   
    
    for nrow in new_rows:
        if int(nrow['scale2_1_Count']) > 1:
                    scale2_1_Count_gt1 = deepcopy(nrow)
                    scale2_1_gt_1allrows.append(scale2_1_Count_gt1)  
        if int(nrow['scale3_Count']) > 1:
                    scale3_Count_gt1 = deepcopy(nrow)
                    scale3_gt_1allrows.append(scale3_Count_gt1)     
    
    return new_rows,scale3_gt_1allrows,scale2_1_gt_1allrows



def splitRowPairwise(rowdicts,noof_items_hit,missingresponses):
    AllRows=list()
    Worker_rows=[]
    
    rowdicts.sort(key=operator.itemgetter("HITId"))
    for dictkey, items in itertools.groupby(rowdicts, operator.itemgetter("HITId")):
        Hitids=list(items)
        for count in range(1,noof_items_hit+1):
            NewDict=defaultdict()
            NewDict['scale3_Count']=0
            NewDict['scale2_1_Count']=0
            for row in Hitids:
                    
                    if str(row["AssignmentStatus"]).lower()=="rejected":
                        continue
                    if not row['Answer.response'+ str(count)]:
                        response=getmisingresponse(row["WorkerId"],row["HITId"],row["Input.uniqueRowNo_"+str(count)],missingresponses)
                        NewDict["Id_"+ row["WorkerId"]]=int(response)
                    else:
                        NewDict["Id_"+ row["WorkerId"]] = int(row['Answer.response'+ str(count)] )
                        
                    if int(NewDict["Id_"+ row["WorkerId"]]) == 3 :
                            NewDict['scale3_Count']=int(NewDict['scale3_Count']) +1
                    else:        
                        if int(NewDict["Id_"+ row["WorkerId"]]) < 3:  
                            NewDict['scale2_1_Count']=int(NewDict['scale2_1_Count'])+1 
                
                             
            for fieldname in row.keys():
                if str(fieldname).startswith("Input.") :
                    indexOfscore=str(fieldname).rfind("_")
                    newkey=fieldname[6:indexOfscore]
                    NewDict[newkey]=row["Input."+newkey+"_"+ str(count)]  
            NewDict["HITId"]=row["HITId"]
            NewDict["HITTypeId"]=row["HITTypeId"] 
                
           # Worker_rows.append(response_dict)
            AllRows.append(NewDict)
            
             
    return (AllRows)

def runDP(section):
    args= readCongigFileDP(section)
    mtResults_csv= args[0]
    split_mtcsv=args[1]
    worker_input=args[2]
    scale3_gt1=args[3]
    scale2_1_gt1=args[4]
    noof_items_hit= int(args[5])
    executeDP(mtResults_csv,split_mtcsv,worker_input,scale3_gt1,scale2_1_gt1,noof_items_hit)
    
    
def executeDP(mtResults_csv,split_mtcsv,worker_input,scale3_gt1,scale2_1_gt1,noof_items_hit):
    rowdicts= file_handling.readCsv(mtResults_csv)
    all_rows_extra=splitRowPairwise(rowdicts,noof_items_hit,missingresponses)
    mergeMissingHITS(all_rows_extra,missingresponses)
    all_rows_tuple=selectRandomRowsBin(all_rows_extra)
    all_rows=all_rows_tuple[0]
    scale3_gt_1allrows= all_rows_tuple[1]
    scale2_1_gt_1allrows=all_rows_tuple[2]
    #file_handling.write_csv_header_unkown(worker_input, worker_response_rows)
    file_handling.write_csv_header_unkown(split_mtcsv,all_rows)
    file_handling.write_csv_header_unkown(scale3_gt1,scale3_gt_1allrows)
    file_handling.write_csv_header_unkown(scale2_1_gt1,scale2_1_gt_1allrows)

def readCongigFileDP(section):
    config = configparser.ConfigParser()
    config.read('splitAgumentQualityScale_Config.ini')
    mt_results_csv = config.get(section, 'mt_results_csv')
    mt_split_csv= config.get(section, 'mt_split_csv')
    worker_input=config.get(section, 'mt_worker_input_csv')
    scale3_gt1=config.get(section,"scale3_gt1")
    scale2_1_gt1=config.get(section,"scale2_1_gt1")
    noof_items_hit=config.get(section,"noof_items_hit")
    arguments=(mt_results_csv,mt_split_csv,worker_input,scale3_gt1,scale2_1_gt1,noof_items_hit)
    return arguments



def getScale3_Scale1_2(new_rows):
    scale2_1_gt_1allrows=[]
    scale3_gt_1allrows=[]
    for nrow in new_rows:
            if int(nrow['scale2_1_Count']) > 1:
                        scale2_1_Count_gt1 = deepcopy(nrow)
                        scale2_1_gt_1allrows.append(scale2_1_Count_gt1)  
            if int(nrow['scale3_Count']) > 1:
                        scale3_Count_gt1 = deepcopy(nrow)
                        scale3_gt_1allrows.append(scale3_Count_gt1)   
    return (scale3_gt_1allrows,scale2_1_gt_1allrows)


def mergeResultsA2D_worker(results_A2D_worker,all_rows,WorkerId):
    workerrows=file_handling.readCsv(results_A2D_worker)
    for row in all_rows:
        uniqueRowNo=row["uniqueRowNo"]
        HITId=row["HITId"]
        rowwork=[rowmatch for rowmatch in workerrows if rowmatch["WorkerId"]==WorkerId and rowmatch["uniqueRowNo"]==str(uniqueRowNo) and rowmatch["HITId"]==HITId ]
    #assert len(row) == 1 
        if len(rowwork)!=1:
            print("error in " + "WorkerId "+ WorkerId  + "uniqueRow  "+ str(uniqueRowNo))
            sys.exit(-1)
        if  "Id_"+ WorkerId not in row.keys():
            row["Id_"+ WorkerId]=int(rowwork[0]["response"])
            updatescales(row,WorkerId)
        
    return all_rows


def executeGMScale(mt2resultsFormatted,results_A2D_worker,split_mtcsv,worker_input,scale3_gt1,scale2_1_gt1,noof_items_hit):
    rowdicts= file_handling.readCsv(mt2resultsFormatted)
    all_rows=splitRowPairwise(rowdicts,noof_items_hit,missingresponses)
    WorkerIdA2D="A2DLH5XGBNYXWS"
    mergeResultsA2D_worker(results_A2D_worker,all_rows,WorkerIdA2D)
    scale3,scale2=getScale3_Scale1_2(all_rows)
    file_handling.write_csv_header_unkown(split_mtcsv, all_rows)
    file_handling.write_csv_header_unkown(scale3_gt1,scale3)
    file_handling.write_csv_header_unkown(scale2_1_gt1,scale2)
    
def readCongigFileGM(section):
    config = configparser.ConfigParser()
    config.read('splitAgumentQualityScale_Config.ini')
    mt_results_csv = config.get(section, 'mt_results_csv')
    workerA2Dresults = config.get(section, 'workerA2Dresults')
    mt_split_csv= config.get(section, 'mt_split_csv')
    worker_input=config.get(section, 'mt_worker_input_csv')
    scale3_gt1=config.get(section,"scale3_gt1")
    scale2_1_gt1=config.get(section,"scale2_1_gt1")
    noof_items_hit=config.get(section,"noof_items_hit")
    arguments=(mt_results_csv,workerA2Dresults,mt_split_csv,worker_input,scale3_gt1,scale2_1_gt1,noof_items_hit)
    return arguments
     
def runGMScale(section):
    args= readCongigFileGM(section)
    mt2resultsFormatted=args[0]
    results_A2D_worker=args[1]
    split_mtcsv=args[2]
    worker_input=args[3]
    scale3_gt1=args[4]
    scale2_1_gt1=args[5]
    noof_items_hit= int(args[6])
    executeGMScale(mt2resultsFormatted,results_A2D_worker,split_mtcsv,worker_input,scale3_gt1,scale2_1_gt1,noof_items_hit)
    

def createcorrelation(inputcsv,outputcsv,workerlist):
    inrows=file_handling.readCsv(inputcsv)
    file_handling.writeCsv(outputcsv,inrows, workerlist)
   

def readCongigFileGC(section):
    config = configparser.ConfigParser()
    config.read('splitAgumentQualityScale_Config.ini')
    mt_results_csv = config.get(section, 'mt_results_csv')
    mt_split_csv= config.get(section, 'mt_split_csv')
    worker_input_csv=config.get(section, 'mt_worker_input_csv')
    scale3_gt1=config.get(section,"scale3_gt1")
    scale2_1_gt1=config.get(section,"scale2_1_gt1")
    noof_items_hit=config.get(section,"noof_items_hit")
    arguments=(mt_results_csv,mt_split_csv,worker_input_csv,scale3_gt1,scale2_1_gt1,noof_items_hit)
    return arguments

def runGCScale(section):
    args= readCongigFileGC(section)
    mtresultsFormatted=args[0]
    split_mtcsv=args[1]
    worker_input=args[2]
    scale3_gt1=args[3]
    scale2_1_gt1=args[4]
    noof_items_hit= int(args[5])
    executeGCScale(mtresultsFormatted,split_mtcsv,worker_input,scale3_gt1,scale2_1_gt1,noof_items_hit)
        

def executeGCScale(mt2resultsFormatted,split_mtcsv,worker_input,scale3_gt1,scale2_1_gt1,noof_items_hit):
    rowdicts= file_handling.readCsv(mt2resultsFormatted)
    all_rows=splitRowPairwise(rowdicts,noof_items_hit,missingresponses)
    scale3,scale2=getScale3_Scale1_2(all_rows)
    file_handling.write_csv_header_unkown(split_mtcsv, all_rows)
    file_handling.write_csv_header_unkown(scale3_gt1,scale3)
    file_handling.write_csv_header_unkown(scale2_1_gt1,scale2)
         
def runcorrsection(section):
    input1="/Users/amita/facet_similarity_part2/data/gayMarriage/mechanicalTurk/greater_than_55/argumentQuality/results_mt2/mt2_split_gm_gt55_argQualScale.csv"
    output1="/Users/amita/facet_similarity_part2/data/gayMarriage/mechanicalTurk/greater_than_55/argumentQuality/results_mt2/mt2_split_gm_gt55_argQualScale_Corr.csv"
    input2="/Users/amita/facet_similarity_part2/data/deathPenalty/mechanicalTurk/greater_than_55/argumentQuality/Results/mt1_split_dp_gt55_argQualScale.csv"
    output2="/Users/amita/facet_similarity_part2/data/deathPenalty/mechanicalTurk/greater_than_55/argumentQuality/Results/mt1_split_dp_gt55_argQualScale_Corr.csv"
    workerlist=["Id_A1SWV4X4PD25S1","Id_A2DLH5XGBNYXWS","Id_ADUJUZANFOWKW"]
    createcorrelation(input1,output1,workerlist)
    createcorrelation(input2,output2,workerlist)
    


if __name__ == '__main__':
    #section="argumentQualityScaleDP"
    #missingresponses="/Users/amita/facet_similarity_part2/data/deathPenalty/mechanicalTurk/greater_than_55/argumentQuality/Results/HMWmt1ArgQualScaleDPMissingFile.csv"
    #runDP(section)
    
    #section="argumentQualityScaleGM"
    #missingresponses="/Users/amita/facet_similarity_part2/data/gayMarriage/mechanicalTurk/greater_than_55/argumentQuality/results_mt2/missingResponsesM2_GMScale.csv"
    #runGMScale(section)
    
    #section="correlation"
    #runcorrsection(section)
    
    section="argumentQualityScaleGCNofilters"
    missingresponses=""
    runGCScale(section)
    
    