#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
Created on Oct 18, 2015

@author: amita
'''
import operator
import itertools
from collections import defaultdict
import  configparser
import sys,os
from utilities import file_handling
from copy import deepcopy

def run(section):
    args= readCongigFile(section)
    input_csv= args[0]
    mt_input_csv=args[1]
    output_csv=args[2]
    worker_input=args[3]   
    yesCount_gt3=args[4]
    count_worker_instances=args[5]
    noof_items_hit= int(args[6])
    execute(input_csv,mt_input_csv,output_csv,worker_input,yesCount_gt3,count_worker_instances,noof_items_hit)


def readCongigFile(section):
    config = configparser.ConfigParser()
    config.read('split_mt1_argument_quality_Config.ini')
    input_csv = config.get(section, 'input_csv')
    mt_input_csv=config.get(section, 'mt_input_csv')
    output_csv= config.get(section, 'output_csv')
    noof_items_hit=config.get(section, 'noof_items_hit')
    worker_input=config.get(section,'worker_input')
    yesCount_gt3=config.get(section,'yesCount_gt3')
    count_worker_instances=config.get(section,'count_worker_instances')
    arguments=(input_csv,mt_input_csv,output_csv,worker_input,yesCount_gt3,count_worker_instances,noof_items_hit)
    return  (arguments)

def getcountinstancesworker(Worker_response_rows):
    allworkerinfo=[]
    workercountdict=defaultdict(int)
    for row in Worker_response_rows:
        for key,value in row.items():
            if str(key).startswith("Id"):
                if value == 1 or value == -1:
                    workercountdict[key]=workercountdict[key]+1
            
    allworkerinfo.append(workercountdict)
    newdict=defaultdict(int)
    for key,value in workercountdict.items():
        if value > 200:
            newvalue=float(value)/200.0 
            newdict[key]=newvalue
        
    allworkerinfo.append(newdict)    
        
    return allworkerinfo  

def splitRowPairwise(rowdicts,noof_items_hit,yescount):
    AllRows=list()
    Worker_rows=[]
    yesCount_gt3_allrows=[]
    rowdicts.sort(key=operator.itemgetter("HITId"))
    for dictkey, items in itertools.groupby(rowdicts, operator.itemgetter("HITId")):
        Hitids=list(items)
        for count in range(1,noof_items_hit+1):
            NewDict=defaultdict()
            NewDict['Yes_Count']=0
            NewDict['No_Count']=0
            response_dict={}
            for row in Hitids:
                    if str(row["AssignmentStatus"]).lower()=="rejected":
                        continue
                    NewDict["Id_"+ row["WorkerId"]]=row['Answer.respose'+ str(count)]
                    if row['Answer.respose'+ str(count)].lower() == 'yes' :
                            NewDict['Yes_Count']=NewDict['Yes_Count']+1
                            response_dict["Id_"+ row["WorkerId"]]=1
                    else:        
                        if row['Answer.respose'+ str(count)].lower() =='no':  
                            NewDict['No_Count']=NewDict['No_Count']+1 
                            response_dict["Id_"+ row["WorkerId"]]=-1
                        else:
                            response_dict["Id_"+ row["WorkerId"]]="NA"
                                
                             
            for fieldname in row.keys():
                if str(fieldname).startswith("Input.") :
                    indexOfscore=str(fieldname).rfind("_")
                    newkey=fieldname[6:indexOfscore]
                    NewDict[newkey]=row["Input."+newkey+"_"+ str(count)]  
            NewDict["HITId"]=row["HITId"]
            NewDict["HITTypeId"]=row["HITTypeId"] 
            
            if int(NewDict['Yes_Count']) > yescount:
                yesCount_gt3 = deepcopy(NewDict)
                yesCount_gt3_allrows.append(yesCount_gt3)
            Worker_rows.append(response_dict)
            AllRows.append(NewDict)
            
             
    return (AllRows, Worker_rows,yesCount_gt3_allrows)
        
def execute(input_csv,mt_input_csv,output_csv,worker_input,yesCount_gt3,count_worker_instances,noof_items_hit): 
    rowdicts= file_handling.readCsv(input_csv)
    new_rows_tuple=splitRowPairwise(rowdicts,noof_items_hit)
    all_rows=new_rows_tuple[0]
    worker_response_rows=new_rows_tuple[1]
    yesCount_gt3_allrows= new_rows_tuple[2]
    #Worker_response_rowsCols=[key for key in Worker_response_rows[0].keys() if str(key).startswith("Id")]
    worker_countinfo=getcountinstancesworker(worker_response_rows)
    
    file_handling.write_csv_header_unkown  (count_worker_instances,worker_countinfo)
    file_handling.write_csv_header_unkown(worker_input, worker_response_rows)
    file_handling.write_csv_header_unkown(output_csv,all_rows)
    file_handling.write_csv_header_unkown(yesCount_gt3,yesCount_gt3_allrows)

def readCongigFileGM(section):
    config = configparser.ConfigParser()
    config.read('split_mt1_argument_quality_Config.ini')
    input_csv = config.get(section, 'input_csv')
    mt_input_csv=config.get(section, 'mt_input_csv')
    output_csv= config.get(section, 'output_csv')
    noof_items_hit=config.get(section, 'noof_items_hit')
    yesCount_gt1=config.get(section,'yesCount_gt1')
    worker_input=config.get(section,'worker_input')
    arguments=(input_csv,mt_input_csv,output_csv,worker_input,yesCount_gt1,noof_items_hit)
    return  (arguments)

def runGM(section):
    args= readCongigFileGM(section)
    input_csv= args[0]
    mt_input_csv=args[1]
    output_csv=args[2]
    worker_input=args[3]
    yesCount_gt1=args[4]
    noof_items_hit= int(args[5])
    
    executeGM(input_csv,mt_input_csv,output_csv,worker_input,yesCount_gt1,noof_items_hit)

def executeGM(input_csv,mt_input_csv,output_csv,worker_input,yesCount_gt1,noof_items_hit):
    rowdicts= file_handling.readCsv(input_csv)
    yescount=1
    new_rows_tuple=splitRowPairwise(rowdicts,noof_items_hit,yescount)
    all_rows=new_rows_tuple[0]
    worker_response_rows=new_rows_tuple[1]
    yesCount_gt1_allrows= new_rows_tuple[2]
    file_handling.write_csv_header_unkown(worker_input, worker_response_rows)
    file_handling.write_csv_header_unkown(output_csv,all_rows)
    file_handling.write_csv_header_unkown(yesCount_gt1,yesCount_gt1_allrows)

def readconfigmergeMissingGM(section):
    config = configparser.ConfigParser()
    config.read('split_mt1_argument_quality_Config.ini')
    input_csv1 = config.get(section, 'input_csv1')
    input_csv2=config.get(section, 'input_csv2')
    output_csv= config.get(section, 'output_csv')
    mtinput=config.get(section, 'mtinput')
    yes_Count_File=config.get(section,"yes_Count_File")
    no_Count_File=config.get(section,"no_Count_File")
    output_csv_sorted=config.get(section,"output_csv_sorted")
    arguments=(input_csv1,input_csv2,output_csv,mtinput,yes_Count_File,no_Count_File,output_csv_sorted)
    return arguments
    

def mergemissingGM(section):
    arguments=readconfigmergeMissingGM(section)
    input_csv1=arguments[0]
    input_csv2=arguments[1]
    output_csv=arguments[2]
    mtinput=arguments[3]
    yes_Count_File=arguments[4]
    no_Count_File=arguments[5]
    output_csv_sorted=arguments[6]
    
    rowdicts1= file_handling.readCsv(input_csv1)
    rowdicts2= file_handling.readCsv(input_csv2)
    rowdicts3=file_handling.readCsv(mtinput)
    erroruniquerow=int(38733)
    errorsentencerow=[rowe for rowe in rowdicts3 if int(rowe["uniqueRowNo"])== erroruniquerow][0]
    sentence=errorsentencerow["sentence"]
    number=len(rowdicts1)
    newrows=[]
    yesCount_gt1=[]
    noCount_gt1=[]
    count_bin2=0
    removedrandomrowsbin2=[]
    for count in range(0,number):
        row1=rowdicts1[count]
        uniqueRowNo=int(row1["uniqueRowNo"])
        row2=[rowt for rowt in rowdicts2 if int(rowt["uniqueRowNo"])== uniqueRowNo][0]
        row1.update(row2)
        row1['No_Count']=0
        row1['Yes_Count']=0
        for key,value in row1.items():
            if str(key).startswith("Id"):
                if value.lower() == 'yes' :
                            row1['Yes_Count']=row1['Yes_Count']+1
                else:        
                    if value.lower() =='no':  
                            row1['No_Count']=row1['No_Count']+1 
                    else:        
                        print("error in merging rows in mergemissingGM ")
                        sys.exit(-1)    
        
        # row with unique no 38733 got corrupted so replace the text
        if int(row1["uniqueRowNo"])==erroruniquerow:
            row1["sentence"]=sentence
        if row1["bin"].startswith("binNo==2") and count_bin2 < 8  and int(row1['Yes_Count'])> 1:
            removedrandomrowsbin2.append(row1)
            count_bin2=count_bin2+1
            continue 
        if row1['Yes_Count']> 1:
                yesCount_gt1.append(row1)
        if  row1['No_Count']> 1:
            noCount_gt1.append(row1)          
            
            
        newrows.append(row1)
    #return newrows
    colnames=newrows[0].keys()
    nocolnames=[]
    for col in colnames:
        if str(col).startswith("Id") or str(col).startswith("HIT") or str(col).startswith("tmp") :
            continue
        else:
            nocolnames.append(col)
    
    removedrowsbin2=os.path.join(os.path.dirname(yes_Count_File), "removedrowsbin2.csv")
    file_handling.writeCsv(removedrowsbin2,removedrandomrowsbin2,nocolnames)
    file_handling.writeCsv(output_csv, newrows, colnames)
    listsorted = sorted(newrows, key=lambda x: int(operator.itemgetter("Yes_Count")(x)))
    file_handling.writeCsv(yes_Count_File,yesCount_gt1,nocolnames)
    file_handling.writeCsv(no_Count_File,noCount_gt1,nocolnames)
    file_handling.writeCsv(output_csv_sorted, listsorted, colnames)
    file_handling.writeCsv(output_csv, newrows, colnames)
    

#change a yes to 1 and a no to 0 for gun control arg quality
def executeregressionGC(input_csv,output_csv):
    inrows=file_handling.readCsv(input_csv)
    allrows=[]
    for row in inrows:
        newdict=dict()
        for key,value in row.items():
            if str(key).lower().startswith("id_"):
                if str(value).lower() ==str("yes"):
                    newdict[key]=1
                else:
                    if str(value).lower() ==str("no"):
                        newdict[key]=0
        allrows.append(newdict)
                        
    file_handling.write_csv_header_unkown(output_csv, allrows)                    

def runRegressionGC(section):
    config = configparser.ConfigParser()
    config.read('split_mt1_argument_quality_Config.ini')
    input_csv = config.get(section, 'input_csv')
    output_csv= config.get(section, 'output_csv')
    executeregressionGC(input_csv,output_csv)
                    
if __name__ == '__main__':
    #===========================================================================
    # section="Arg_Quality_MT1"   done for gun control
    # run(section)
    #===========================================================================
    section="GM"       #done for GM
    #runGM(section)
    section="mergemissingGM"
    #mergemissingGM(section)
    
    section="createregressioninputGC"
    runRegressionGC(section)
    