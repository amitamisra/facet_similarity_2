'''
Created on Oct 29, 2015
get MT annotations for argument quality pairs and split to get annotation for each row and calculate true score
@author: amita
'''

import operator
import itertools
from collections import defaultdict
from copy import deepcopy
import configparser
from utilities import file_handling
import sys
from mechanicalturk import mt1_argumentqualitypairsGC


def mergemissingrowsDP(inputrows,missinganscsv,missingworkerid):
    missingansrows=file_handling.readCsv(missinganscsv)
    for roworder in  inputrows:
        unique1=int(roworder["uniqueRowNo_1"])
        unique2=int(roworder["uniqueRowNo_2"])
        rowWithMissingData =[row_miss for row_miss in missingansrows if int(row_miss["uniqueRowNo_1"])== unique1 and int(row_miss["uniqueRowNo_2"]) == unique2]
        if rowWithMissingData:
            if len(rowWithMissingData ) >1:
                print(" error in merging missing data with results in function mergeMissingresults")
                sys.exit(-1)
            else:   
                
                        roworder[missingworkerid]=rowWithMissingData[0]["response"]
    
    return  inputrows       


def executemissing(section):
    config = configparser.ConfigParser()
    config.read('split_mt1_argumentqualitypairs_Config.ini')
    mtOriginal_csv=config.get(section,'input-file')
    missingrows_csv=config.get(section,"missingrows-file")
    num_pairs_hit=config.get(section, 'num-pairs-hit')
    missingFormatted=config.get(section,'missingFormatted')
    no_ofhits=int(config.get(section, 'no-ofhits'))
    rowdicts= file_handling.readCsv(mtOriginal_csv)
    missingrows=rowdicts[1420:1440]
    colnames=missingrows[0].keys()
    file_handling.writeCsv(missingrows_csv, missingrows, colnames)
    mt1_argumentqualitypairsGC.createHitInput(missingrows_csv,missingFormatted,num_pairs_hit,no_ofhits)
    

def orderRowsasperInput(mtoriginalsequence,mttruelabelrows):
    newrows=[]
    for roworiginal in mtoriginalsequence:
        unique1=roworiginal["uniqueRowNo_1"]
        unique2=roworiginal["uniqueRowNo_2"]
        rowtrueLabel =[row for row in mttruelabelrows if row["uniqueRowNo_1"]== unique1 and row["uniqueRowNo_2"] == unique2]
        if len(rowtrueLabel) != 1:
            print(" error in merging true score labels with original csv in function  orderRowsasperInput")
            sys.exit(-1)
        else:
            newrows.append(rowtrueLabel[0]) 
    return newrows           

def mergeMissingresults(all_rows_order,missingResults,noof_items_hit):
    misssingResultsrow=file_handling.readCsv(missingResults)
    allmissingRows=[]
    mergedRows=[]
    for key, items in itertools.groupby( misssingResultsrow, operator.itemgetter("HITId")):
        Hitids=list(items)
        for count in range(1,noof_items_hit+1):
            NewDict=defaultdict()
            for row in Hitids:
                    NewDict["Id_"+ row["WorkerId"]]=(row["Answer.response"+ str(count)])
            for fieldname in row.keys():
                if str(fieldname).startswith("Input.") and "A2LCFORIW0NF1S" not in str(fieldname):
                    indexOfscore=str(fieldname).rfind("_")
                    newkey=fieldname[6:indexOfscore]
                    NewDict[newkey]=row["Input."+newkey+"_"+ str(count)]  
            
            
            NewDict["HITId"]=row["HITId"]
            NewDict["HITTypeId"]=row["HITTypeId"] 
            allmissingRows.append(NewDict) 
    
    for roworder in all_rows_order:
        unique1=roworder["uniqueRowNo_1"]
        unique2=roworder["uniqueRowNo_2"]
        rowWithMissingData =[row for row in allmissingRows if row["uniqueRowNo_1"]== unique1 and row["uniqueRowNo_2"] == unique2]
        if rowWithMissingData:
            if len(rowWithMissingData ) !=1:
                print(" error in merging missind data with results in function mergeMissingresults")
                sys.exit(-1)
            roworder["Id_A2LCFORIW0NF1S"]=rowWithMissingData[0]["Id_A2LCFORIW0NF1S"]
        mergedRows.append(roworder)
        
    return mergedRows

def splitRowPairwise(rowdicts,noof_items_hit):
    allRows=list()
    Worker_rows=[]
    rowdicts.sort(key=operator.itemgetter("HITId"))
    for key, items in itertools.groupby(rowdicts, operator.itemgetter("HITId")):
        Hitids=list(items)
        for count in range(1,noof_items_hit+1):
            NewDict=defaultdict()
            response_dict={}
            for row in Hitids:
                    NewDict["Id_"+ row["WorkerId"]]=(row["Answer.response"+ str(count)])
            response_dict=deepcopy(NewDict)
            for fieldname in row.keys():
                if str(fieldname).startswith("Input.") :
                    indexOfscore=str(fieldname).rfind("_")
                    newkey=fieldname[6:indexOfscore]
                    NewDict[newkey]=row["Input."+newkey+"_"+ str(count)]  
            NewDict["HITId"]=row["HITId"]
            NewDict["HITTypeId"]=row["HITTypeId"] 
  
            Worker_rows.append(response_dict)
            allRows.append(NewDict) 
    
            
    return allRows   

def createCorrInoutRows(orderRows):
    newrows=[]
    for row in  orderRows:
        newdict={}
        for key,value in row.items():
            if str(key).lower().startswith("id_"):
                newdict[key]=value
        newrows.append(newdict)
    return newrows


def calculatetruescores(worker_scores_csv):
    newrows=[]
    rowdicts= file_handling.readCsv(worker_scores_csv)
    for row in rowdicts:
        count=0
        label=0
        for key,value in row.items():
            if str(key).lower().startswith("id_"):
                if value:
                    label=label+int(value)
                    count=count+1
        if count == 0:
            continue            
        row["regression_label"]=float(label)/count
        row["count_annotation"]=count        
        newrows.append(row)
    return newrows                    

def checksamerows(uniquedataset1,uniquedataset2, uniqueall1,  uniqueall2,sentdataset1,sentdataset2,sentallrows1,sentallrows2):
    if uniquedataset1==uniqueall1 and uniquedataset2==uniqueall2 and sentdataset1==sentallrows1 and sentdataset2==sentallrows2:
        return 1
    else:
        return -1
    

# add this function as gay marriage pairs file was missing dataset id
def add_datasetId(datasetidrows,allrows):
    datasetidrows_sorted = sorted(datasetidrows, key=operator.itemgetter('uniqueRowNo_1','uniqueRowNo_2'))
    allrows_sorted = sorted(allrows, key=operator.itemgetter('uniqueRowNo_1', 'uniqueRowNo_2'))
    
    count=0
    for count in range(0,len( allrows_sorted)):
        uniquerowdataset1= datasetidrows_sorted[count]['uniqueRowNo_1']
        uniquerowdataset2= datasetidrows_sorted[count]['uniqueRowNo_2']
        uniquerowall1=allrows_sorted[count]['uniqueRowNo_1']
        uniquerowall2=allrows_sorted[count]['uniqueRowNo_2']
        sentdataset1=datasetidrows_sorted[count]["sentence_1"]
        sentdataset2=datasetidrows_sorted[count]["sentence_2"]
        sentallrows1=allrows_sorted[count]["sentence_1"]
        sentallrows2=allrows_sorted[count]["sentence_2"]
        res=checksamerows(uniquerowdataset1,uniquerowdataset2, uniquerowall1,  uniquerowall2,sentdataset1,sentdataset2,sentallrows1,sentallrows2)
        if res ==1:
            allrows_sorted[count]["datasetId_1"]=datasetidrows_sorted[count]["datasetId_1"]
            allrows_sorted[count]["datasetId_2"]=datasetidrows_sorted[count]["datasetId_2"]
        else:
            print( "error in merging rows in function  adddatasetId unique rows dataset"  + uniquerowdataset1, uniquerowdataset1 )
            sys.exit(-1) 
    return allrows_sorted

    


def executeGM(input_csv,output_csv,correl_input,truescores_csv,mtOriginal_csv,noof_items_hit,splitrowsreduced_csv):
    rowdicts= file_handling.readCsv(input_csv)
    all_rows=splitRowPairwise(rowdicts,noof_items_hit)
    mtOriginal_rows=file_handling.readCsv(mtOriginal_csv)
    all_rows=add_datasetId( mtOriginal_rows,all_rows)
    file_handling.write_csv_header_unkown(output_csv,all_rows)
    row_truescores=calculatetruescores(output_csv)
    Worker_response_rows=createCorrInoutRows(all_rows)
    file_handling.write_csv_header_unkown(correl_input, Worker_response_rows)
    file_handling.write_csv_header_unkown(truescores_csv, row_truescores,get_keys_from_first_row=True)

    
                    
def execute(input_csv,output_csv,correl_input,truescores_csv,mtOriginal_csv,missingResults,noof_items_hit,splitrowsreduced_csv): 
    rowdicts= file_handling.readCsv(input_csv)
    all_rows=splitRowPairwise(rowdicts,noof_items_hit)
    mtOriginal_rows=file_handling.readCsv(mtOriginal_csv)
    all_rows_order=orderRowsasperInput(mtOriginal_rows,all_rows)
    all_rows_order=mergeMissingresults(all_rows_order,missingResults,noof_items_hit)
    file_handling.write_csv_header_unkown(output_csv,all_rows_order)
    row_truescores=calculatetruescores(output_csv)
    Worker_response_rows=createCorrInoutRows(all_rows_order)
    file_handling.write_csv_header_unkown(correl_input, Worker_response_rows)
    file_handling.write_csv_header_unkown(truescores_csv, row_truescores,get_keys_from_first_row=True)



def readCongigFile(section):
    config = configparser.ConfigParser()
    config.read('split_mt1_argumentqualitypairs_Config.ini')
    input_csv = config.get(section, 'input_csv')
    output_csv= config.get(section, 'output_csv')
    noof_items_hit=config.get(section, 'noof_items_hit')
    correl_input=config.get(section, 'correl_input')
    splitrowsreduced_csv=config.get(section, 'splitrowsreduced_csv')
    mtOriginal_csv=config.get(section,'mtOriginal_csv')
    truescores_csv=config.get(section, 'truescores_csv')
    missingFormatted=config.get(section, 'missingResults')
    arguments=(input_csv,output_csv,correl_input,truescores_csv,mtOriginal_csv,noof_items_hit,splitrowsreduced_csv,missingFormatted)
    return  (arguments)    


def readCongigFileGM(section):
    config = configparser.ConfigParser()
    config.read('split_mt1_argumentqualitypairs_Config.ini')
    input_csv = config.get(section, 'input_csv')
    output_csv= config.get(section, 'output_csv')
    noof_items_hit=config.get(section, 'noof_items_hit')
    correl_input=config.get(section, 'correl_input')
    splitrowsreduced_csv=config.get(section, 'splitrowsreduced_csv')
    mtOriginal_csv=config.get(section,'mtOriginal_csv')
    truescores_csv=config.get(section, 'truescores_csv')
    arguments=(input_csv,output_csv,correl_input,truescores_csv,mtOriginal_csv,noof_items_hit,splitrowsreduced_csv)
    return  (arguments)
    

def runGM(section):
    args= readCongigFileGM(section)
    input_csv= args[0]
    output_csv=args[1]
    correl_input=args[2]
    truescores_csv=args[3]
    mtOriginal_csv=args[4]
    noof_items_hit= int(args[5])
    splitrowsreduced_csv=args[6]
    executeGM(input_csv,output_csv,correl_input,truescores_csv,mtOriginal_csv,noof_items_hit,splitrowsreduced_csv)
          

    

def runGC(section):
    args= readCongigFile(section)
    input_csv= args[0]
    output_csv=args[1]
    correl_input=args[2]
    truescores_csv=args[3]
    mtOriginal_csv=args[4]
    noof_items_hit= int(args[5])
    splitrowsreduced_csv=args[6]
    missingResults=args[7]
    execute(input_csv,output_csv,correl_input,truescores_csv,mtOriginal_csv,missingResults,noof_items_hit,splitrowsreduced_csv)


def executeDP(input_csv,output_csv,correl_input,truescores_csv,mtOriginal_csv,missingResults,noof_items_hit,splitrowsreduced_csv):
    rowdicts= file_handling.readCsv(input_csv)
    all_rows=splitRowPairwise(rowdicts,noof_items_hit)
    #mtOriginal_rows=file_handling.readCsv(mtOriginal_csv)
    #all_rows_order=orderRowsasperInput(mtOriginal_rows,all_rows)
    missingworkerid="Id_A1FBBY2JJYRMRI"
    all_rows_merged=mergemissingrowsDP(all_rows,missingResults,missingworkerid)
    file_handling.write_csv_header_unkown(output_csv,all_rows)
    row_truescores=calculatetruescores(output_csv)
    Worker_response_rows=createCorrInoutRows(all_rows_merged)
    file_handling.write_csv_header_unkown(correl_input, Worker_response_rows)
    file_handling.write_csv_header_unkown(truescores_csv, row_truescores,get_keys_from_first_row=True)


def readCongigFileDP(section):
    config = configparser.ConfigParser()
    config.read('split_mt1_argumentqualitypairs_Config.ini')
    input_csv = config.get(section, 'input_csv')
    output_csv= config.get(section, 'output_csv')
    noof_items_hit=config.get(section, 'noof_items_hit')
    correl_input=config.get(section, 'correl_input')
    splitrowsreduced_csv=config.get(section, 'splitrowsreduced_csv')
    mtOriginal_csv=config.get(section,'mtOriginal_csv')
    truescores_csv=config.get(section, 'truescores_csv')
    missingResults=config.get(section, 'missingResults')
    arguments=(input_csv,output_csv,correl_input,truescores_csv,mtOriginal_csv,noof_items_hit,splitrowsreduced_csv,missingResults)
    return  (arguments)
    

def runDP(section):
    args= readCongigFileDP(section)
    input_csv= args[0]
    output_csv=args[1]
    correl_input=args[2]
    truescores_csv=args[3]
    mtOriginal_csv=args[4]
    noof_items_hit= int(args[5])
    splitrowsreduced_csv=args[6]
    missingResults=args[7]
    executeDP(input_csv,output_csv,correl_input,truescores_csv,mtOriginal_csv,missingResults,noof_items_hit,splitrowsreduced_csv)



def readCongigFileHasanNgLyn(section):
    config = configparser.ConfigParser()
    config.read('split_mt1_argumentqualitypairs_Config.ini')
    input_csv = config.get(section, 'input_csv')
    output_csv= config.get(section, 'output_csv')
    noof_items_hit=config.get(section, 'noof_items_hit')
    correl_input=config.get(section, 'correl_input')
    truescores_csv=config.get(section, 'truescores_csv')
    arguments=(input_csv,output_csv,correl_input,truescores_csv,noof_items_hit)
    return  (arguments)
    

def executeHasanNg_Lyn(input_csv,output_csv,correl_input,truescores_csv,noof_items_hit):
    rowdicts= file_handling.readCsv(input_csv)
    all_rows=splitRowPairwise(rowdicts,noof_items_hit)
    file_handling.write_csv_header_unkown(output_csv,all_rows)
    worker_rows=createCorrInoutRows(all_rows)
    truescores=calculatetruescores(output_csv)
    file_handling.write_csv_header_unkown(correl_input,worker_rows)
    file_handling.write_csv_header_unkown( truescores_csv,truescores)
    
    
    

    


def runHasanNG_Lun(section):
    args= readCongigFileHasanNgLyn(section)
    input_csv= args[0]
    output_csv=args[1]
    correl_input=args[2]
    truescores_csv=args[3]
    noof_items_hit= int(args[4])
    executeHasanNg_Lyn(input_csv,output_csv,correl_input,truescores_csv,noof_items_hit)


def exceutereviewerGC(input_csv,output_csv,correl_input,truescores_csv,noof_items_hit):
    executeHasanNg_Lyn(input_csv,output_csv,correl_input,truescores_csv,noof_items_hit)
    
    
    
def runreviewerGC(section):
    args= readCongigFileHasanNgLyn(section)
    input_csv= args[0]
    output_csv=args[1]
    correl_input=args[2]
    truescores_csv=args[3]
    noof_items_hit= int(args[4])
    exceutereviewerGC(input_csv,output_csv,correl_input,truescores_csv,noof_items_hit)

    
        
          
if __name__ == '__main__':
    #section="missing"      # done only once to create  missing results file
    #executemissing(section)#done only once to create  missing results file
    
    section="MT1ArgumentQualityPairs"
    #runGC(section)  #done for gc 
    
    section="MT1ArgumentQualityPairsGM"
    runGM(section) # done for gm


    section="MT1ArgumentQualityPairsDP"
    #runDP(section)
    
    section ="HasanNG_Lyn"
    #runHasanNG_Lun(section)
    
    section="reviewer_bottomumbcGC"
    #runreviewerGC(section)
    section="reviewer_midumbcGC"
    #runreviewerGC(section)
    