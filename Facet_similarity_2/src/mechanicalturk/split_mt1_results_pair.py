'''
Created on Jul 6, 2015
Take the mechanical turk results file. combine  results for a  HIT and  get values for each pair.
create a record for each individual pair containing  the responses from all the workers.
create an input file for calculating correlation among workers.
@author: amita
'''
import operator
import itertools
from collections import defaultdict
import  configparser
from utilities import file_handling
from copy import deepcopy

def runGC():
    args= readCongigFile()
    input_csv= args[0]
    mt_input_csv=args[1]
    output_csv=args[2]
    correl_input=args[3]
    noof_items_hit= int(args[4])
    unique_list_cols=args[5]
    execute(input_csv,mt_input_csv,output_csv,correl_input,noof_items_hit,unique_list_cols)


def readCongigFile():
    config = configparser.ConfigParser()
    config.read('split_mt1_results_pair_Config.ini')
    input_csv = config.get('GC', 'input_csv')
    mt_input_csv=config.get('GC', 'mt_input_csv')
    output_csv= config.get('GC', 'output_csv')
    noof_items_hit=config.get('GC', 'noof_items_hit')
    unique_list_cols=config.get('GC', 'unique_list_cols').split(",")
    correl_input=config.get('GC', 'correl_input')
    arguments=(input_csv,mt_input_csv,output_csv,correl_input,noof_items_hit,unique_list_cols)
    return  (arguments)


def addPMI(all_rows,mt_input_csv,uniquefield):
    rows_withpmi=file_handling.readCsv(mt_input_csv)
    newrows=[]
    for row in all_rows:
        newrow={}
        uniquerec=int(row[uniquefield])
        pmirow=[pmrow for pmrow in rows_withpmi if int(pmrow[uniquefield])==uniquerec][0]
        pmikeys=pmirow.keys()
        pmikeynames=[keyname for keyname in pmikeys if keyname.startswith("PMI")]
        for key, value in row.items():
            if key.startswith("PMI"):
                for keynm in pmikeynames:
                    if "_1" in keynm:
                        newk=keynm.replace("_1","_a")
                        newk=newk.replace("PMI_","PMI.")
                    if "_2" in keynm:
                        newk=keynm.replace("_2","_b")
                        newk=newk.replace("PMI_","PMI.")    
                    newrow[newk]=pmirow[keynm]
            else:
                newrow[key]=row[key]    
        newrows.append(newrow)    
    return newrows 
   
def splitRowPairwise(rowdicts,noof_items_hit,colnames_list,unique_list_cols):
    AllRows=list()
    Worker_rows=[]
    rowdicts.sort(key=operator.itemgetter("HITId"))
    for key, items in itertools.groupby(rowdicts, operator.itemgetter("HITId")):
        Hitids=list(items)
        for count in range(1,noof_items_hit+1):
            NewDict=defaultdict()
            response_dict={}
            for row in Hitids:
                    NewDict["Id_"+ row["WorkerId"]]=(row["Answer.response"+ str(count)])
            response_dict=deepcopy(NewDict)
            for field in colnames_list:
                if field in unique_list_cols:
                    NewDict[field]=row["Input."+field+ str(count)]  
                    response_dict[field]= row["Input."+field+ str(count)]  
                else:
                    NewDict[field ]=row["Input."+field+ "_"+ str(count)]          
            NewDict["HITId"]=row["HITId"]
            NewDict["HITTypeId"]=row["HITTypeId"] 
            Worker_rows.append(response_dict)
            AllRows.append(NewDict) 
    
            
    return (AllRows, Worker_rows)      
 
                
def getcolnames(mt_input_csv):
    rows=  file_handling.readCsv(mt_input_csv) 
    colnames_list=list(rows[0].keys() )
    return colnames_list   


def changecolnmaes(colnames_list):
    newcollist=[]
    for colname in  colnames_list:
        if "_1" in colname:
            newcolname=colname.replace("_1","_a")
        else:    
            if "_2" in colname:
                newcolname=colname.replace("_2","_b") 
            else:
                newcolname=colname
        newcollist.append(newcolname)    
    return   newcollist  
               
        
             
                    
def execute(input_csv,mt_input_csv,output_csv,correl_input,noof_items_hit,unique_list_cols): 
    rowdicts= file_handling.readCsv(input_csv)
    colnames_list=getcolnames(mt_input_csv)
    new_col_list=changecolnmaes(colnames_list)
    new_rows_tuple=splitRowPairwise(rowdicts,noof_items_hit,new_col_list,unique_list_cols)
    all_rows=new_rows_tuple[0]
    new_rows_pmi=addPMI(all_rows,mt_input_csv,unique_list_cols[1])
    Worker_response_rows=new_rows_tuple[1]
    file_handling.write_csv_header_unkown(output_csv, new_rows_pmi)
    file_handling.write_csv_header_unkown(correl_input, Worker_response_rows)
    
          
if __name__ == '__main__':
    runGC()