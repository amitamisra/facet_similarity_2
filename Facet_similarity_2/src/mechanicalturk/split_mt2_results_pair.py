'''
Created on Sep 26, 2015

@author: amita

Take the mechanical turk results file. combine  results for a  HIT and  get values for each pair.
create a record for each individual pair containing  the responses from all the workers.
create an input file for calculating correlation among workers.
@author: amita
'''
from collections import defaultdict
import configparser
from utilities.file_handling import writeCsv
from utilities.file_handling import readCsv
from utilities.file_handling import write_csv_header_unkown
import itertools,operator
from random import shuffle
import operator
import itertools
from collections import defaultdict
import  configparser
from copy import deepcopy

def revertfields(merged_rows):
    revertedRows=[]
    for row in  merged_rows:
            row["rouge_w_1.2_f_score"]=row.pop("rouge_w_1_2_f_score")
            row["rouge_s*_f_score"]=row.pop("rouge_s__f_score")
            row["rouge_su*_f_score"]=row.pop("rouge_su__f_score")
            row["PMI.Max_b"]=row.pop("PMI_Max_b")
            row["PMI.Max_a"]=row.pop("PMI_Max_a")
            row["PMI.Harmonic_b"]=row.pop("PMI_Harmonic_b")
            row["PMI.Harmonic_a"]= row.pop("PMI_Harmonic_a") 
            revertedRows.append(row) 
    return revertedRows         


                
def getcolnames(mt_input_csv):
    rows=  file_handling.readCsv(mt_input_csv) 
    colnames_list=list(rows[0].keys() )
    return colnames_list   


def changecolnmaes(colnames_list):
    newcollist=[]
    for colname in  colnames_list:
        if "_1" in colname:
            newcolname=colname.replace("_1","_a")
        else:    
            if "_2" in colname:
                newcolname=colname.replace("_2","_b") 
            else:
                newcolname=colname
        newcollist.append(newcolname)    
    return   newcollist  
 

 
def checkFields(newdict,mtdict,list_cols):
    
    for field in list_cols:
        if newdict[field] != mtdict[field]:
            return False
            
    return True   
                 
def mergeRows(all_rows,row_dicts_mtinput,list_cols,unique_field): 
    
    sortedallrows= sorted(all_rows, key=lambda x: int(operator.itemgetter(unique_field)(x)))
    sorted_mtrows=sorted(row_dicts_mtinput, key=lambda x: int(operator.itemgetter(unique_field)(x)))
    norec=len(sortedallrows)
    allMergedRecords=[]
    for count in range(0, norec):
        newdict=deepcopy(sortedallrows[count])
        mtdict=deepcopy(sorted_mtrows[count])
        result=checkFields(newdict,mtdict,list_cols)
        if result == False :
            print("Merging of MT results and MT input failed, aborting")
            exit(-1)
            
        else:
            newdict.update(mtdict)
            
        allMergedRecords.append(newdict)        
    return  allMergedRecords          
    

def splitRowPairwise(rowdicts,noof_items_hit,colnames_list,unique_field):
    AllRows=list()
    Worker_rows=[]
    rowdicts.sort(key=operator.itemgetter("HITId"))
    for key, items in itertools.groupby(rowdicts, operator.itemgetter("HITId")):
        Hitids=list(items)
        for count in range(1,noof_items_hit+1):
            NewDict=defaultdict()
            response_dict={}
            for row in Hitids:
                    NewDict["Id_"+ row["WorkerId"]]=(row["Answer.response"+ str(count)])
            response_dict=deepcopy(NewDict)
            for field in colnames_list:
                NewDict[field]=row["Input."+field+ str(count)]  
            response_dict["UmbcSimilarity"]= row["Input."+"UmbcSimilarity"+ str(count)]
            response_dict[unique_field]= row["Input."+unique_field+ str(count)]
                                                  
            NewDict["HITId"]=row["HITId"]
            NewDict["HITTypeId"]=row["HITTypeId"] 
            Worker_rows.append(response_dict)
            AllRows.append(NewDict) 
    
            
    return (AllRows, Worker_rows)      
 
                    
                    
def execute(input_csv,mt_input_csv,splitrowsreduced_csv,output_csv,correl_input,noof_items_hit,unique_field,list_cols): 
    rowdicts= file_handling.readCsv(input_csv)
    row_dicts_mtinput=file_handling.readCsv(mt_input_csv)
    new_rows_tuple=splitRowPairwise(rowdicts,noof_items_hit,list_cols,unique_field)
    all_rows_reduced=new_rows_tuple[0]
    file_handling.write_csv_header_unkown(splitrowsreduced_csv,all_rows_reduced)
    merged_rows=mergeRows(all_rows_reduced,row_dicts_mtinput,list_cols,unique_field)
    revertedrows=revertfields(merged_rows)
    Worker_response_rows=new_rows_tuple[1]
    file_handling.write_csv_header_unkown(output_csv,revertedrows)
    file_handling.write_csv_header_unkown(correl_input, Worker_response_rows)



def readCongigFile():
    config = configparser.ConfigParser()
    config.read('split_mt2_pair_Config.ini')
    input_csv = config.get('GC', 'input_csv')
    mt_input_csv=config.get('GC', 'mt_input_csv')
    output_csv= config.get('GC', 'output_csv')
    noof_items_hit=config.get('GC', 'noof_items_hit')
    correl_input=config.get('GC', 'correl_input')
    unique_field=config.get('GC', 'unique_row')
    list_cols=config.get('GC', 'list_cols').split(",")
    splitrowsreduced_csv=config.get('GC', 'splitrowsreduced_csv')
    arguments=(input_csv,mt_input_csv,output_csv,correl_input,noof_items_hit,unique_field,list_cols,splitrowsreduced_csv)
    return  (arguments)    

def runGC():
    args= readCongigFile()
    input_csv= args[0]
    mt_input_csv=args[1]
    output_csv=args[2]
    correl_input=args[3]
    noof_items_hit= int(args[4])
    unique_field=args[5]
    list_cols=args[6]
    splitrowsreduced_csv=args[7]
    execute(input_csv,mt_input_csv,splitrowsreduced_csv,output_csv,correl_input,noof_items_hit,unique_field,list_cols)
          
if __name__ == '__main__':
    runGC()