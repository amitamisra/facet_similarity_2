'''
Created on Aug 3, 2015
Change MT labels to 0,1,2 for bootstrapping
@author: amita
'''
from random import shuffle
import  configparser
from utilities import file_handling

def runGC():
    args= readCongigFile()
    input_csv= args[0]
    output_csv=args[1]
    regression_label= args[2]
    execute(input_csv, output_csv, regression_label)

def execute(input_csv,output_csv,regression_label):
    rows=file_handling.readCsv(input_csv)
    newrows=[]
    for row in rows:
        label=float(row[regression_label])
        
        if label < 2.6:
            row[regression_label]=0
        else:
            if label >= 2.6 and label < 4.0:
                row[regression_label]=1.0
            else:
                row[regression_label]=2.0                   
        newrows.append(row)   
    colnames=newrows[0].keys()    
    file_handling.writeCsv(output_csv, newrows, colnames)    
        
    
def readCongigFile():
    config = configparser.ConfigParser()
    config.read('updateMTlabel_Config.ini')
    input_csv = config.get('GC', 'input_csv')
    output_csv=config.get('GC', 'output_csv')
    regression_label= config.get('GC', 'regression_label')
    arguments=(input_csv,output_csv,regression_label)
    return  (arguments)

if __name__ == '__main__':
    runGC()