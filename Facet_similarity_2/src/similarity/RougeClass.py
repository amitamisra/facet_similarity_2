#!/usr/bin/env python3
#coding: utf8 

'''
rouge similarity
@author: amita
'''


import os
from pyrouge import Rouge155
from utilities import file_handling

class rouge:
    def __init__(self):
        self.rouge_1_f_score=0.0
        self.rouge_2_f_score=0.0
        self.rouge_3_f_score=0.0
        self.rouge_4_f_score=0.0
        self.rouge_l_f_score=0.0
        self.rouge_s_f_score=0.0
        self.rouge_su_f_score=0.0
        self.rouge_w_1_2_f_score=0.0

    def get_rouge_1_f_score(self):
        return self.__rouge_1_f_score


    def get_rouge_2_f_score(self):
        return self.__rouge_2_f_score


    def get_rouge_3_f_score(self):
        return self.__rouge_3_f_score


    def get_rouge_4_f_score(self):
        return self.__rouge_4_f_score


    def get_rouge_l_f_score(self):
        return self.__rouge_l_f_score


    def get_rouge_s_f_score(self):
        return self.__rouge_s_f_score


    def get_rouge_su_f_score(self):
        return self.__rouge_su_f_score


    def get_rouge_w_1_2_f_score(self):
        return self.__rouge_w_1_2_f_score


    def set_rouge_1_f_score(self, value):
        self.__rouge_1_f_score = value


    def set_rouge_2_f_score(self, value):
        self.__rouge_2_f_score = value


    def set_rouge_3_f_score(self, value):
        self.__rouge_3_f_score = value


    def set_rouge_4_f_score(self, value):
        self.__rouge_4_f_score = value


    def set_rouge_l_f_score(self, value):
        self.__rouge_l_f_score = value


    def set_rouge_s_f_score(self, value):
        self.__rouge_s_f_score = value


    def set_rouge_su_f_score(self, value):
        self.__rouge_su_f_score = value


    def set_rouge_w_1_2_f_score(self, value):
        self.__rouge_w_1_2_f_score = value

    


    def Setrougue(self,stringa, stringb, writedirRouge):
        r = Rouge155()
        count=0
        dirname_sys= writedirRouge +"rougue/System/"
        dirname_mod=writedirRouge +"rougue/Model/"
        if not os.path.exists(dirname_sys):
            os.makedirs(dirname_sys)
        if not os.path.exists(dirname_mod):  
            os.makedirs(dirname_mod)
        Filename=dirname_sys +"string_."+str(count)+".txt"
        LinesA=list()
        LinesA.append(stringa)
        file_handling.writeTextFile(Filename, LinesA)
        LinesB=list()
        LinesB.append(stringb)
        Filename=dirname_mod+"string_.A."+str(count)+ ".txt"
        file_handling.writeTextFile(Filename, LinesB)
        r.system_dir = dirname_sys
        r.model_dir = dirname_mod
        r.system_filename_pattern = 'string_.(\d+).txt'
        r.model_filename_pattern = 'string_.[A-Z].#ID#.txt'
        output = r.convert_and_evaluate()
        output_dict = r.output_to_dict(output)
        self.set_rouge_1_f_score=output_dict["rouge_1_f_score"]
        self.set_rouge_2_f_score=output_dict["rouge_2_f_score"]
        self.set_rouge_3_f_score=output_dict["rouge_3_f_score"]
        self.set_rouge_4_f_score=output_dict["rouge_4_f_score"]
        self.set_rouge_l_f_score=output_dict["rouge_l_f_score"]
        self.set_rouge_s_f_score=output_dict["rouge_s*_f_score"]
        self.set_rouge_su_f_score=output_dict["rouge_su*_f_score"]
        self.set_rouge_w_1_2_f_score=output_dict["rouge_w_1.2_f_score"]
    
    
        
        
if __name__ == '__main__':
    
    stringa="Anybody with a concealed carry permit will tell you that trying to hide a gun of average size like the 1911."
    stringb="In contrast to near-complete bans in Australia and Great Britain,  most public places."
    writedirRouge="/Users/amita/Facet_similarity_part2/Data/"
    