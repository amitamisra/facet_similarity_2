'''
Created on Dec 30, 2015
merge all topics death penalty, gay marriage, gun control 
@author: amita
'''

import configparser,sys,os
from utilities import file_handling
from collections import OrderedDict

FeatureList=["liwc","rouge","umbcsimilarity","ngram","umbc_combined", "regression_label"]
ignorekeys=["liwc_dep_overlap_norm","NGramIntersectionSizeNormalized"]
replacetuple=[("LIWC_affective processes_OVERLAP_NORMALIZED","LIWC_affective_processes_OVERLAP_NORMALIZED"),
            ("LIWC_biological processes_OVERLAP_NORMALIZED","LIWC_biological_processes_OVERLAP_NORMALIZED"),
            ("LIWC_cognitive processes_OVERLAP_NORMALIZED","LIWC_cognitive_processes_OVERLAP_NORMALIZED"),
            ("LIWC_negative emotion_OVERLAP_NORMALIZED","LIWC_negative_emotion_OVERLAP_NORMALIZED"),
            ("LIWC_perceptual processes_OVERLAP_NORMALIZED","LIWC_perceptual_processes_OVERLAP_NORMALIZED"),
           ("LIWC_positive emotion_OVERLAP_NORMALIZED","LIWC_positive_emotion_OVERLAP_NORMALIZED"),
           ("LIWC_social processes_OVERLAP_NORMALIZED","LIWC_social_processes_OVERLAP_NORMALIZED")]

def run(section):
    args= readCongigFile(section)
    input_gc= args[0]
    input_dp= args[1]
    input_gm= args[2]
    output_all=args[3]
    output_features=args[4]
    datafields=list(args[5].split(","))
    execute(input_gc,input_dp,input_gm,output_all,output_features,datafields)

def createFeaturesdict(row,featureRow,FeatureList):
    for key,value  in row.items():
        if key  in ignorekeys:
            continue
        else:
            if key.strip().lower().startswith(tuple(FeatureList)):
                featureRow[key.strip()]= value 
 
# had to be done for merging the three topics since gun control was having features through arff and other topics through csv            
def replace_byspace(row):
    for key,value in row.items():
        for item in replacetuple:
            if key == item[1]:
                del row[key]
                row[item[0]]=value
        
    
def addtopic(rowstopic,topic,datafields):
    featurerows=[]
    allrows=[]
    for row in rowstopic:
        featureRow=OrderedDict()
        newrow=OrderedDict()
        if topic=="guncontrol":
            replace_byspace(row)
        createFeaturesdict(row,featureRow,FeatureList) 
        for item in datafields:
            newrow[item+ "_1"]=row[item +"_1"]
            newrow[item+ "_2"]=row[item +"_2"]
        newrow["topic"]=topic
        newrow.update(featureRow)
        
        allrows.append(newrow)
        featurerows.append(featureRow) 
    return featurerows,allrows



def mergerows(input_gc,input_dp,input_gm,datafields ):
   
    gcrows=file_handling.readCsv(input_gc)
    dprows=file_handling.readCsv(input_dp)
    gmrows=file_handling.readCsv(input_gm)
    
    gcfeatures,gcallrows=addtopic(gcrows,"guncontrol",datafields)  
    dpfeatures,dpallrows=addtopic(dprows,"deathpenalty",datafields)  
    gmfeatures,gmallrows=addtopic(gmrows,"gaymarriage",datafields)  
    
    gmfeaturekeys=sorted(list(gmfeatures[0].keys()))
    dpfeaturekeys=sorted(list(dpfeatures[0].keys()))
    gcfeaturekeys=sorted(list(gcfeatures[0].keys()))
    
    gmallkeys=sorted(list(gmallrows[0].keys()))
    dpallkeys=sorted(list(dpallrows[0].keys()))
    gcallkeys=sorted(list(gcallrows[0].keys()))
    
   
     
        
    if  gmallkeys==dpallkeys==gcallkeys:   
                    
        if gmfeaturekeys == dpfeaturekeys and gmfeaturekeys==gcfeaturekeys:
            return(gcallrows, dpallrows,  gmallrows, gcfeatures, dpfeatures, gmfeatures)
            
        else:
            print(gmfeaturekeys)
            print(dpfeaturekeys) 
            print(gcfeaturekeys)
            print("error in merging features, feature names different across topics") 
            sys.exit(-1)  
    else:
            print(gmallkeys)
            print(dpallkeys) 
            print(gcallkeys)
            print("error in merging all keys, column  names different across topics") 
            sys.exit(-1)  
        
    
def readCongigFile(section):
    config = configparser.ConfigParser()
    config.read('mergealltopicpairs_Config.ini')
    input_gc = config.get(section, 'inputgc')
    input_dp = config.get(section, 'inputdp')
    input_gm = config.get(section, 'inputgm')
    output_all= config.get(section, 'output_all')
    output_features= config.get(section, 'output_features')
    datafields=config.get(section, 'datafields')
    arguments=(input_gc,input_dp,input_gm,output_all,output_features,datafields)
    return  (arguments)
    


def movelabeltoLast(colnames):
    colnames.remove("regression_label")
    colnames.append("regression_label")
    
def execute(input_gc,input_dp,input_gm,output_all,output_features,datafields):
    gcallrows, dpallrows,  gmallrows,gcfeatures, dpfeatures, gmfeatures =mergerows(input_gc,input_dp,input_gm,datafields)
    merged_gc_gm_all= gcallrows + gmallrows
    merged_gc_dp_all= gcallrows + dpallrows
    merged_gm_dp_all= gmallrows + dpallrows
    
    merged_dp_gm_gc_all= gcallrows + gmallrows+dpallrows
    
    merged_features_dp_gc_gm=gcfeatures + gmfeatures+dpfeatures
    merged_features_gc_gm= gcfeatures + gmfeatures
    merged_features_gc_dp=gcfeatures + dpfeatures
    merged_features_gm_dp=dpfeatures + gmfeatures
    
    colnames_gc_gm_all=sorted(list(merged_gc_gm_all[0].keys()))
    colnamesfeatures_gc_gm_all=sorted(list(merged_features_gc_gm[0].keys()))
    
    colnames_gc_dp_all=sorted(list(merged_gc_dp_all[0].keys()))
    colnamesfeatures_gc_dp_all=sorted(list(merged_features_gc_dp[0].keys()))
    
    colnames_gm_dp_all=sorted(list(merged_gm_dp_all[0].keys()))
    colnamesfeatures_gm_dp_all=sorted(list(merged_features_gm_dp[0].keys()))
    
    colnames_dp_gm_gc_all=sorted(list( merged_dp_gm_gc_all[0].keys()))
    colnamesfeatures_dp_gm_gc_all=sorted(list(merged_features_dp_gc_gm[0].keys()))
    
    
    
    
    movelabeltoLast(colnames_gc_gm_all)
    movelabeltoLast(colnames_gc_dp_all)
    movelabeltoLast(colnames_gm_dp_all)
    movelabeltoLast(colnamesfeatures_gc_gm_all)
    movelabeltoLast(colnamesfeatures_gc_dp_all)
    movelabeltoLast(colnamesfeatures_gm_dp_all)
    
    movelabeltoLast(colnames_dp_gm_gc_all)
    movelabeltoLast(colnamesfeatures_dp_gm_gc_all)
    
    outputall_gc_gm=os.path.join(os.path.dirname(output_all),"mt1_truelabel_GC_GM_gt55_argQualPairsTrain_with_all47_features_norm.csv")
    outputall_gc_dp=os.path.join(os.path.dirname(output_all),"mt1_truelabel_GC_DP_gt55_argQualPairsTrain_with_all47_features_norm.csv")
    outputall_gm_dp=os.path.join(os.path.dirname(output_all),"mt1_truelabel_GM_DP_gt55_argQualPairsTrain_with_all47_features_norm.csv")
    outputfeatures_gc_gm=os.path.join(os.path.dirname(output_all),"mt1_truelabel_GC_GM_gt55_argQualPairsTrain_only47_features_norm.csv")
    outputfeatures_gc_dp=os.path.join(os.path.dirname(output_all),"mt1_truelabel_GC_DP_gt55_argQualPairsTrain_only47_features_norm.csv")
    outputfeatures_gm_dp=os.path.join(os.path.dirname(output_all),"mt1_truelabel_GM_DP_gt55_argQualPairsTrain_only47_features_norm.csv")
    outputfeatures_dp_gm_gc_all=os.path.join(os.path.dirname(output_all),"mt1_truelabel_GC_GM_DP_gt55_argQualPairsTrain_with_all47_features_norm.csv")
    outputfeaturesdp_gm_gc_features=os.path.join(os.path.dirname(output_all),"mt1_truelabel_GC_GM_DP_gt55_argQualPairsTrain_only47_features_norm.csv")
    
    
    file_handling.writeCsv(outputall_gc_gm, merged_gc_gm_all, colnames_gc_gm_all)  
    file_handling.writeCsv(outputall_gc_dp, merged_gc_dp_all,  colnames_gc_dp_all)  
    file_handling.writeCsv(outputall_gm_dp, merged_gm_dp_all, colnames_gm_dp_all)  
    file_handling.writeCsv(outputfeatures_gc_gm, merged_features_gc_gm, colnamesfeatures_gc_gm_all)  
    file_handling.writeCsv(outputfeatures_gc_dp, merged_features_gc_dp, colnamesfeatures_gc_dp_all)  
    file_handling.writeCsv(outputfeatures_gm_dp, merged_features_gm_dp, colnamesfeatures_gm_dp_all) 
    file_handling.writeCsv(outputfeatures_dp_gm_gc_all,merged_dp_gm_gc_all,colnames_dp_gm_gc_all )
    file_handling.writeCsv(outputfeaturesdp_gm_gc_features, merged_features_dp_gc_gm,colnamesfeatures_dp_gm_gc_all,)

    

def runTest(section):
    args= readCongigFile(section)
    input_gc= args[0]
    input_dp= args[1]
    input_gm= args[2]
    output_all=args[3]
    output_features=args[4]
    datafields=list(args[5].split(","))
    executeTest(input_gc,input_dp,input_gm,output_all,output_features,datafields)
    
def executeTest(input_gc,input_dp,input_gm,output_all,output_features,datafields):
    gcallrows, dpallrows,  gmallrows,gcfeatures, dpfeatures, gmfeatures =mergerows(input_gc,input_dp,input_gm,datafields)
    
    outputall_gc=os.path.join(os.path.dirname(output_all),"mt1_truelabel_GC_gt55_argQualPairsTest_with_all47_features_norm.csv")
    outputall_dp=os.path.join(os.path.dirname(output_all),"mt1_truelabel_DP_gt55_argQualPairsTest_with_all47_features_norm.csv")
    outputall_gm=os.path.join(os.path.dirname(output_all),"mt1_truelabel_GM_gt55_argQualPairsTest_with_all47_features_norm.csv")
    outputfeatures_gc=os.path.join(os.path.dirname(output_all),"mt1_truelabel_GC_gt55_argQualPairsTest_only47_features_norm.csv")
    outputfeatures_dp=os.path.join(os.path.dirname(output_all),"mt1_truelabel_DP_gt55_argQualPairsTest_only47_features_norm.csv")
    outputfeatures_gm=os.path.join(os.path.dirname(output_all),"mt1_truelabel_GM_gt55_argQualPairsTest_only47_features_norm.csv")
    
    colnamesgcall=sorted(list(gcallrows[0].keys()))
    colnamesdpall=sorted(list(dpallrows[0].keys()))
    colnamesgmall=sorted(list(gmallrows[0].keys()))
    colnamesgcfeatures=sorted(list(gcfeatures[0].keys()))
    colnamesdpfeatures=sorted(list(dpfeatures[0].keys()))
    colnamesgmfeatures=sorted(list(gmfeatures[0].keys()))
    
    
    movelabeltoLast(colnamesgcall)
    movelabeltoLast(colnamesgcfeatures)
    movelabeltoLast(colnamesdpall)
    movelabeltoLast(colnamesdpfeatures)
    movelabeltoLast(colnamesgmall)
    movelabeltoLast(colnamesgmfeatures)
    
        
    file_handling.writeCsv(outputall_gc, gcallrows, colnamesgcall)  
    file_handling.writeCsv(outputall_dp, dpallrows,  colnamesdpall)  
    file_handling.writeCsv(outputall_gm, gmallrows, colnamesgmall)  
    file_handling.writeCsv(outputfeatures_gc, gcfeatures, colnamesgcfeatures)  
    file_handling.writeCsv(outputfeatures_dp, dpfeatures, colnamesdpfeatures)  
    file_handling.writeCsv(outputfeatures_gm, gmfeatures, colnamesgmfeatures)     
       
    
if __name__ == '__main__':
  
    
    section ="merge_dp_gc_gm_train"
    run(section)
    section="create_dp_gc_gm_test"
    runTest(section)
    
    