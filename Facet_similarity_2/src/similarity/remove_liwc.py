'''
Created on Aug 22, 2015
Read the LIWC categories to be removed from a file and remove them from training and test.
@author: amita
'''
import  configparser
from utilities import file_handling

def execute(input_train_csv,input_test_csv,output_train_csv,output_test_csv,liwc_remove):
    cats_remove=file_handling.readCsv(liwc_remove)
    remove_cat_key=[ row["ignore_cat"] for row in cats_remove]
    rowstrain=file_handling.readCsv(input_train_csv)
    rowstest=file_handling.readCsv(input_test_csv)
    
    trainkeys=rowstrain[0].keys()
    testkeys=rowstest[0].keys()
    
    filtered_trainkeys=trainkeys - remove_cat_key
    filtered_testkeys=testkeys-remove_cat_key
    
    file_handling.writeCsv(output_train_csv, rowstrain, filtered_trainkeys)
    file_handling.writeCsv(output_test_csv, rowstest, filtered_testkeys)
    
    
def readCongigFile():
    config = configparser.ConfigParser()
    config.read('remove_liwc_Config.ini')
    input_train_csv = config.get('GC', 'input_train_csv')
    input_test_csv=config.get('GC', 'input_test_csv')
    output_train_csv= config.get('GC', 'output_train_csv')
    output_test_csv=config.get('GC', 'output_test_csv')
    liwc_remove=config.get('GC', 'liwc_remove')
    arguments=(input_train_csv,input_test_csv,output_train_csv, output_test_csv,liwc_remove)
    return  (arguments)

def runGC():
    args= readCongigFile()
    input_train_csv = args[0]
    input_test_csv=args[1]
    output_train_csv=args[2]
    output_test_csv=args[3]
    liwc_remove= args[4]
    execute(input_train_csv,input_test_csv,output_train_csv,output_test_csv,liwc_remove)


if __name__ == '__main__':
    runGC()