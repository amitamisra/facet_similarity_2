'''
Created on Jul 15, 2015

@author: amita
'''
import urllib.request
import gensim
def word2vec():
    model = gensim.models.Word2Vec.load_word2vec_format('/Users/amita/Facet_similarity_part2/Data/word2vec/GoogleNews-vectors-negative300.bin', binary=True)
   
    print (model.most_similar(positive=["rights"]))
    print (model.most_similar(negative=["rights"]))
    
   
    print (model.most_similar(positive=["death"]))
    print (model.most_similar(negative=["death"]))
    
    
    print (model.most_similar(positive=["amendment"]))
    print (model.most_similar(negative=["amendment"]))
    
    print (model.most_similar(positive=["constitution"]))
    print (model.most_similar(negative=["constitution"]))
    
    print (model.most_similar(positive=["control"]))
    print (model.most_similar(negative=["control"]))
    
    print (model.most_similar(positive=["arms"]))
    print (model.most_similar(negative=["arms"]))
    
def UMBC(label1, label2):
    
    url="""http://swoogle.umbc.edu/SimService/GetSimilarity?operation=api&phrase1=""" + label1+ """ &phrase2=""" + label2
    response=urllib.request.urlopen(url).read()
    return  (response.text.strip())


if __name__ =="main":
    label1="arms"
    label2="weapon"
    re=UMBC(label1, label2)
    print (re)
    
    