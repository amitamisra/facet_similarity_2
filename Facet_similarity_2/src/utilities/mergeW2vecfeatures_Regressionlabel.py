'''
Created on Apr 11, 2016
merge word2vec sentences features with true regression label for train and test set
@author: amita
'''

import pandas as pd
import configparser

def readConfigFile(section):
    config = configparser.ConfigParser()
    config.read('mergeW2vecfeatures_Regressionlabel_Config.ini')
    csv_mergecols=config.get(section,"csv_mergecols")
    csv_features=config.get(section,"csv_features")
    csv_result=config.get(section,"csv_result")
    collisttoappend=list(config.get(section,"collisttoappend").split(","))
    execute(csv_mergecols,csv_features,csv_result,collisttoappend)


def execute(csv_mergecols,csv_features,csv_result,collisttoappend):
    separator=","
    encod="utf-8"
    joincolscsv(csv_mergecols,csv_features,csv_result,separator,encod,collisttoappend)


#add columns from csv1 to resultscsv
#cols to add are in collisttoappend, if all then it appends all the columns from csv1 else appends
#only colnames in collisttoappend
def joincolscsv(csv_mergecols,csv_features,csv_result,separator,encod,collisttoappend):
    df1=pd.read_csv(csv_mergecols, sep=separator, encoding=encod)
    if 'all' in collisttoappend:
        df_cols=df1
    else:    
        df_cols = df1[collisttoappend]
    df_features=pd.read_csv(csv_features, sep=separator, encoding=encod)
    df_features.columns = ["Word2Vecfeature_" + str(col)  for col in df_features.columns]
    df_result=df_features.join(df_cols)
    df_result.to_csv(csv_result,index=False)

def run(section):
    readConfigFile(section)
        
if __name__ == '__main__':
    
    section="GC_combined_allfeaturesTrain300"
    run(section)
    section="GM_combined_allfeaturesTrain300"
    run(section)
    section="DP_combined_allfeaturesTrain300"
    run(section)
    section="GC_combined_allfeaturesTest300"
    run(section)
    section="GM_combined_allfeaturesTest300"
    run(section)
    section="DP_combined_allfeaturesTest300"
    run(section)
    section="GC_combined_allfeaturesTrain300Google"
    run(section)
    section="GM_combined_allfeaturesTrain300Google"
    run(section)
    section="DP_combined_allfeaturesTrain300Google"
    run(section)
    section="GC_combined_allfeaturesTest300Google"
    run(section)
    section="GM_combined_allfeaturesTest300Google"
    run(section)
    section="DP_combined_allfeaturesTest300Google"
    run(section)
   
    #--------------------------------------------------------- section="GCTrain"
    #-------------------------------------------------------------- run(section)
    #--------------------------------------------------------- section="GMTrain"
    #-------------------------------------------------------------- run(section)
    #--------------------------------------------------------- section="DPTrain"
    #-------------------------------------------------------------- run(section)
    #-------------------------------------------------------------- run(section)
    #---------------------------------------------------------- section="GCTest"
    #-------------------------------------------------------------- run(section)
    #---------------------------------------------------------- section="GMTest"
    #-------------------------------------------------------------- run(section)
    #---------------------------------------------------------- section="DPTest"