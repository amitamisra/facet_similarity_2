'''
Created on Nov 18, 2015
create train and test split for argument pairs similarity for NAACL 2015.
@author: amita
'''
from utilities import file_handling

def split(inputfeaturescsv,inputdatacsv,totalrecords, trainrecordscount,outputtrainfeatures,outputtraindata,outputtestfeatures,outputtestdata,labelCol):
    
    file_handling.traintestsplit(inputfeaturescsv,inputdatacsv,totalrecords, trainrecordscount,outputtrainfeatures,outputtraindata,outputtestfeatures,outputtestdata,labelCol)
    
if __name__ == '__main__':
    
    
     # DO train test split for death penalty pairs
    inputdir="/Users/amita/facet_similarity_part2/data/deathpenalty"
    outputtraindir="/Users/amita/facet_similarity_part2/data/deathpenalty/greater_than_55/argumentSimilarity/traintestsplit/train"
    outputtestdir="/Users/amita/facet_similarity_part2/data/deathpenalty/greater_than_55/argumentSimilarity/traintestsplit/test"
    
    totalrecords=2000
    trainrecordscount=1800
    
    inputfeaturescsv=inputdir+"/greater_than_55/argumentSimilarity/mt1_truelabel_dp_gt55_argQualPairsTop2000_Onlyfeaturesnorm.csv"
    inputdatacsv=inputdir+"/greater_than_55/argumentSimilarity/mt1_truelabel_dp_gt55_argQualPairsTop2000_with_all_features_norm.csv"
    outputtrainfeatures=outputtraindir+"/mt1_truelabel_dp_gt55_argQualPairsTop2000_allTrainFeaturesnorm.csv"
    outputtraindata=outputtraindir+"/mt1_truelabel_dp_gt55_argQualPairsTop2000TrainData.csv"
    
    outputtestfeatures=outputtestdir+"/mt1_truelabel_dp_gt55_argQualPairsTop2000allTestFeaturesnorm.csv"
    outputtestdata=outputtestdir+"/mt1_truelabel_dp_gt55_argQualPairsTop2000TestData.csv"
    labelCol="regression_label"
    split(inputfeaturescsv,inputdatacsv,totalrecords,trainrecordscount,outputtrainfeatures,outputtraindata,outputtestfeatures,outputtestdata,labelCol) 
    
    
    
    
    
    
    
# DO TRAIN TEST SPLIT FOR  ARGUMEMT QUALITY PAIRS FOR  GUN CONTROL
    #------------ inputdir="/Users/amita/facet_similarity_part2/data/guncontrol"
    # outputtraindir="/Users/amita/facet_similarity_part2/data/guncontrol/greater_than_55/argumentQuality/argumentPairs/traintestsplit/train"
    # outputtestdir="/Users/amita/facet_similarity_part2/data/guncontrol/greater_than_55/argumentQuality/argumentPairs/traintestsplit/test"
#------------------------------------------------------------------------------ 
    #--------------------------------------------------------- totalrecords=2000
    #---------------------------------------------------- trainrecordscount=1800
#------------------------------------------------------------------------------ 
    # inputfeaturescsv=inputdir+"/greater_than_55/argumentQuality/argumentPairs/FeatureAblationInputCsvs/mt1_gc_gt55_argQualPairs_norm_featuresall47ProposedModel.csv"
    # inputdatacsv=inputdir+"/mechanicalTurk/greater_than_55/argumentQuality/argumentPairs/mt1_truelabel_gc_gt55_argQualPairsTop2000.csv"
#------------------------------------------------------------------------------ 
    # outputtrainfeatures=outputtraindir+"/mt1_truelabel_gc_gt55_argQualPairsTop200TrainFeatures.csv"
    # outputtraindata=outputtraindir+"/mt1_truelabel_gc_gt55_argQualPairsTop200TrainData.csv"
#------------------------------------------------------------------------------ 
    # outputtestfeatures=outputtestdir+ "/mt1_truelabel_gc_gt55_argQualPairsTop200TestFeatures.csv"
    # outputtestdata=outputtestdir+ "/mt1_truelabel_gc_gt55_argQualPairsTop200TestData.csv"
    # split(inputfeaturescsv,inputdatacsv,totalrecords,trainrecordscount,outputtrainfeatures,outputtraindata,outputtestfeatures,outputtestdata)
    
    
    
    
    
    # DO TRAIN TEST SPLIT FOR GAY MARRIAGE ARGUMEMT QUALITY PAIRS
    
    #----------- inputdir="/Users/amita/facet_similarity_part2/data/gayMarriage"
    # outputtraindir="/Users/amita/facet_similarity_part2/data/gayMarriage/greater_than_55/argumentSimilarity/traintestsplit/train"
    # outputtestdir="/Users/amita/facet_similarity_part2/data/gayMarriage/greater_than_55/argumentSimilarity/traintestsplit/test"
#------------------------------------------------------------------------------ 
    #--------------------------------------------------------- totalrecords=2000
    #---------------------------------------------------- trainrecordscount=1800
#------------------------------------------------------------------------------ 
    # inputfeaturescsv=inputdir+"/greater_than_55/argumentSimilarity/mt1_truelabel_gm_gt55_argQualPairsTop2000_Onlyfeaturesnorm.csv"
    # inputdatacsv=inputdir+"/greater_than_55/argumentSimilarity/mt1_truelabel_gm_gt55_argQualPairsTop2000_with_all_features_norm.csv"
    # outputtrainfeatures=outputtraindir+"/mt1_truelabel_gm_gt55_argQualPairsTop2000_allTrainFeaturesnorm.csv"
    # outputtraindata=outputtraindir+"/mt1_truelabel_gm_gt55_argQualPairsTop2000TrainData.csv"
#------------------------------------------------------------------------------ 
    # outputtestfeatures=outputtestdir+ "/mt1_truelabel_gm_gt55_argQualPairsTop2000allTestFeaturesnorm.csv"
    # outputtestdata=outputtestdir+ "/mt1_truelabel_gm_gt55_argQualPairsTop2000TestData.csv"
    #----------------------------------------------- labelCol="regression_label"
    # split(inputfeaturescsv,inputdatacsv,totalrecords,trainrecordscount,outputtrainfeatures,outputtraindata,outputtestfeatures,outputtestdata,labelCol)
#------------------------------------------------------------------------------ 
#------------------------------------------------------------------------------ 
    
   
    