'''
Created on Oct 6, 2015

@author: amita
'''
from pydoc import help
from scipy.stats.stats import pearsonr
from utilities import file_handling
import pandas as pd
import os, copy,sys
from collections import OrderedDict


def pearson(list1,list2):
    ans=pearsonr(list1, list2)
    return ans[0]

def crossfolds(rows,numberoffolds): 
    itemsinfold=int(len(rows)/numberoffolds)
    crosslist= [rows[i:i+itemsinfold] for i in range(0,len(rows),itemsinfold)]
    return crosslist
       
def pearsonCVfolds(inputfile,actualCol, predictCol,numOfFold,noOfinstances):
    dataFrame=pd.read_csv(inputfile, nrows=noOfinstances)
    columns=[actualCol, predictCol]
    #colslist=[(row[actualCol],row[PredictCol])  for row in rows1 ]
    dataFrame[columns] = dataFrame[columns].convert_objects(convert_numeric=True)
    actualColList=list(dataFrame[actualCol])
    predictColList=list(dataFrame[predictCol])
    Rsquared=pearson(actualColList,predictColList)
    crosslistActual=crossfolds(actualColList,numOfFold)
    crosslistPredict=crossfolds(predictColList,numOfFold)
    corrlist=[]
    for count in range(0,len(crosslistActual)):
        actualFold=crosslistActual[count]
        predictFold=crosslistPredict[count]
        corrlist.append(pearson(actualFold,predictFold))
    return (corrlist,Rsquared)     

def writeCorrFolds(output, featureName, corrlist):
    allRows=[]
    if os.path.isfile(output):
        rows=file_handling.readCsv(output)
        i=0
        if len(rows) == len(corrlist):
                for row in rows:
                    newdict= OrderedDict()
                    newdict=copy.deepcopy(row)
                    newdict[featureName]=corrlist[i]
                    i=i+1
                    allRows.append(newdict)
        else:
            print("Number of records do not match in correlation file, check number of folds in each file")
            sys.exit(-1);
    else:
        for count in range(0, len(corrlist)) :
            newdict=newdict= OrderedDict()
            newdict[featureName]=corrlist[count]
            allRows.append(newdict)
    
    df = pd.DataFrame(allRows) 
    return df

def executepearson(inputfile1,inputfile2,output,actualCol, predictCol,numOfFold,noOfinstances):
    corrList1_Rsquared=pearsonCVfolds(inputfile1,actualCol, predictCol,numOfFold,noOfinstances)
    corrList2_Rsquared=pearsonCVfolds(inputfile2,actualCol, predictCol,numOfFold,noOfinstances)
    if os.path.exists(output):
        os.remove(output)
    featureName1=os.path.basename(inputfile1[:-4]) +"_CorrFolds:" + str(numOfFold)
    Rsquared1=corrList1_Rsquared[1]
    df=writeCorrFolds(output, featureName1, corrList1_Rsquared[0])
    df.insert(0, "RsquaredAvg"+featureName1,  Rsquared1)
    df.to_csv(output, encoding='utf-8',index=False)
    featureName2=os.path.basename(inputfile2[:-4]) +"_CorrFolds:" + str(numOfFold)
    df=writeCorrFolds(output, featureName2, corrList2_Rsquared[0])
    Rsquared2=corrList2_Rsquared[1]
    df.insert(0, "RsquaredAvg"+featureName2,  Rsquared2)
    df.to_csv(output, encoding='utf-8',index=False)
    
     
       
if __name__ == '__main__':
#------------------------------------------------------------------------------  for Cross VAlidation with 2000 instances 
    # inputfile1="/Users/amita/facet_similarity_part2/data/guncontrol/greater_than_55/argumentQuality/argumentPairs/WekaResults/Cross/SMO/mt1_gc_gt55_argQualPairs_norm_featuresall47ProposedModelSMOPredict.csv"
    # inputfile2="/Users/amita/facet_similarity_part2/data/guncontrol/greater_than_55/argumentQuality/argumentPairs/WekaResults/Cross/SMO/mt1_gc_gt55_argQualPairs_norm_featureLIWC_And_SimpleDepSMOCrossPredict.csv"
    # output="/Users/amita/facet_similarity_part2/data/guncontrol/greater_than_55/argumentQuality/argumentPairs/WekaResults/Cross/SMO/correlationLIWC_SimpleDepAll.csv"
    #------------------------------------------------------------- col1="actual"
    #---------------------------------------------------------- col2="predicted"
    #-------------------------------------------------------------- numOfFold=10
    #-------------------------------------------------------- noOfinstances=2000
    # executepearson(inputfile1,inputfile2,output,col1,col2,numOfFold,noOfinstances)
#------------------------------------------------------------------------------ 
#------------------------------------------------------------------------------ 


    # Done for gun control
    # inputdir="/Users/amita/facet_similarity_part2/data/guncontrol/greater_than_55/argumentQuality/argumentPairs/traintestsplit/WekaResults/featureablation/"
    # inputfile1= inputdir + "/AllFeatures/SMO/CVmt1_truelabel_gc_gt55_argQualPairsTop200TrainFeaturesPredict.csv"
    # inputfile2= inputdir +"/Rouge/SMO/CVmt1_truelabel_gc_gt55_argQualPairsTop200TrainRougePredict.csv"
    #----------------- inputfile3= inputdir+"/LIWC_ROUGE/CV_LIWC_ROUGETrain.csv"
    # inputfile4=inputdir+"/LIWC/SMO/CVmt1_truelabel_gc_gt55_argQualPairsTop200TrainLIWC_simpleDepPredict.csv"
#------------------------------------------------------------------------------ 
    #-------- output2=inputdir+"PairedTTest/SMOAllFeaturesTrain__Rouge_Corr.csv"
    # output3=inputdir+"PairedTTest/SMOAllfeaturesTrain__LIWC_DEP_ROUGECorr.csv"
    #----------- output4=inputdir+"PairedTTest/SMOAllfeaturesTrain_LIWCCorr.csv"
    
    
    
  
    
    # inputdir="/Users/amita/facet_similarity_part2/data/gayMarriage/greater_than_55/argumentSimilarity/traintestsplit/WekaResults/featureablation/"
#------------------------------------------------------------------------------ 
    # inputfileallSMO= inputdir + "/all47features/SMO/all47featuresNormTrain_gm_gt55_argQualPairs_Predict.csv"
    # inputfileallLR= inputdir +  "/all47features/LinearRegression/all47featuresNormTrain_gm_gt55_argQualPairsPredict.csv"
    # inputfileallGaussian=inputdir +"/Rouge_LIWC_DEP_Ngram_UmbcSim/GaussianPuk/rouge_liwc_dep_ngram_umbc_simNormTrain_gm_gt55_argQualPairsPredict.csv"
#------------------------------------------------------------------------------ 
#------------------------------------------------------------------------------ 
    #-------------------------------------------- # perform paired TTest for SMO
    # inputfile2= inputdir +"/Rouge/SMO/RougeallNormTrain_gm_gt55_argQualPairsPredict.csv"
    # inputfile3= inputdir+"/LIWC_SimpleDep/SMO/LIWC_SimpleDepNormTrain_gm_gt55_argQualPairsPredict.csv"
    # inputfile4=inputdir+"/Rouge_LIWC_Dep/SMO/rouge_liwc_depNormTrain_gm_gt55_argQualPairsPredict.csv"
#------------------------------------------------------------------------------ 
    #------- output2=inputdir+"PairedTTest/SMOAll47FeaturesTrain__RougeCorr.csv"
    # output3=inputdir+"PairedTTest/SMOAll47featuresTrain__LIWC_SimpleDepCorr.csv"
    # output4=inputdir+"PairedTTest/SMOAll47featuresTrain__LIWCSimpleDepRougeCorr.csv"
#------------------------------------------------------------------------------ 
    # executepearson(inputfileallSMO,inputfile2,output2,col1,col2,numOfFold,noOfinstances)
    # executepearson(inputfileallSMO,inputfile3,output3,col1,col2,numOfFold,noOfinstances)
    # executepearson(inputfileallSMO,inputfile4,output4,col1,col2,numOfFold,noOfinstances)
#------------------------------------------------------------------------------ 
#------------------------------------------------------------------------------ 
     #-------------------------------------------- # perform paired TTest for LR
    # inputfile2= inputdir +"/Rouge/LinearRegression/RougeallNormTrain_gm_gt55_argQualPairsPredict.csv"
    # inputfile3= inputdir+"/LIWC_SimpleDep/LinearRegression/LIWC_SimpleDepNormTrain_gm_gt55_argQualPairsPredict.csv"
    # inputfile4=inputdir+"/Rouge_LIWC_Dep/LinearRegression/rouge_liwc_depNormTrain_gm_gt55_argQualPairsPredict.csv"
#------------------------------------------------------------------------------ 
    #-------- output2=inputdir+"PairedTTest/LRAll47FeaturesTrain__RougeCorr.csv"
    # output3=inputdir+"PairedTTest/LRAll47featuresTrain__LIWC_SimpleDepCorr.csv"
    # output4=inputdir+"PairedTTest/LRAll47featuresTrain__RougeLIWCSimpleDepCorr.csv"
#------------------------------------------------------------------------------ 
    # executepearson(inputfileallLR,inputfile2,output2,col1,col2,numOfFold,noOfinstances)
    # executepearson(inputfileallLR,inputfile3,output3,col1,col2,numOfFold,noOfinstances)
    # executepearson(inputfileallLR,inputfile4,output4,col1,col2,numOfFold,noOfinstances)
#------------------------------------------------------------------------------ 
#------------------------------------------------------------------------------ 
    #---------------------------------------- #perform paired TTest for gaussian
    # inputfile2= inputdir +"/Rouge/GaussianPuk/RougeallNormTrain_gm_gt55_argQualPairsPredict.csv"
    # inputfile3= inputdir+"/LIWC_SimpleDep/GaussianPuk/LIWC_SimpleDepNormTrain_gm_gt55_argQualPairsPredict.csv"
    # inputfile4=inputdir+"/Rouge_LIWC_Dep/GaussianPuk/rouge_liwc_depNormTrain_gm_gt55_argQualPairsPredict.csv"
#------------------------------------------------------------------------------ 
    #---------- output2=inputdir+"PairedTTest/GP_46FeaturesTrain__RougeCorr.csv"
    #- output3=inputdir+"PairedTTest/GP_46featuresTrain__LIWC_SimpleDepCorr.csv"
    # output4=inputdir+"PairedTTest/GP_46featuresTrain__LIWCSimpleDepRougeCorr.csv"
#------------------------------------------------------------------------------ 
    # executepearson(inputfileallGaussian,inputfile2,output2,col1,col2,numOfFold,noOfinstances)
    # executepearson(inputfileallGaussian,inputfile3,output3,col1,col2,numOfFold,noOfinstances)
    # executepearson(inputfileallGaussian,inputfile4,output4,col1,col2,numOfFold,noOfinstances)


    col1="actual"
    col2="predicted"
    numOfFold=10
    noOfinstances=1800
    
    #=========================================================================== done for gun control
    # inputdir="/Users/amita/facet_similarity_part2/data/guncontrol/greater_than_55/argumentQuality/argumentPairs/traintestsplit/WekaResults/featureablation"
    # 
    # inputfileallSMO= inputdir + "/AllFeatures/SMO/CVmt1_truelabel_gc_gt55_argQualPairsTop200TrainFeaturesPredict.csv"
    # inputfileallLR= inputdir +   "/all47features/LinearRegression/all47featuresNormTrain_gm_gt55_argQualPairsPredict.csv"
    # inputfileallGaussian=inputdir + "/LIWC_ROUGE_NGRAM_UMBCSimilarity/GaussianPuk/LIWC_ROUGE_NGRAM_UMBCSimilarityPredict.csv"
    # 
    # 
    # # perform paired TTest for SMO
    # inputfile2= inputdir +"/Rouge/SMO/CVmt1_truelabel_gc_gt55_argQualPairsTop200TrainRougePredict.csv"
    # inputfile3= inputdir+"/LIWC/SMO/CVmt1_truelabel_gc_gt55_argQualPairsTop200TrainLIWC_simpleDepPredict.csv"
    # inputfile4=inputdir+"/LIWC_ROUGE/SMO/CV_LIWC_ROUGETrain.csv"
    # 
    # output2=inputdir+"/PairedTTest/SMOAll47FeaturesTrain__RougeCorr.csv"
    # output3=inputdir+"/PairedTTest/SMOAll47featuresTrain__LIWC_SimpleDepCorr.csv"
    # output4=inputdir+"/PairedTTest/SMOAll47featuresTrain__LIWCSimpleDepRougeCorr.csv"
    # 
    # executepearson(inputfileallSMO,inputfile2,output2,col1,col2,numOfFold,noOfinstances)
    # executepearson(inputfileallSMO,inputfile3,output3,col1,col2,numOfFold,noOfinstances)
    # executepearson(inputfileallSMO,inputfile4,output4,col1,col2,numOfFold,noOfinstances)
    # 
    #===========================================================================
    
     #-------------------------------------------- # perform paired TTest for LR
    # inputfile2= inputdir +"/Rouge/LinearRegression/RougeallNormTrain_gm_gt55_argQualPairsPredict.csv"
    # inputfile3= inputdir+"/LIWC_SimpleDep/LinearRegression/LIWC_SimpleDepNormTrain_gm_gt55_argQualPairsPredict.csv"
    # inputfile4=inputdir+"/Rouge_LIWC_Dep/LinearRegression/rouge_liwc_depNormTrain_gm_gt55_argQualPairsPredict.csv"
#------------------------------------------------------------------------------ 
    #-------- output2=inputdir+"PairedTTest/LRAll47FeaturesTrain__RougeCorr.csv"
    # output3=inputdir+"PairedTTest/LRAll47featuresTrain__LIWC_SimpleDepCorr.csv"
    # output4=inputdir+"PairedTTest/LRAll47featuresTrain__RougeLIWCSimpleDepCorr.csv"
       
    
    #perform paired TTest for gaussian  gun control
      
    # inputfile2= inputdir +"/Rouge/GaussianPuk/mt1_truelabel_gc_gt55_argQualPairsTop200TrainRougePredict.csv"
    # inputfile3= inputdir+"/LIWC/GaussianPuk/mt1_truelabel_gc_gt55_argQualPairsTop200TrainLIWC_simpleDepFeaturesPredict.csv"
    #-- inputfile4=inputdir+"/LIWC_ROUGE/GaussianPuk/LIWC_ROUGETrainPredict.csv"
#------------------------------------------------------------------------------ 
    #--------- output2=inputdir+"/PairedTTest/GP_46FeaturesTrain__RougeCorr.csv"
    # output3=inputdir+"/PairedTTest/GP_46featuresTrain__LIWC_SimpleDepCorr.csv"
    # output4=inputdir+"/PairedTTest/GP_46featuresTrain__LIWCSimpleDepRougeCorr.csv"
#------------------------------------------------------------------------------ 
    # executepearson(inputfileallGaussian,inputfile2,output2,col1,col2,numOfFold,noOfinstances)
    # executepearson(inputfileallGaussian,inputfile3,output3,col1,col2,numOfFold,noOfinstances)
    # executepearson(inputfileallGaussian,inputfile4,output4,col1,col2,numOfFold,noOfinstances)
    
    
    
    
    
    
    #PairedTTEStfor Gaussian DeathPenalty
    
    # inputdir="/Users/amita/facet_similarity_part2/data/deathPenalty/greater_than_55/argumentSimilarity/traintestsplit/WekaResults/featureablation"
    # inputfileallGaussian=inputdir+"/all47Features/GaussianPuk/CVall47GPukNormTrain_dp_gt55_argQualPairsPredict.csv"
#------------------------------------------------------------------------------ 
    # inputfile1= inputdir +"/NgramCosine/GaussianPuk/NgramCosineNormGPukTrain_dp_gt55_argQualPairsPredict.csv"
    # inputfile2=inputdir +"/Rouge/GaussianPuk/CVRougeallNormNormGPukTrain_dp_gt55_argQualPairsPredict.csv"
    # inputfile3=inputdir +"/LIWC_SimpleDep/GaussianPuk/CVLIWC_SimpleDepGPukNormTrain_dp_gt55_argQualPairsPredict.csv"
    # inputfile4=inputdir +"/Rouge_LIWC_Dep/GaussianPuk/CVrouge_liwc_depGPukNormTrain_dp_gt55_argQualPairsPredict.csv"
    # inputfile5=inputdir +"/UMBCSimilarity/GaussianPuk/CVUMBCSimilarityfeaturesNormGPukTrain_dp_gt55_argQualPairsPredict.csv"
#------------------------------------------------------------------------------ 
    # output1=inputdir +"/PairedTTest/GP_all47FeaturesTrain__NgramCosineCorr.csv"
    #----- output2=inputdir +"/PairedTTest/GP_all47FeaturesTrain__RougeCorr.csv"
    # output3=inputdir +"/PairedTTest/GP_all47featuresTrain__LIWC_SimpleDepCorr.csv"
    # output4=inputdir +"/PairedTTest/GP_all47featuresTrain__LIWCSimpleDepRougeCorr.csv"
    # output5=inputdir +"/PairedTTest/GP_all47featuresTrain__UMBCSimilarity_Corr.csv"
#------------------------------------------------------------------------------ 
    # executepearson(inputfileallGaussian,inputfile1,output1,col1,col2,numOfFold,noOfinstances)
    # executepearson(inputfileallGaussian,inputfile2,output2,col1,col2,numOfFold,noOfinstances)
    # executepearson(inputfileallGaussian,inputfile3,output3,col1,col2,numOfFold,noOfinstances)
    # executepearson(inputfileallGaussian,inputfile4,output4,col1,col2,numOfFold,noOfinstances)
    # executepearson(inputfileallGaussian,inputfile5,output5,col1,col2,numOfFold,noOfinstances)
    
    
    #===========================================================================
    # inputfile2a=inputdir +
    # inputfile2b=inputdir +
    # output2=inputdir +
    #===========================================================================
    
    
    #GUN CONTROL  NAACL 2016,  NGRAM VS ALL
    inputdir="/Users/amita/facet_similarity_part2/data/guncontrol/greater_than_55/argumentQuality/argumentPairs/traintestsplit/WekaResults/featureablation/"
    
    input_ngram=inputdir+"/NgramCosine/GaussianPuk/mt1_truelabel_gc_gt55_argQualPairsTop200TrainNgramCosinePredict.csv"
    
    input_ngram_extended=inputdir+"NGRAM_Extended/GausssianPuk/Ngram_extended_GC_Predict.csv"
    output_ngram__ngram_extended=inputdir + "/PairedTTest/ngram__ngramextended.csv"
    
    input_ngram_liwc=inputdir+"NGRAM_LIWC/GaussianPuk/NGARM_LIWC_TrainPredict_GC.csv"
    output_ngram__ngramliwc=inputdir + "/PairedTTest/ngram__ngramliwc.csv"
    
    input_ngram_rouge=inputdir +"NGRAM_ROUGE/GaussianPuk/NGRAM_ROUGE_GC.csv"
    output_ngram__ngramrouge=inputdir + "/PairedTTest/ngram__ngramrouge.csv"
    
    input_ngram_umbc=inputdir +"NGRAM_UMBC/GaussianPuk/ngram_umbc_GC_Predict.csv"
    output_ngram__ngramumbc=inputdir + "/PairedTTest/ngram__ngramumbc.csv"
    
    input_liwc_rouge=inputdir +"LIWC_ROUGE/GaussianPuk/LIWC_ROUGETrainPredict.csv"
    output_ngram__ngramliwcrouge=inputdir + "/PairedTTest/ngram__ngramliwcrouge.csv"
    
    input_ngramliwcrouge=inputdir +"LIWC_ROUGE_NGRAM/GaussianPuk/LIWC_rouge_NgramTrainPredict.csv"
    output_liwc_rouge__ngramliwcrouge=inputdir + "/PairedTTest/liwc_rouge__ngramliwcrouge.csv"
    
    input_ngramliwcrougeumbc=inputdir + "LIWC_ROUGE_NGRAM_UMBCSimilarity/GaussianPuk/LIWC_ROUGE_NGRAM_UMBCSimilarityPredict.csv" 
    output_ngramliwcrouge__ngramliwc_rouge_umbc=inputdir + "/PairedTTest/ngramliwcrouge__ngramliwcrougeumbc.csv"
    
    executepearson(input_ngram,input_ngram_extended,output_ngram__ngram_extended,col1,col2,numOfFold,noOfinstances)
    executepearson(input_ngram,input_ngram_liwc,output_ngram__ngramliwc,col1,col2,numOfFold,noOfinstances)
    executepearson(input_ngram,input_ngram_rouge,output_ngram__ngramrouge,col1,col2,numOfFold,noOfinstances)
    executepearson(input_ngram,input_ngram_umbc,  output_ngram__ngramumbc,col1,col2,numOfFold,noOfinstances)
    executepearson(input_liwc_rouge,input_ngramliwcrouge,output_ngram__ngramliwcrouge,col1,col2,numOfFold,noOfinstances)
    executepearson(input_ngramliwcrouge,input_ngramliwcrougeumbc,output_ngramliwcrouge__ngramliwc_rouge_umbc,col1,col2,numOfFold,noOfinstances)
    
    
    
    
    
  #  GAY MARRIAGE NAACL 
    inputdir="/Users/amita/facet_similarity_part2/data/gayMarriage/greater_than_55/argumentSimilarity/traintestsplit/WekaResults/featureablation/"
    
    input_ngram=inputdir+"NgramCosine/GaussianPuk/NgramCosineNormTrain_gm_gt55_argQualPairsPredict.csv"
    
    #input_ngram_extended=inputdir+"NGRAMExtended/GaussianPuk/NGRAM_EXtended_GMPredict.csv"
    #output_ngram__ngram_extended=inputdir + "/PairedTTest/ngram__ngramextendedGM.csv"
    
    input_ngram_liwc=inputdir+"NGRAM_LIWCDep/Gaussian/NGRAM_LIWC_GM.csv"
    output_ngram__ngramliwc=inputdir + "/PairedTTest/ngram__ngramliwcGM.csv"
    
    input_ngram_rouge=inputdir +"NGRAMROUGE/GaussianPuk/NGRAM_ROUGE_GM.csv"
    output_ngram__ngramrouge=inputdir + "/PairedTTest/ngram__ngramrougeGM.csv"
    
    input_ngram_umbc=inputdir +"NGRAMUMBC/GaussianPuk/ngram_umbc_Predict_GM.csv"
    output_ngram__ngramumbc=inputdir + "/PairedTTest/ngram__ngramumbcGM.csv"
    
    input_liwc_rouge=inputdir +"Rouge_LIWC_Dep/GaussianPuk/rouge_liwc_depNormTrain_gm_gt55_argQualPairsPredict.csv"
    output_ngram__ngramliwcrouge=inputdir + "/PairedTTest/ngram__ngramliwcrougeGM.csv"
    
    input_ngramliwcrouge=inputdir +"/Rouge_Liwc_Dep_Ngram/GaussianPuk/Rouge_Liwc_dep_NgramNormTrain_gm_gt55_argQualPairsPredict.csv"
    output_liwc_rouge__ngramliwcrouge=inputdir + "/PairedTTest/liwc_rouge__ngramliwcrougeGM.csv"
    
    input_ngramliwcrougeumbc=inputdir + "Rouge_LIWC_DEP_Ngram_UmbcSim/GaussianPuk/rouge_liwc_dep_ngram_umbc_simNormTrain_gm_gt55_argQualPairsPredict.csv" 
    output_ngramliwcrouge__ngramliwc_rouge_umbc=inputdir + "/PairedTTest/ngramliwcrouge__ngramliwcrougeumbcGM.csv"
    
    #executepearson(input_ngram,input_ngram_extended,output_ngram__ngram_extended,col1,col2,numOfFold,noOfinstances)
    executepearson(input_ngram,input_ngram_liwc,output_ngram__ngramliwc,col1,col2,numOfFold,noOfinstances)
    executepearson(input_ngram,input_ngram_rouge,output_ngram__ngramrouge,col1,col2,numOfFold,noOfinstances)
    executepearson(input_ngram,input_ngram_umbc,  output_ngram__ngramumbc,col1,col2,numOfFold,noOfinstances)
    executepearson(input_liwc_rouge,input_ngramliwcrouge,output_ngram__ngramliwcrouge,col1,col2,numOfFold,noOfinstances)
    executepearson(input_ngramliwcrouge,input_ngramliwcrougeumbc,output_ngramliwcrouge__ngramliwc_rouge_umbc,col1,col2,numOfFold,noOfinstances)
    
    
     
    #DEATH PENALTY NAACL 2016
    
    inputdir="/Users/amita/facet_similarity_part2/data/deathPenalty/greater_than_55/argumentSimilarity/traintestsplit/WekaResults/featureablation/"
    
    input_ngram=inputdir+"NgramCosine/GaussianPuk/NgramCosineNormGPukTrain_dp_gt55_argQualPairsPredict.csv"
    
    #input_ngram_extended=inputdir+"NGRAMExtended/GaussianPuk/NGRAM_EXtended_GMPredict.csv"
    #output_ngram__ngram_extended=inputdir + "/PairedTTest/ngram__ngramextendedGM.csv"
    
    input_ngram_liwc=inputdir+"NgramLIWCDep/GaussianPuk/NgramLIWCNormTrain_dp_gt55_argQualPairs.csv"
    output_ngram__ngramliwc=inputdir + "/PairedTTest/ngram__ngramliwcDP.csv"
    
    input_ngram_rouge=inputdir +"NGRAMROUGE/GaussianPuk/NGRAM_ROUGE_DP.csv"
    output_ngram__ngramrouge=inputdir + "/PairedTTest/ngram__ngramrougeDP.csv"
    
    input_ngram_umbc=inputdir +"Ngram_UMbcSim/GaussianPuk/CVngram_umbc_simGPukNrmTrain_dp_gt55QualPairsPredict.csv"
    output_ngram__ngramumbc=inputdir + "/PairedTTest/ngram__ngramumbcDP.csv"
    
    input_liwc_rouge=inputdir +"Rouge_LIWC_Dep/GaussianPuk/CVrouge_liwc_depGPukNormTrain_dp_gt55_argQualPairsPredict.csv"
    output_ngram__ngramliwcrouge=inputdir + "/PairedTTest/ngram__ngramliwcrougeDP.csv"
    
    input_ngramliwcrouge=inputdir +"Rouge_Liwc_Dep_Ngram/GaussianPuk/CVRouge_Liwc_dep_NgramGPukNormTrain_dp_gt55_argQualPairsPredict.csv"
    output_liwc_rouge__ngramliwcrouge=inputdir + "/PairedTTest/liwc_rouge__ngramliwcrougeDP.csv"
    
    input_ngramliwcrougeumbc=inputdir + "Rouge_LIWC_DEP_Ngram_UmbcSim/GaussianPuk/CVroge_liwc_dep_ngram_umbc_simGPukNrmTrain_dp_gt55QualPairsPredict.csv" 
    output_ngramliwcrouge__ngramliwc_rouge_umbc=inputdir + "/PairedTTest/ngramliwcrouge__ngramliwcrougeumbcDP.csv"
    
    #executepearson(input_ngram,input_ngram_extended,output_ngram__ngram_extended,col1,col2,numOfFold,noOfinstances)
    executepearson(input_ngram,input_ngram_liwc,output_ngram__ngramliwc,col1,col2,numOfFold,noOfinstances)
    executepearson(input_ngram,input_ngram_rouge,output_ngram__ngramrouge,col1,col2,numOfFold,noOfinstances)
    executepearson(input_ngram,input_ngram_umbc,  output_ngram__ngramumbc,col1,col2,numOfFold,noOfinstances)
    executepearson(input_liwc_rouge,input_ngramliwcrouge,output_liwc_rouge__ngramliwcrouge,col1,col2,numOfFold,noOfinstances)
    executepearson(input_ngramliwcrouge,input_ngramliwcrougeumbc,output_ngramliwcrouge__ngramliwc_rouge_umbc,col1,col2,numOfFold,noOfinstances)
    
