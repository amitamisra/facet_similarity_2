'''
Created on Mar 13, 2016
create only features csv for gc,gm,dp. Also split into train test
@author: amita
'''
import pandas as pd
from utilities import file_handling
featureList=["LIWC_","rouge","NGramCosine","liwc_simplified_dep_","UmbcSimilarity","UMBC_COMBINED","regression_label"]

def createfeatures(inputfeatures_data,outfeaturescsv,reg_label):
    train_df=pd.read_csv(inputfeatures_data,na_filter=False)
    colnames=list(train_df.columns.values)
    feature_cols=[col for col in colnames if str(col).startswith(tuple(featureList))]
    df_features=train_df[feature_cols]
    cols = df_features.columns.tolist()
    lastindex=len(cols)
    cols.insert(lastindex-1, cols.pop(cols.index(reg_label)))
    df_features = df_features.reindex(columns= cols)
    df_features.to_csv(outfeaturescsv,index=False)

if __name__ == '__main__':
    gcdata="/Users/amita/facet_similarity_part2/data/guncontrol/mechanicalTurk/greater_than_55/argumentQuality/argumentPairs/argumentPairs_AFS_V2/mt1_truelabel_gc_gt55_argQual_yes_gt3_comb_sim_max-sent-incl-10_sort_Top2000_features_norm.csv"
    gcfeatures="/Users/amita/facet_similarity_part2/data/guncontrol/mechanicalTurk/greater_than_55/argumentQuality/argumentPairs/argumentPairs_AFS_V2/mt1_truelabel_gc_gt55_argQual_yes_gt3_comb_sim_max-sent-incl-10_sort_Top2000_ONLYfeatures_norm.csv"
    
    dpdata="/Users/amita/facet_similarity_part2/data/deathPenalty/mechanicalTurk/greater_than_55/argumentQuality/argumentPairs/argumentPairs_AFS_V2/mt1_truelabel_dp_gt55_argQualPairsTop2000_features_norm.csv"
    dpfeatures="/Users/amita/facet_similarity_part2/data/deathPenalty/mechanicalTurk/greater_than_55/argumentQuality/argumentPairs/argumentPairs_AFS_V2/mt1_truelabel_dp_gt55_argQualPairsTop2000_ONLYfeatures_norm.csv"
    
    gmdata="/Users/amita/facet_similarity_part2/data/gayMarriage/mechanicalTurk/greater_than_55/argumentQuality/argumentPairs/argumentPairs_AFS_V2/mt1_truelabel_gm_gt55_argQualPairsTop2000_features_norm.csv"
    gmfeatures="/Users/amita/facet_similarity_part2/data/gayMarriage/mechanicalTurk/greater_than_55/argumentQuality/argumentPairs/argumentPairs_AFS_V2/mt1_truelabel_gm_gt55_argQualPairsTop2000_ONLYfeatures_norm.csv"
    totalrecords=2000
    trainrecordscount=1800
    labelcol="regression_label"

    gcdir="/Users/amita/facet_similarity_part2/data/guncontrol/greater_than_55/argumentQuality/argumentpairsV2/traintestsplit/"
    dpdir="/Users/amita/facet_similarity_part2/data/deathPenalty/greater_than_55/argumentQuality/argumentpairsV2/traintestsplit/"
    gmdir="/Users/amita/facet_similarity_part2/data/gayMarriage/greater_than_55/argumentQuality/argumentPairsV2/traintestsplit/"
    
    gctrainfeatures=gcdir+"train/mt1_truelabel_gc_gt55_argQual_yes_gt3_comb_sim_max-sent-incl-10_sort_Top2000_TrainOnlyfeatures_norm.csv"
    gctestfeatures=gcdir+"test/mt1_truelabel_gc_gt55_argQual_yes_gt3_comb_sim_max-sent-incl-10_sort_Top2000_TestOnlyfeatures_norm.csv"
    gctraindata=gcdir+"train/mt1_truelabel_gc_gt55_argQual_yes_gt3_comb_sim_max-sent-incl-10_sort_Top2000_Trainfeatures_norm.csv"
    gctestdata=gcdir +"test/mt1_truelabel_gc_gt55_argQual_yes_gt3_comb_sim_max-sent-incl-10_sort_Top2000_Testfeatures_norm.csv"
    
    dptrainfeatures=dpdir+"train/mt1_truelabel_dp_gt55_argQualPairsTop2000_TrainOnlyfeatures_norm.csv"
    dptestfeatures=dpdir+"test/mt1_truelabel_dp_gt55_argQualPairsTop2000_TestOnlyfeatures_norm.csv"
    dptraindata=dpdir+"train/mt1_truelabel_dp_gt55_argQualPairsTop2000_Trainfeatures_norm.csv"
    dptestdata=dpdir+"test/mt1_truelabel_dp_gt55_argQualPairsTop2000_Testfeatures_norm.csv"
    
    gmtrainfeatures=gmdir+"train/mt1_truelabel_gm_gt55_argQualPairsTop2000_TrainONLYfeatures_norm.csv"
    gmtestfeatures=gmdir+"test/mt1_truelabel_gm_gt55_argQualPairsTop2000_TestONLYfeatures_norm.csv"
    gmtraindata=gmdir+"train/mt1_truelabel_gm_gt55_argQualPairsTop2000_Trainfeatures_norm.csv"
    gmtestdata=gmdir+"test/mt1_truelabel_gm_gt55_argQualPairsTop2000_Testfeatures_norm.csv"
    
    
    #createfeatures(gcdata, gcfeatures,labelcol)
    #createfeatures(dpdata,dpfeatures,labelcol)
    createfeatures(gmdata,gmfeatures,labelcol)
    #file_handling.traintestsplit(gcfeatures,gcdata,totalrecords, trainrecordscount,gctrainfeatures,gctraindata,gctestfeatures,gctestdata, labelcol)
    file_handling.traintestsplit(gmfeatures,gmdata,totalrecords, trainrecordscount,gmtrainfeatures,gmtraindata,gmtestfeatures,gmtestdata, labelcol)
    #file_handling.traintestsplit(dpfeatures,dpdata,totalrecords, trainrecordscount,dptrainfeatures,dptraindata,dptestfeatures,dptestdata, labelcol)
