'''
Created on Feb 24, 2016

@author: amita
'''

import os, subprocess,sys

class Featureablation():
    inputtrain=""
    inputtest=""
    outputtrain=""
    outputtest=""
    removeattributecommand=""
    choice=""
    

    def __init__(self, inputtrain, inputtest, outputtrain, outputtest,keepattributecommand, removeattributecommand):
        self.inputtrain = inputtrain
        self.inputtest = inputtest
        self.outputtrain = outputtrain
        self.outputtest = outputtest
        self.removeattributecommand = removeattributecommand
        self.keepattributecommand=keepattributecommand


    def createoutputdir(self):
        outtraindir=os.path.dirname(self.outputtrain)
        if os.path.exists(outtraindir):
            os.makedirs(outtraindir)
        outtestdir=os.path.dirname(self.outputtest)
        if os.path.exists(outtestdir):
            os.makedirs(outtestdir)    
        
    def keepattribute (self,wekajarpath):
        cmd= """ java -cp {0}  weka.filters.unsupervised.attribute.RemoveByName -V -E  "{1}" -b -i {2} -o {3} -r {4} -s  {5}""" .format(wekajarpath,self.keepattributecommand, self.inputtrain,self.outputtrain,self.inputtest,self.outputtest )  
        print(cmd)
        proc=subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE)
        output, errors=proc.communicate()
        if proc.returncode != 0:
            print("weka Failed {0} {1} {2}".format( proc.returncode, output,errors))
            sys.exit(-1) 