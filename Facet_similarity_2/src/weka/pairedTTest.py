'''
Created on Mar 15, 2016
create input to run paired TTest
@author: amita
'''
from pydoc import help
from scipy.stats.stats import pearsonr
from utilities import file_handling
import pandas as pd
import os, copy,sys
from collections import OrderedDict
import configparser

class Correlationfolds():
    
    def __init__(self, col1,col2,numFold,no):
        self.col1=col1
        self.col2=col2
        self.numOfFold=numFold
        self.noOfinstances=no



    def pearson(self,list1,list2):
        ans=pearsonr(list1, list2)
        return ans[0]

    def crossfolds(self,rows,numberoffolds): 
        itemsinfold=int(len(rows)/numberoffolds)
        crosslist= [rows[i:i+itemsinfold] for i in range(0,len(rows),itemsinfold)]
        return crosslist
       
    def pearsonCVfolds(self,inputfile,actualCol, predictCol,numOfFold,noOfinstances):
        dataFrame=pd.read_csv(inputfile,nrows=noOfinstances)
        columns=[actualCol, predictCol]
        #colslist=[(row[actualCol],row[PredictCol])  for row in rows1 ]
        dataFrame[columns] = dataFrame[columns].convert_objects(convert_numeric=True)
        actualColList=list(dataFrame[actualCol])
        predictColList=list(dataFrame[predictCol])
        Rsquared=self.pearson(actualColList,predictColList)
        crosslistActual=self.crossfolds(actualColList,numOfFold)
        crosslistPredict=self.crossfolds(predictColList,numOfFold)
        corrlist=[]
        for count in range(0,len(crosslistActual)):
            actualFold=crosslistActual[count]
            predictFold=crosslistPredict[count]
            corrlist.append(self.pearson(actualFold,predictFold))
        return (corrlist,Rsquared)     

    def writeCorrFolds(self,output, featureName, corrlist):
        allRows=[]
        if os.path.isfile(output):
            rows=file_handling.readCsv(output)
            i=0
            if len(rows) == len(corrlist):
                    for row in rows:
                        newdict= OrderedDict()
                        newdict=copy.deepcopy(row)
                        newdict[featureName]=corrlist[i]
                        i=i+1
                        allRows.append(newdict)
            else:
                print("Number of records do not match in correlation file, check number of folds in each file")
                sys.exit(-1);
        else:
            for count in range(0, len(corrlist)) :
                newdict=newdict= OrderedDict()
                newdict[featureName]=corrlist[count]
                allRows.append(newdict)
        
        df = pd.DataFrame(allRows) 
        return df

    def executepearson(self,inputfile1,inputfile2,output,actualCol, predictCol,numOfFold,noOfinstances):
        corrList1_Rsquared=self.pearsonCVfolds(inputfile1,actualCol, predictCol,numOfFold,noOfinstances)
        corrList2_Rsquared=self.pearsonCVfolds(inputfile2,actualCol, predictCol,numOfFold,noOfinstances)
        if os.path.exists(output):
            os.remove(output)
        featureName1=os.path.basename(inputfile1[:-4]) +"_CorrFolds:" + str(numOfFold)
        Rsquared1=corrList1_Rsquared[1]
        df=self.writeCorrFolds(output, featureName1, corrList1_Rsquared[0])
        df.insert(0, "RsquaredAvg"+featureName1,  Rsquared1)
        df.to_csv(output, encoding='utf-8',index=False)
        featureName2=os.path.basename(inputfile2[:-4]) +"_CorrFolds:" + str(numOfFold)
        df=self.writeCorrFolds(output, featureName2, corrList2_Rsquared[0])
        Rsquared2=corrList2_Rsquared[1]
        df.insert(0, "RsquaredAvg"+featureName2,  Rsquared2)
        df.to_csv(output, encoding='utf-8',index=False)
        


    
def readCongigFile(section):
    config = configparser.ConfigParser()
    config.read('pairedTTest_config.ini')
    inputdata=config.get(section,"csv_with_file_list")
    pairedTTestresult=config.get(section,"pairedTTestresult")
    arguments=(inputdata,pairedTTestresult)
    return  (arguments)

def execute(inputdata,pairedTTestresult):
    inputdata_df=pd.read_csv(inputdata,na_filter=False)
   
    for index, row in inputdata_df.iterrows():
        inputfile1=row["input1"]
        inputfile2=row["input2"]
        outputdir=row["outputDir"]
        filename=row["outputFile"]
        output=os.path.join(outputdir,filename)
        numFold=row["numFold"]
        noOfinstances=row["noOfinstances"]
        correlationfolds=Correlationfolds("actual","predicted",numFold,noOfinstances)
        correlationfolds.executepearson(inputfile1,inputfile2,output,correlationfolds.col1, correlationfolds.col2,correlationfolds.numOfFold,correlationfolds.noOfinstances)

def run(section):    
    args=readCongigFile(section)
    execute(args[0],args[1])
       
if __name__ == '__main__':
    section="ablationDP"
    run(section)

    



    