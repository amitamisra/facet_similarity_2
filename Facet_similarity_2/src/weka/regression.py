'''
Created on Feb 19, 2016
perform grod search for SVM and Gaussian using nested cross validation
@author: amita
'''

import os, subprocess,pandas as pd
class WekaRegression():

    def __init__(self, reg_type_list,classifier_list,train_file,kernel_type, test_file, reg_label,folds,SVMPuk,gaussianPuk,wekapath):
        
        self.regtype_list= reg_type_list
        self.classifier_list=classifier_list
        self.train_file=train_file
        self.test_file=test_file
        self.kernel_type=kernel_type
        self.reg_label=reg_label
        self.folds=folds
        self.SVMPUK=SVMPuk
        self.wekapath=wekapath
        self.GaussianPUK=gaussianPuk
    
    def movelablecol_lastcol(self):
        df=pd.read_csv(self.train_file)
        cols = df.columns.tolist()
        lastindex=len(cols)
        cols.insert(lastindex-1, cols.pop(cols.index(self.reg_label)))
        df = df.reindex(columns= cols)
        df.to_csv(self.train_file,index=False)
        
        
    def run(self):
        self.movelablecol_lastcol()
        if "CV" in self.regtype_list:
            self.execute_CV()
        if "predict" in self.regtype_list:
            self.execute_Predict()  
 
    
    # assumes regression label is last column
    def gridSvmCV(self):
        dirname=os.path.join(os.path.dirname(self.train_file), "SVM")
        if not os.path.exists(dirname):
            os.mkdir(dirname)
        outputfile=os.path.join(dirname, os.path.basename(self.train_file)[:-4]+"_CV.csv")
        classifier ="weka.classifiers.meta.GridSearch"
        classifieroption=self.SVMPUK
        cmd= str("java -cp " + self.wekapath+ " weka.Run " + classifier + " -t " + self.train_file + " -c last " + " -x "+ str(self.folds) + " " + classifieroption +" > "+ outputfile)
        print(cmd)
        proc=subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE)
        output, errors=proc.communicate()
        print (errors)
        
    def gaussianCV(self):    
        dirname=os.path.join(os.path.dirname(self.train_file), "Gaussian")
        if not os.path.exists(dirname):
            os.mkdir(dirname)
        outputfile=os.path.join(dirname, os.path.basename(self.train_file)[:-4]+"_CV.csv")
        classifier ="""weka.classifiers.functions.GaussianProcesses -L 1.0 -N 0 -K "weka.classifiers.functions.supportVector.Puk -O 1.0 -S 1.0 -C 250007"""
        cmd= str("java -cp " + self.wekapath+ " " + classifier + " -t " + self.train_file + " -c " + self.reg_label_index + " -x "+ str(self.folds) + " > "+ outputfile)
        proc=subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE)
        output, errors=proc.communicate()
        print (errors)   
    
    
    def execute_CV(self):
            if "grid" in self.classifier_list:  
                self.gridSvmCV()              
                    
            if "Gaussian" in  self.classifier_list:
                self.gaussianCV() 
                        
                    
            
    def execute_Predict(self):
        pass
    
    def getkernelType(self):
        return self.kernel_type
    
    def setOutputDir(self):
        self.outputDir=os.path.join(os.path.basename(self.inputFile),"CV")
        