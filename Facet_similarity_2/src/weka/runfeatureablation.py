'''
Created on March 12, 2016
used to run weka from command line. Works with train and test as input.
Input is read through a csv file. Generate feature combinations using remove features column in input csv. the classifier is
also specified as an input in csv.  
@author: amita
'''
import configparser

import pandas as pd,os
from weka.featureablation import Featureablation
from utilities.file_handling import readTextFile
from weka.wekamodel import WekaClassification

def readCongigFile(section):
    config = configparser.ConfigParser()
    config.read('runfeatureablation.ini')
    inputdata=config.get(section,"csv_with_file_list")
    wekapath=config.get(section,"wekapath")
    featureabresults=config.get(section,"featureaboutput")
    arguments=(inputdata,wekapath,featureabresults)
    return  (arguments)

def getscore(relevant_res,scoresdict,suffix):
   
    count=0
    for line in relevant_res:
        if "Correlation coefficient" in line:
            index=count
            break
        else:
            count=count+1
                
    for indexscore in range(index,index+3):   
            line=relevant_res[indexscore]
            result=line.split()
            scoresdict[" ".join(result[:-1])+"_"+suffix]=result[-1]
      

def writescores(outputtrain,outputtest,outputarff_CV,outputarff_predict,scoredict):
    if outputarff_CV:
        CVresults=readTextFile(outputarff_CV)
        relevant_res_cv= CVresults.index("=== Cross-validation ===\n")
        getscore(CVresults[relevant_res_cv:],scoredict,"CV")
    if   outputarff_predict: 
        predict_results=readTextFile(outputarff_predict)
        relevant_res_p= predict_results.index("=== Error on test data ===\n")
        getscore(predict_results[relevant_res_p:],scoredict,"testSet")
    scoredict["train"]= outputtrain
    scoredict["test"]=outputtest   
        
          
        
    
    

def execute(inputdata,wekajarpath,featureabresults):
    allrows=[]
    inputdata_df=pd.read_csv(inputdata,na_filter=False)
    for index, row in inputdata_df.iterrows():
        done=row["done"]
        if done==str(1):
            continue
        inputtrain=row["inputTrain"]
        inputtest=row["inputTest"]
        keepattributecommand=row["keepattributecommand"]
        removeattributecommand=row["removeattributecommand"]
        reg_type_list=row["reg_type_list"]
        class_label=row["class_label"]
        folds=row["folds"]
        classifier=row["classifier"]
        classifier_option=row["classifier_option"]
        classifier_dirname=row["classifier_dirname"]
        choice=row["choice"]
        outputbaseDir=row["outputBaseDir"]
        outputFeature=row["outputFeatureName"]
        outputtrain= os.path.basename(inputtrain).split(".csv")[0]+"_"+outputFeature + "_" + ".arff"
        outputtrain=os.path.join(outputbaseDir,outputFeature,outputtrain)
        outputtest=os.path.basename(inputtest).split(".csv")[0]+"_"+outputFeature + "_" + ".arff"
        outputtest=os.path.join(outputbaseDir,outputFeature,outputtest)
        dirname=os.path.join(outputbaseDir,outputFeature)
        if not os.path.exists(dirname):
            os.makedirs(dirname)
        featureablation = Featureablation(inputtrain,inputtest,outputtrain,outputtest,keepattributecommand, removeattributecommand)
        if choice=="keep":
            scoredict=dict()
            featureablation.keepattribute(wekajarpath)
            classifier_obj= WekaClassification(reg_type_list,outputtrain,outputtest,class_label,folds,classifier,classifier_option,wekajarpath,classifier_dirname)
            classifier_obj.run()
            writescores(outputtrain,outputtest,classifier_obj.outputarff_CV,classifier_obj.outputarff_test,scoredict)
            scoredict["classifier"]=classifier+classifier_option
            scoredict["Feature"]=outputFeature
            allrows.append(scoredict)
    df=pd.DataFrame(allrows)
    df.to_csv(featureabresults,index=False)
        
def run(section):
    args=readCongigFile(section)
    execute(args[0],args[1],args[2])
    
     
if __name__ == '__main__':
    section="ablationGC"
    run(section)
    section="ablationGM"
    run(section)
    section="ablationDP"
    run(section)
