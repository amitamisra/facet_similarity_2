'''
Created on Feb 19, 2016

@author: amita
'''

from weka import regression
import configparser
def runwekaregression(section):
    args= readCongigFile(section)
    reg_type_list= args[0]
    classifier_list=args[1]
    train_file=args[2]
    test_file=args[3]
    kernel_type=args[4]
    reg_label=args[5]
    folds=args[6]
    SVMPuk=args[7]
    gaussianPuk=args[8]
    wekapath=args[9]
    execute(reg_type_list,classifier_list,train_file,test_file,kernel_type,reg_label,folds,SVMPuk, gaussianPuk,wekapath)

def readCongigFile(section):
    config = configparser.ConfigParser()
    config.read('runregression_Config.ini')
    reg_type_list = config.get(section, 'reg_type').split(",")
    classifier_list=config.get(section,'classifier_list').split(",")
    train_file= config.get(section, 'train_file')
    test_file= config.get(section, 'test_file')
    kernel_type=config.get(section, 'kernel_type')
    reg_label=config.get(section, 'reg_label')
    folds= int(config.get(section, 'folds'))
    SVMPuk=config.get(section,"SVMPUK")
    gaussianPuk=config.get(section,"GaussianPuk")
    wekapath=config.get(section,"wekapath")
    arguments=(reg_type_list,classifier_list,train_file,test_file,kernel_type,reg_label,folds,SVMPuk, gaussianPuk,wekapath)
    return  (arguments)

def execute(reg_type_list,classifier_list,train_file,test_file,kernel_type,reg_label,folds,SVMPuk, gaussianPuk,wekapath):
    weka_regression= regression.WekaRegression(reg_type_list,classifier_list,train_file,kernel_type, test_file, reg_label,folds,SVMPuk,gaussianPuk,wekapath)
    weka_regression.run()
    
    
if __name__ == '__main__':
    
    section="GC"
    
    runwekaregression(section)