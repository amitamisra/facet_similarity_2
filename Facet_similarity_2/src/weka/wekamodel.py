'''
Created on Feb 19, 2016
perform grod search for SVM and Gaussian using nested cross validation
@author: amita
'''

import os,sys, subprocess,pandas as pd
from utilities import file_handling
class WekaClassification():

    def __init__(self, reg_type_list,train_file, test_file, class_label,folds,classifier,classifier_option,wekapath,classifier_dir):
        
        self.regtype_list= reg_type_list
        self.train_file=train_file
        self.test_file=test_file
        self.class_label=class_label
        self.folds=folds
        self.wekapath=wekapath
        self.classifier_option=classifier_option
        self.classifier=classifier
        self.classifier_dir=classifier_dir
        self.outputarff_CV=""
        self.outputarff_test=""
        self.output_CVPredict=""
    
    def movelablecol_lastcol(self):
        df=pd.read_csv(self.train_file)
        cols = df.columns.tolist()
        lastindex=len(cols)
        cols.insert(lastindex-1, cols.pop(cols.index(self.reg_label)))
        df = df.reindex(columns= cols)
        df.to_csv(self.train_file,index=False)
        
        
    def run(self):
        #self.movelablecol_lastcol()
        if "CV" in self.regtype_list:
            self.execute_CV()
        if "CV_predict" in self.regtype_list:
            self.execute_CVpredict()
        if "test" in   self.regtype_list:
            self.execute_test()   
 
 
    def execute_CV(self):
        dirname=os.path.join(os.path.dirname(self.train_file), self.classifier_dir)
        if not os.path.exists(dirname):
            os.mkdir(dirname)
        outputfile=os.path.join(dirname, os.path.basename(self.train_file)[:-5]+ self.classifier_dir+"_CV.arff")
        cmd= str("java -cp " + self.wekapath+ " " + self.classifier + " -t " + self.train_file + " -c last " + " -x "+ str(self.folds) + " " + self.classifier_option +" > "+ outputfile)
        print(cmd)
        proc=subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE)
        output, errors=proc.communicate()
        if proc.returncode != 0:
            print("weka Failed {0} {1} {2".format( proc.returncode, output,errors))
            sys.exit(-1)
        with open(outputfile, "a") as myfile:
            myfile.write("\ntrain File "+ self.train_file)
            myfile.write("\nclassifier "+ cmd)    
        self.outputarff_CV=outputfile     
        
    def execute_test(self):
        dirname=os.path.join(os.path.dirname(self.test_file), self.classifier_dir)
        if not os.path.exists(dirname):
            os.mkdir(dirname)
        outputfile=os.path.join(dirname, os.path.basename(self.test_file)[:-5]+"_test.arff")
        cmd= str( "java -cp " + self.wekapath+ " " +self.classifier + " -t " + self.train_file +  " -T " + self.test_file+  " -c last " + " -x "+ str(self.folds) + " " + self.classifier_option +" > "+ outputfile)
        print(cmd)
        proc=subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE)
        output, errors=proc.communicate()
        if proc.returncode != 0:
            print("weka Failed {0} {1} {2}".format( proc.returncode, output,errors))
            sys.exit(-1)
        with open(outputfile, "a") as myfile:
            myfile.write("\ntrain File "+ self.train_file)
            myfile.write("\ntest File"+ self.test_file)
            myfile.write("\nclassifier "+ cmd)
        self.outputarff_test= outputfile 
        
    def execute_CVpredict(self):
        dirname=os.path.join(os.path.dirname(self.test_file), self.classifier_dir)
        if not os.path.exists(dirname):
            os.mkdir(dirname)
        outputfile=os.path.join(dirname, os.path.basename(self.train_file)[:-5]+ self.classifier_dir+"_CVPredict.txt")
        cmd= str("java -cp " + self.wekapath+ " " + self.classifier + " -t " + self.train_file + " -c last " + " -x "+ str(self.folds) + " " + self.classifier_option +" -p 0  > "+ outputfile)
        print(cmd)
        proc=subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE)
        output, errors=proc.communicate()
        if proc.returncode != 0:
            print("weka Failed {0} {1} {2".format( proc.returncode, output,errors))
            sys.exit(-1)
        self.parsepredictionstocsv(outputfile)
    def parsepredictionstocsv(self,wekapredictiontxtFile):  
        predictionlines=  file_handling.readTextFile(wekapredictiontxtFile)
        count=0
        for line in predictionlines:
            if "actual" in line:
                index=count
                break
            else:
                count=count+1
        allrows=[]       
        predlen=len(predictionlines)
        for indexscore in range(index+1,predlen-1):
            line=predictionlines[indexscore]
            result=line.split()  
            preddict={}
            preddict["inst#"]= result[0]    
            preddict["actual"]=  result[1]
            preddict["predicted"]=result[2]
            preddict["error"]=result[3]
            allrows.append(preddict)
        df=pd.DataFrame(allrows)
        outfile=wekapredictiontxtFile[:-3]+".csv"
        df.to_csv(outfile,index=False)
            
if __name__ == '__main__':
    reg_type_list=""
    train_file=""
    test_file=""
    class_label=""
    folds=""
    classifier=""
    classifier_option=""
    wekapath=""
    classifier_dir=""
    predictionfile="/Users/amita/facet_similarity_part2/data/deathPenalty/greater_than_55/argumentQuality/argumentpairsV2/traintestsplit/WekaResults/LIWC/SMO/mt1_truelabel_dp_gt55_argQualPairsTop2000_TrainOnlyfeatures_norm_LIWC_SMO_CVPredict.txt"
    WekaClassificationobj=WekaClassification(reg_type_list,train_file, test_file, class_label,folds,classifier,classifier_option,wekapath,classifier_dir)           
    WekaClassificationobj.parsepredictionstocsv(predictionfile)